#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
# from openalpr import Alpr
import numpy as np
import glob
import cv2
from matplotlib import pyplot as plt
import json



def ReadJSON(path):
    file_object  = open(path, "r")
    json_info = file_object.read()
    dic = json.loads(json_info)
    file_object.close()
    return dic


def WriteJSON(path,dic):
    file_object = open(path,"w")
    str1 = json.dumps(dic, sort_keys=True,indent=4, separators=(',', ': '))
    file_object.write(str1)
    file_object.close()



def bb_intersection_over_union(boxA, boxB):
	# determine the (x, y)-coordinates of the intersection rectangle
	xA = max(boxA[0], boxB[0])
	yA = max(boxA[1], boxB[1])
	xB = min(boxA[2], boxB[2])
	yB = min(boxA[3], boxB[3])
 
	# compute the area of intersection rectangle
	interArea = (xB - xA + 1) * (yB - yA + 1)
 
	# compute the area of both the prediction and ground-truth
	# rectangles
	boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
	boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
 
	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = interArea / float(boxAArea + boxBArea - interArea)
 
	# return the intersection over union value
	return iou


###
def showPlatesDetected(plates,img):
    for (x,y,w,h) in plates: 
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),5)
        plt.subplot(121)
        plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')
        plt.subplot(122)
        crop = img[y:y+h,x:x+w,:]
        plt.imshow(cv2.resize(crop[:,:,::-1], (64, 32)) ,'gray'),plt.title('ORIGINAL')
    plt.show()

def getFilesNames(path):
    files = glob.glob(path)
    names  = []
    for  file in files:
        garbage, name = os.path.split(file)
        names.append(name)
    return names

def convertLabel(user_pos):
    user_pos[2]+=user_pos[0]
    user_pos[3]+=user_pos[1]
    return user_pos

def existPlate(image_dic):
    if "Pos_usuario" in image_dic and "Placa_usuario" in image_dic:
        return image_dic["Pos_usuario"]!= [0,0,0,0] and image_dic["Placa_usuario"] !="0"
    else:
        return False


def plate2labels(plate):
    if len(plate) == 6:
        let = []
        num = []
        for i in range(3):
            let.append(ord(plate[i].lower())-96)
            num.append(int(plate[i+3]))
        return let+num
    else:
        return None

        

def cropandsave(img,ROI,path,show = False):
    kernel = np.ones((3,3),np.uint8)
    crop = img[ROI[1]:ROI[3], ROI[0]:ROI[2],:]
    cv2.imwrite(path, crop)
    gray_image = cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY)
    th3 = cv2.adaptiveThreshold(gray_image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)
    closing = cv2.morphologyEx(th3, cv2.MORPH_CLOSE, kernel)
    
    if show:
        plt.subplot(221),plt.imshow(crop[:,:,::-1])
        plt.subplot(222),plt.imshow(gray_image,cmap='gray')
        plt.subplot(223),plt.imshow(th3,cmap='gray')
        plt.subplot(224),plt.imshow(closing,cmap='gray')
        
        plt.show()    

def Crop_resize_reshape(img, ROI, shape=(64,32), show=False):
    crop = img[ROI[1]:ROI[3], ROI[0]:ROI[2],:]
    gray_image = cv2.resize( cv2.cvtColor(crop, cv2.COLOR_BGR2GRAY),shape)
    im_reshaped = gray_image.reshape((-1,1))
    if show:
        cv2.imshow('image',gray_image)
        cv2.waitKey(0)
    return im_reshaped

def createdataset():
        folder_path = '/home/ssi_ralvarez/Documentos/Dataset_placas'
    
    crop_images_path = folder_path+ "/Cropped_Plates/"

    lp_cascade = cv2.CascadeClassifier('co.xml')
    # diccionarios y rutas donde se encuenctran las imagenes:
    
    files_dic = {'registro_placas.txt':[folder_path+'/Univalle1/','.jpg'],
                'registro_placas_3.txt':[folder_path+'/Univalle2/Sn_distorsion/','.png'],
                'registro_placas_4.txt':[folder_path+'/Univalle3/','.png'],
                'registro_placas_5.txt':[folder_path+'/Univalle4/','.png'],
                'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png']}

    data = []
    labels = []
    for  dic_key in files_dic:
        # diccionario que contiene las etiquetas de las imagenes
        labels_dic = ReadJSON(dic_key) 
        # lee los archivos presentes en la carpeta:
        names = getFilesNames(files_dic[dic_key][0] +'*'+files_dic[dic_key][1])
    	_,prefix = os.path.split(files_dic[dic_key][0][:-1])
        images_cnt =0
        images_detected = 0
        falses = 0
        print prefix   
        for key,image_dic in labels_dic.iteritems(): 
            if key in names:
                if existPlate(image_dic):
                    images_cnt+=1
                    user_pos = convertLabel(image_dic["Pos_usuario"])
                    img = cv2.imread(files_dic[dic_key][0]+key)
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    plates = lp_cascade.detectMultiScale(gray, 1.1, 5)
                    for (x,y,w,h) in plates: 
                        lib_pos = [x,y,x+w,y+h]
                        result = bb_intersection_over_union(lib_pos,user_pos)
                            label =plate2labels(image_dic["Placa_usuario"])
                            if label!=None:
                        if result >0.5 and result <=1.0:
                                labels.append(label)
                                data.append(Crop_resize_reshape(img,lib_pos,show =False))
                            images_detected += 1
                            # print images_detected*100.0/images_cnt,image_dic["Placa_usuario"],result
                            # cropandsave(img,lib_pos,crop_images_path+prefix+"_" + key[:-4] + 'l.jpg')
                            break
                        else:
                            #cropandsave(img,user_pos,crop_images_path+prefix+"_" + key[:-4] + 'u.jpg')
                            falses+=1
        print images_cnt,images_detected
        print len(data),len(labels)

    np.save('data.npy',data)
    np.save('labels.npy',labels)

                     
        

if __name__ == "__main__":
    # alpr = Alpr("co", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data")
    # folder_path = '/home/ralvarez/Documentos/BaseDedatosPlacas'
