#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2
import os
import tkFileDialog
import glob
import json
import matplotlib.pyplot as plt
def ReadJSON(path):
    file_object  = open(path, "r")
    json_info = file_object.read()
    dic = json.loads(json_info)
    file_object.close()
    return dic

class croppingCharacters:

	def __init__(self):
		self.W = []
		self.H = []
		self.charsCount = 0		
		self.images_list = []
		self.letters_list = []
		
	def cropCaracters(self,img,array,show=False):
		if array == 0:
			return []
		characters = []
		letters = []
		for letter in array:
			let, pos = letter.keys()[0],letter.values()[0]
			num = ord(let)
			if (num>= ord('A') and num <= ord('Z')) or (num>= ord('0') and num <= ord('9')):
				x,y,w,h = pos
				self.W.append(w)
				self.H.append(h)
				crop_char_img = img[y:y+h,x:x+w,:]
				crop_char_img = cv2.resize(crop_char_img,dsize=(16,32))
				characters.append(crop_char_img)
				letters.append(let)
				if show:
					cv2.imshow('imagen',crop_char_img )
					cv2.waitKey(0)
		return characters,letters

	def cropCharactersAndaddtolist(self,img,array,show=False):
		if array == 0:
			return
		characters,letters =self.cropCaracters(img,array,show=False)
		print letters
		self.images_list =self.images_list +characters
		self.letters_list = self.letters_list+letters


	def savelist(self,name):
		np.save(name+'_images.npy',np.asarray(self.images_list))
		np.save(name+'_letters.npy',np.asarray(self.letters_list))


	def getWithtAndHeight(self,array):
		if array == 0:
			return []
		for letter in array:
			let, pos = letter.keys()[0],letter.values()[0]
			x,y,w,h = pos
			self.W.append(w)
			self.H.append(h)


	def SaveCharacters(self,characterlist,path,suffix='.jpg'):
		for img in characterlist:
			cv2.imwrite(path+'img_'+str(self.charsCount)+ '.jpg', img)
			self.charsCount+=1


folder_path ='/home/ssi_ralvarez/Documentos/Dataset_placas'
crop_plates_path = folder_path+ "/Cropped_Plates/"
crop_chars_path = folder_path+ "/Cropped_Chars/"

"""
files_dic = {'registro_placas.txt':[folder_path+'/Univalle1/','.jpg'],
			'registro_placas_2.txt':[folder_path+'/Anexo 10 Imagenes Pruebas de integraciвn/pos1/','.png'],
			'registro_placas_3.txt':[folder_path+'/Univalle2/Sn_distorsion/','.png'],
			'registro_placas_4.txt':[folder_path+'/Univalle3/','.png'],
			'registro_placas_5.txt':[folder_path+'/Univalle4/','.png'],
			'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png']}
"""
files_dic = {'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png','json_char(viernes_1).txt']}
cp = croppingCharacters()

		
for key in files_dic:
	print key
	dic = ReadJSON(key) # diccionario que contiene en
	dic_chars = ReadJSON(files_dic[key][2]) # diccionario que contiene en
	
	images_path = files_dic[key][0][:-1]
	ext ='/*'+files_dic[key][1]
	garbage,prefix = os.path.split(images_path)
	dir_list =  glob.glob(images_path + ext)
	values = dic.values()

	empty  = [0, 0, 0, 0]
	show = False
	crop = True
	analize = False
	statistics = True
	
	files = dir_list

	for file in files:
		folder,filename = os.path.split(file)
		if filename in dic:
			val =dic[filename]
			if not os.path.exists(crop_plates_path + filename):
				temp =val['Pos_usuario']
				if temp != empty:	
					if crop:				
						img = cv2.imread(file)
						cropped_image = img[temp[1]:temp[1]+temp[3], temp[0]:temp[0]+temp[2],:]
						#cv2.imwrite(crop_plates_path+prefix+"_" + filename[:-4] + '.jpg', cropped_image)
						if filename in dic_chars:
							print filename
							# cp_chars =cp.cropCaracters(cropped_image,dic_chars[filename])
							# cp.SaveCharacters(cp_chars,crop_chars_path)
							cp.cropCharactersAndaddtolist(cropped_image,dic_chars[filename])				
						if show:
							cv2.namedWindow('imagen', cv2.WINDOW_NORMAL)
							im = cv2.resizeWindow('imagen', img.shape[1]/4, img.shape[0]/4)
							cv2.imshow('imagen', img)
							cv2.waitKey(0)
							cv2.imshow("Imagen recortada", cropped_image)
							cv2.waitKey(0)
					if analize:				
						if filename in dic_chars:
							cp.getWithtAndHeight(dic_chars[filename])			
	
	cp.savelist('data')
	if analize:
		div = np.divide(np.asarray(cp.W,dtype=float),np.asarray(cp.H,dtype=float))
		plt.hist(div)
		plt.show()
		plt.hist(cp.H)
		plt.hist(cp.W)
		plt.show()
		