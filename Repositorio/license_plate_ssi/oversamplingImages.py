import os 
import cv2
import glob
import numpy as np
from matplotlib import pyplot as plt
import time
import copy
files= glob.glob('/home/ssi_ralvarez/Documentos/cropped_plates/*.png')
T =time.time()
print T
np.random.seed(int(T))


plt.figure()
idx  = 0
show = False
for file in files:
    try:
        img = cv2.imread(file)
        
        for i  in range(1,10):
            img_copy =copy.copy(img)
            k = np.random.randint(6)+1
            kernel = np.ones((k,k),np.float32)/(k*k)
            img_copy = cv2.filter2D(img_copy,-1,kernel)
            p =2.4*np.random.random()+0.25
            img_copy  = np.power(255.,1.-p) *np.power(img_copy,p)
            height,width,g  = img_copy.shape
            r_h,r_w  = int(height*k/100)+1,int(width*k/200)+1
            img_copy = img_copy[r_h:-r_h,r_w:-r_w,:]
            # if np.random.random()>0.5:
            #     img_copy  =img_copy[:,::-1,:]
            img_copy  =img_copy +np.random.randint(k*2,size= img_copy.shape)-k
            img_copy[img_copy<0] = 0
            img_copy[img_copy>255] = 255
            cv2.imwrite('/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/train-detector/positive/img'+str(idx)+'.png',img_copy)
            
            idx = idx +1
            if show:     
              plt.subplot(330+i),plt.imshow(img_copy.astype('uint8')[:,:,::-1],'gray'),plt.title('ORIGINAL')
        if show:     
            plt.show()

    except KeyboardInterrupt:
        break
    