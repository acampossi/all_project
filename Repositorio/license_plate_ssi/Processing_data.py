#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import cv2
import os
import glob
import json
import matplotlib.pyplot as plt

def ReadJSON(path):
    file_object  = open(path, "r")
    json_info = file_object.read()
    dic = json.loads(json_info)
    file_object.close()
    return dic

ORDERS =[   [0, 1, 2,3,4,5],
            [0, 2, 1,3,5,4],
            [1, 0, 2,4,3,5],
            [1, 2, 0,4,5,3],
            [2, 0, 1,5,3,4],
            [2, 1, 0,5,4,3]]


def processCharsArray(array):
    if array == 0 or array == None or array == [] or len(array)!=6:
            return [],[]
    characters = []
    letters = []
    for letter in array:
        letters.append(letter.keys()[0]),characters.append( letter.values()[0])
    letters = np.asarray(letters)
    characters = np.asarray(characters)
    x = np.argsort(characters[:,0])
    letters = letters[x]
    characters = characters[x,:]
    return letters,characters


def organizelabel(label,order):
    out = ""
    for i in range(len(order)):
        out= out+ label[order[i]]
    return out

def CropImage(img,position):
    return img[position[1]:position[1]+position[3], position[0]:position[0]+position[2],:]

if __name__ == "__main__":
    folder_path ='/home/ssi_ralvarez/Documentos/Dataset_placas'
    crop_plates_path = folder_path+ "/Cropped_Plates/"
    crop_chars_path = folder_path+ "/Cropped_Chars/"
    

class croppingCharacters:


    def __init__(self):
        self.W = []
        self.H = []
        self.charsCount = 0		
        self.images_list = []
        self.letters_list = []
        self.position = []

    # muestra estadistiscasa de las posisciones de los caracteres
    def ShowPositionStatistics(self):
        data = np.asarray(self.position,dtype=float)
        plt.subplot(131), plt.hist(data[:,2]/data[:,3])
        plt.subplot(132),plt.hist(data[:,2])
        plt.subplot(133),plt.hist(data[:,3])
        plt.show()

    # Cambiar la posicion de los caracteres de acuerdo al orden entregado
    def changeCharactersOrder(self,img,pos,order):
        img_copy = np.copy(img)
        for i in range(len(order)):
            if i!=order[i]:
                x,y,w,h = pos[i,:]
                ponderation = self.getimagePonderation(w,h)
                img_copy[y:y+h,x:x+w,:]= img_copy[y:y+h,x:x+w,:]*(-ponderation+1.0) +ponderation*cv2.resize(CropImage(img,pos[order[i],:]), dsize=(w,h))
            # img_copy[y:y+h,x:x+w,:]= cv2.resize(CropImage(img,pos[order[i],:]), dsize=(w,h))
        return   img_copy

    # genrea la grilla de  que pondera la imagen nueva con la imagen original 
    def  getimagePonderation(self,w,h):
        image = np.ones((h,w,3))
        array = [0.3, 0.5, 0.7, 0.9]
        cnt = 0 
        for val in array:
            image[cnt,:,:] =image[cnt,:,:]*val
            image[-cnt-1,:,:] =image[-cnt-1,:,:]*val
            image[:,cnt,:] =image[:,cnt,:]*val
            image[:,-cnt-1,:] =image[:,-cnt-1,:]*val
            cnt+=1
        # plt.imshow(image)
        # plt.show()
        return image
    
    # adiciona una posicion al buffer para observar estadisticas
    def GetPositionData(self, position):
        self.position.append(position)

    #  lee las imagenes las recorta y las muestra 
    def CropResizeAndShow(self,file,position,sizes):
        img = cv2.imread(file)
        img = CropImage(img,position)
        l = len(sizes)
        for i in range(l):
            img_resize = cv2.resize(img,dsize=sizes[i])
            plt.subplot(101+l*10+i)
            plt.imshow(img_resize,cmap='gray')
        plt.show()

    # lee las imagenes las recorta y las adiciona al buffer
    def CropResizeAndSave(self,file,position,label,size):
        if len(label)==6:
            img = cv2.imread(file)
            img = CropImage(img,position)
            img_resize = cv2.resize(img,dsize=size)
            self.images_list.append(img_resize)
            self.letters_list.append(label)

    #  lee las imagenes las recorta, cambia la posicion de los caracteres y las muestra 
    def CropResizeChangePosAndsave(self,file,position,label,charspos,size= (64,32),show= True):

        img = cv2.imread(file)
        img = CropImage(img,position)
        letters,letters_pos = processCharsArray(charspos)
        if len(label)==6:
            if letters_pos != []:        
                cnt = 1
                for order in ORDERS:
                    img_resize = self.changeCharactersOrder(img,letters_pos,order)
                    img_resize = cv2.resize(img_resize,dsize=size)
                    newlabel = organizelabel(label,order)
                    self.images_list.append(img_resize)
                    self.letters_list.append(newlabel)
                    self.charsCount+=1
                
                    if show:
                        plt.subplot(330+cnt),plt.imshow(img_resize[:,:,::-1],cmap='gray')
                        plt.title()
                        cnt+=1
                plt.show()
            else:
                self.charsCount+=1
                img_resize = cv2.resize(img,dsize=size)
                self.images_list.append(img_resize)
                self.letters_list.append(label)



    # recorta los caracteres y retorna listas con el resultados y sus etiquetas individuales en listas
    def cropCaracters(self,img,array,show=False):
        if array == 0:
            return []
        characters = []
        letters = []
        for letter in array:
            let, pos = letter.keys()[0],letter.values()[0]
            x,y,w,h = pos
            self.W.append(w)
            self.H.append(h)
            crop_char_img = img[y:y+h,x:x+w,:]
            crop_char_img = cv2.resize(crop_char_img,dsize=(16,32))
            characters.append(crop_char_img)
            letters.append(let)
            if show:
                cv2.imshow('imagen',crop_char_img )
                cv2.waitKey(0)
        return characters,letters
    
    # recorta los caracteres y los almacena en buffer
    def cropCharactersAndaddtolist(self,img,array,show=False):
        if array == 0:
            return
        characters,letters =self.cropCaracters(img,array,show=False)
        # print letters
        self.images_list =self.images_list +characters
        self.letters_list = self.images_list+letters


    # guarda la informacion almacenada en el buffer
    def savelist(self,name):
        np.save(name+'_images.npy',np.asarray(self.images_list))
        np.save(name+'_letters.npy',np.asarray(self.letters_list))


    
    def getWithtAndHeight(self,array):
        if array == 0:
            return []
        for letter in array:
            let, pos = lett
            
            x,y,w,h = pos
            self.W.append(w)
            self.H.append(h)



    def SaveCharacters(self,characterlist,path,suffix='.jpg'):
        for img in characterlist:
            cv2.imwrite(path+'img_'+str(self.charsCount)+ '.jpg', img)
            self.charsCount+=1





EMPTY  = [0, 0, 0, 0]

class Processing_data:

    def __init__(self,cp,JSON_path,folder_path,extension,JSON_char_path=None):

        self.labels_dic = ReadJSON(JSON_path) # diccionario que contiene en
        self.chars_dic  = None
        if JSON_char_path!=None:
            self.chars_dic = ReadJSON(JSON_char_path) # diccionario que contiene en
        
        self.images_path = folder_path[:-1]
        self.ext ='/*'+extension
        garbage,self.prefix = os.path.split(self.images_path)
        self.dir_list =  glob.glob(self.images_path + self.ext)
        self.cp = cp
    def getfilename(self,file):
        folder,filename = os.path.split(file)
        return filename            

    def validate_image(self,file):
            plate_label = ""
            filename = self.getfilename(file)
            # se observa si existe una etiqueta
            if filename in self.labels_dic:
                # en caso afirmativo se obtiene la posicion de la placa y se verifica si es valida
                plate_position = self.labels_dic[filename]['Pos_usuario']
                
                if plate_position != EMPTY:
                    
                    if 'Placa_usuario' in self.labels_dic[filename]:
                        plate_label = self.labels_dic[filename]['Placa_usuario']
                    
                    if self.chars_dic!=None:
                        if filename in self.chars_dic:
                            return True,plate_position,plate_label,filename,self.chars_dic[filename]
                    
                    return True,plate_position,plate_label,filename,None
                        
            return False,EMPTY,"","",None

    def CropAndSave(self,file,plate_position,folder):
        img = cv2.imread(file)
        cropped_image = CropImage(img,position)
        cv2.imwrite(folder+self.prefix+"_" + filename[:-4] + '.jpg', cropped_image)

    def CropAndSaveCharacters(self,file,plate_position,folder):
            img = cv2.imread(file)
            cropped_image = CropImage(img,plate_position)
            if filename in self.dic_chars:
                self.cp_chars =self.cp.cropCaracters(cropped_image,self.dic_chars[filename])
                self.cp.SaveCharacters(self.cp_chars,crop_chars_path)


    
    """def CropAndSaveCharacters():
            img = cv2.imread(file)
            cropped_image = img[plate_position[1]:plate_position[1]+plate_position[3], plate_position[0]:plate_position[0]+plate_position[2],:]
            #cv2.imwrite(crop_plates_path+prefix+"_" + filename[:-4] + '.jpg', cropped_image)
            if filename in dic_chars:
                print filename
                # self.cp_chars =self.cp.cropCaracters(cropped_image,dic_chars[filename])
                # self.cp.SaveCharacters(self.cp_chars,crop_chars_path)
                self.cp.cropCharactersAndaddtolist(cropped_image,dic_chars[filename])
    """

    def processData(self):        
        #para cada una de las imagenes en la carpeta:
        for file in self.dir_list:
            # se valida la informacion y se obtiene los datos
            success,plate_position,plate_label,filename,chars_position = self.validate_image(file)
            
            if success:
                # print self.cp.charsCount
                # self.cp.GetPositionData(plate_position)
                # self.cp.CropResizeAndShow(file,plate_position,[(128,64),(96,48),(64,32)])
                self.cp.CropResizeChangePosAndsave(file,plate_position,plate_label,chars_position,show= False)
                
                # self.cp.CropResizeAndSave(file,plate_position,plate_label,(64,32))
        self.cp.savelist("plates_v2")

        # self.cp.ShowPositionStatistics()

        """
                if crop:				
                    img = cv2.imread(file)
                    cropped_image = img[plate_position[1]:plate_position[1]+plate_position[3], plate_position[0]:plate_position[0]+plate_position[2],:]
                    #cv2.imwrite(crop_plates_path+prefix+"_" + filename[:-4] + '.jpg', cropped_image)
                    if filename in dic_chars:
                        print filename
                        # self.cp_chars =self.cp.cropCaracters(cropped_image,dic_chars[filename])
                        # self.cp.SaveCharacters(self.cp_chars,crop_chars_path)
                        self.cp.cropCharactersAndaddtolist(cropped_image,dic_chars[filename])				
                    if show:
                        cv2.namedWindow('imagen', cv2.WINDOW_NORMAL)
                        im = cv2.resizeWindow('imagen', img.shape[1]/4, img.shape[0]/4)
                        cv2.imshow('imagen', img)
                        cv2.waitKey(0)
                        cv2.imshow("Imagen recortada", cropped_image)
                        cv2.waitKey(0)
    
                self.cp.savelist('data')
        """
        


    files_dic = {
                'registro_placas.txt':[folder_path+'/Univalle1/','.jpg',None],
                'registro_placas_2.txt':[folder_path+'/Anexo 10 Imagenes Pruebas de integraciвn/pos1/','.png',None],
                'registro_placas_3.txt':[folder_path+'/Univalle2/Sn_distorsion/','.png',None],
                'registro_placas_4.txt':[folder_path+'/Univalle3/','.png','json_char(viernes_24).txt'],
                'registro_placas_5.txt':[folder_path+'/Univalle4/','.png',None],
                'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png','json_char(viernes_1).txt'],
                'registro_placas_7.txt':[folder_path+'/proyecto/PDIPlacas/carros/','.jpg','json_char(carros_medellin).txt']
                }

    # files_dic = {'registro_placas_6.txt':[folder_path+'/Univalle5/Sin_Distorcion/','.png','json_char.txt']}
    cp = croppingCharacters()
    for file in files_dic:
        print cp.charsCount
        print file
        fol_path,extension,char_path = files_dic[file]
        pd = Processing_data(cp,file,fol_path,extension,JSON_char_path=char_path)
        pd.processData()
