import numpy as np
import cv2
import  glob
from matplotlib import pyplot as plt



plate_cascade = cv2.CascadeClassifier('co.xml')
chars_cascade = cv2.CascadeClassifier('coc2.xml')

files  = glob.glob('/home/ralvarez/Documentos/Dataset_placas/validate_chars_detection/*.png')

# files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/proyecto/PDIPlacas/carros/*.jpg')

for  file in files:
    img = cv2.imread(file)
    # img = cv2.blur(img,(3,3))
    img = cv2.medianBlur(img,3)
    #img = cv2.equalizeHist(img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    plates = plate_cascade.detectMultiScale(gray, 1.2, 5)
    for (x,y,w,h) in plates:
        
        img_crop = gray[y:y+h,x:x+w]
        img_crop_color = img[y:y+h,x:x+w,:]
        
        # cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
        chars = chars_cascade.detectMultiScale(img_crop, 1.05, 2)
        
        for (x1,y1,w1,h1) in chars:
            cv2.rectangle(img_crop_color,(x1,y1),(x1+w1,y1+h1),(0,0,255),1)
        plt.imshow(img_crop_color[:,:,::-1],'gray'),plt.title('ORIGINAL')
        plt.show()    
    
    # plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')
    # plt.show()

cv2.destroyAllWindows()


"""##
import numpy as np
import cv2
import  glob
from matplotlib import pyplot as plt

plate_cascade = cv2.CascadeClassifier('co.xml')
chars_cascade = cv2.CascadeClassifier('coc2.xml')

chars_cascade2 = cv2.CascadeClassifier('coc.xml')

files  = glob.glob('/home/ralvarez/Documentos/Dataset_placas/Cropped_Plates/*.jpg')

# files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/proyecto/PDIPlacas/carros/*.jpg')

for  file in files:

    img = cv2.imread(file)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    plates = chars_cascade.detectMultiScale(gray, 1.1, 1)
    for (x,y,w,h) in plates:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
    plt.subplot(121)
    plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')

    img = cv2.imread(file)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    plates = chars_cascade2.detectMultiScale(gray, 1.1, 1)
    for (x,y,w,h) in plates:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
    plt.subplot(122)
    plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')

    plt.show()    
cv2.destroyAllWindows()"""