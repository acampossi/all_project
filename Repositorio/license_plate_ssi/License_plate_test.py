from openalpr import Alpr
import numpy as np
import glob
import cv2
from matplotlib import pyplot as plt


a=np.array([1,2,3])
alpr = Alpr("co", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data")
alpr2 = Alpr("us", "/etc/openalpr/openalpr.conf", "/usr/share/openalpr/runtime_data")

# if not alpr.is_loaded():
#     print("Error loading OpenALPR")
#     sys.exit(1)
# alpr.set_top_n(20)
# alpr.set_default_region("md")

#results = alpr.recognize_file("h786poj.jpg")
files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/*.jpg')
for file in files:
    
    img = cv2.imread(file)
    plt.subplot(131),plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')
    
    results = alpr.recognize_file(file)['results']
    
    # for key, value in results.iteritems():
    #     print key

    #cord = results['coordinates']
    if results != []:
        print results[0]['coordinates']
        upper,lower=  results[0]['coordinates'][0],results[0]['coordinates'][2]

        img_record  = img[upper['y']:lower['y'],upper['x']:lower['x'],:]
        plt.subplot(132),plt.imshow(img_record[:,:,::-1],'gray'),plt.title('ORIGINAL')
    
    results = alpr2.recognize_file(file)['results']
    if results != []:
        print results[0]['coordinates']
        upper,lower=  results[0]['coordinates'][0],results[0]['coordinates'][2]

        img_record  = img[upper['y']:lower['y'],upper['x']:lower['x'],:]
        plt.subplot(133),plt.imshow(img_record[:,:,::-1],'gray'),plt.title('ORIGINAL')
    
    plt.show()
    

    #plt.subplot(122),plt.imshow(img[:,:,::-1],'gray'),plt.title('ORIGINAL')
    
alpr.unload()