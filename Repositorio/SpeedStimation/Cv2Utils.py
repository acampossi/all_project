import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import numpy as np
from matplotlib import pyplot as plt

def getPerspectiveTransform(img,InicialP,FinalP,Size):
    
    rows,cols,ch = img.shape

    pts1 = np.float32(InicialP)
    pts2 = np.float32(FinalP)
    M = cv2.getPerspectiveTransform(pts1,pts2)
    dst = cv2.warpPerspective(img,M,(30,450))
    plt.subplot(121),plt.imshow(img),plt.title('Input')
    plt.subplot(122),plt.imshow(dst),plt.title('Output')
    plt.show()


def getPerspectiveTransformParameters():
    
    Points=[]
    print('1 2 \n3 4')
    for i in range(4):
        Points.append([LeerEntero('x: '),LeerEntero('y: ')])
    return Points

def LeerEntero(text='n: '):
    return int(input(text))
    
def showMaximized():
    fig_manager = plt.get_current_fig_manager()
    if hasattr(fig_manager, 'window'):
        fig_manager.window.showMaximized()

def main():
    pass

if __name__ == "__main__":
   main()