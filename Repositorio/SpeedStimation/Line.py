import os
import numpy as np
from pylab import ginput
import sys
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
#sys.path.append('/usr/local/python/2.7')
import cv2
import Cv2Utils
import math
import Cv2Utils
import multiprocessing
import time
import tkFileDialog
class ColorLine:
    SamplesCount = 0
    PosNewSample = 0
    def __init__(self, points, thd, numChannels=3, limit=100):
        self.NumChannels = numChannels
        self.Points = points
        self.Fitvars = np.polyfit(self.Points[:, 1], self.Points[:, 0], 1)
        self.Limit = limit
        self.PixelsCount = int(self.Points[1, 1]-self.Points[0, 1])
        self.Samples = np.zeros((self.PixelsCount, self.Limit, self.NumChannels))
        self.NewSample = np.zeros((self.PixelsCount, 1, self.NumChannels))
        self.Mean = np.zeros((self.PixelsCount, 1, self.NumChannels))
        self.Dev = np.zeros((self.PixelsCount, 1, self.NumChannels))
        self.Std = np.zeros((self.PixelsCount, 1, self.NumChannels))
        self.PixelsPositions = np.zeros((self.PixelsCount, 2), dtype=np.int)
        self.InitializePixelsPositions()
        self.Thd = thd
        self.Med=[]
        self.AdapThd=0
    def InitializePixelsPositions(self):
        cnt = 0
        for i in range(int(self.Points[0, 1]), int(self.Points[1, 1])):
            y = self.Fitvars[0]*i+self.Fitvars[1]
            self.PixelsPositions[cnt, :] = [y, i]
            cnt = cnt+1
    

    def takeNewSample(self, frame):
        for i in range(self.PixelsCount):
            self.NewSample[i, 0, :] = frame[self.PixelsPositions[i, 1],self.PixelsPositions[i, 0], :]
            
    def InitializeData(self, frame):
        self.takeNewSample(frame)
        self.Samples[:, self.PosNewSample, 0] = self.NewSample[:, 0, 0]# aqui PosNewSample va en 0
        self.Samples[:, self.PosNewSample, 1] = self.NewSample[:, 0, 1]
        self.Samples[:, self.PosNewSample, 2] = self.NewSample[:, 0, 2]
        self.Mean = self.NewSample
        self.PosNewSample = self.PosNewSample+1
        self.SamplesCount = self.SamplesCount+1
        

    def GuassianModelStimation(self, thd):
        self.Lastmed = -1
        if self.SamplesCount > 30:
            Result = np.logical_and(self.NewSample >= self.Mean - self.Std*2, self.NewSample <= self.Mean + self.Std*2)
            self.Lastmed = 100*np.sum(Result)/(self.PixelsCount*self.NumChannels)
            self.Med.append(self.Lastmed)
            if self.Lastmed >= thd-self.AdapThd:
                self.AdapThd = 0
                if self.SamplesCount < self.Limit:
                    self.MeanAndDesviation()
                else:
                    self.SlidingMeanAndDesviation()
                self.SamplesCount = self.SamplesCount+1
                self.PosNewSample = self.PosNewSample+1
                if self.PosNewSample >= self.Limit:
                    self.PosNewSample = 0
            else:
                self.AdapThd = self.AdapThd+1
        else:
            self.MeanAndDesviation()
            self.SamplesCount = self.SamplesCount+1
            self.PosNewSample = self.PosNewSample+1
            if self.PosNewSample >= self.Limit:
                self.PosNewSample = 0
            self.Med.append(self.Lastmed)
    # mediana y desviacion sin desplazamiento
    def MeanAndDesviation(self):
        n=self.SamplesCount
        Sn = self.Dev*(n-1)
        Mn = n*self.Mean
        self.Mean = (Mn+self.NewSample)/(n+1)
        Sn1 = Sn+((self.NewSample*n-Mn)**2)/(n*(n+1))
        self.Dev = Sn1/n
        self.Std = np.sqrt(self.Dev)
        self.Samples[:, self.PosNewSample, 0] = self.NewSample[:, 0, 0]
        self.Samples[:, self.PosNewSample, 1] = self.NewSample[:, 0, 1]
        self.Samples[:, self.PosNewSample, 2] = self.NewSample[:, 0, 2]


    def SlidingMeanAndDesviation(self):
        nm = self.Limit
        Sn = self.Dev*(nm-1)
        Mn = nm*self.Mean
        xm = self.Samples[:, self.PosNewSample, :].reshape((self.PixelsCount, 1, self.NumChannels))
        un1 = (Mn+self.NewSample-xm)/(nm)
        self.Dev = (Sn+ (self.NewSample-un1)**2 -(xm-un1)**2+nm*(self.Mean-un1)**2)/(nm-1)
        self.Mean = un1
        self.Std = np.sqrt(self.Dev)
        self.Samples[:, self.PosNewSample, 0] = self.NewSample[:, 0, 0]
        self.Samples[:, self.PosNewSample, 1] = self.NewSample[:, 0, 1]
        self.Samples[:, self.PosNewSample, 2] = self.NewSample[:, 0, 2]


def ginputToNumpy(a):
    result = np.zeros((len(a), 2))
    for i in range(len(a)):
        result[i, :] = [int(a[i][0]), int(a[i][1])]
    return result

def worker(item, frame):
    item.takeNewSample(frame)
    item.GuassianModelStimation(0.5)

if __name__ == '__main__':
    show = True #False 
    os.system('cls' if os.name == 'nt' else 'clear')
path1=tkFileDialog.askdirectory(initialdir='/home/ssi_ralvarez')
a = os.listdir(path1)
print a
plt.ion()
for path in a:
    Folder_data = '/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/a'
    Path = path[1:]
    print(Path)
    videoPath = Folder_data+Path+'/video'+Path+'.mp4'
    cap = cv2.VideoCapture(videoPath)
    cap.set(1, 0)
    ret, frame = cap.read()
    #    frame =cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
    if show:
        plt.imshow(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
    n=int(input('Ingrese el numero de lineas a utilizar: '))
    n = 6
    Lines = []
    p = np.array([[[315, 261], [67, 692]],
                    [[417, 263], [283, 683]],
                    [[533, 260], [512, 687]],
                    [[653, 263], [745, 691]],
                    [[759, 259], [974, 689]],
                    [[859, 257], [1176, 689]]])
    for i in range(n):
        a = p[i]
        a=ginputToNumpy(plt.ginput(2))
        Line1 = ColorLine(a, 80, 3, 150)
        Line1.InitializeData(frame)
        Lines.append(Line1)
        if show:
            print a
            plt.plot(Line1.PixelsPositions[:, 0], Line1.PixelsPositions[:, 1], 'ro', linewidth=1)
            plt.draw()
    plt.pause(5)
    cnt = 0
    if show:
        plt.figure()
  
    while(1):
        ret, frame = cap.read()
    #        frame =cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
        if ret == False:
            break
        for item in Lines:
            item.takeNewSample(frame)
            item.GuassianModelStimation(40)
        cnt = cnt+1
        if cnt%100 == 0:
            print(cnt, ' ', len(Lines[0].Med))
            for i in range(len(Lines)):
                np.save(Folder_data+Path+'/Speed'+str(i), Lines[i].Med)

        k = cv2.waitKey(30) & 0xff
        if k == 27:
            break

        if show:
            cv2.imshow('frame', frame)
            temp = 0
            if cnt%3 == 0 and len(Lines[0].Med) > 100:
                plt.cla()
                for item in Lines:
                    plt.plot((np.array(item.Med[-100:]) < 50)+temp)
                    temp = temp+1
                plt.grid()
                plt.draw()
                plt.show()
    for i in range(len(Lines)):
        np.save(Folder_data+Path+'/Speed'+str(i), Lines[i].Med)
cap.release()


