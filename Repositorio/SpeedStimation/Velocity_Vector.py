import os
import numpy as np
import matplotlib.pyplot as plt
import tkFileDialog

# pylint: disable=line-too-long

def calculate_velocity(file_path_string):
    # leer los datos de tiempo de camara
    spl1,spl2 = os.path.split(file_path_string)
    times=np.load(file_path_string+'/CameraTimes'+spl2[1:]+'.npy')


    # crear el vector donde se almacenara los datos,leo los datos, umbralizacion y almacenamiento en una matriz
    # definir el numero de marcas
    n = 6
    data_array = np.zeros((n+1, times.shape[0]))
    for i in range(n):
        d = np.load(file_path_string+'/Speed'+str(i)+'.npy')
        d = (d > 20000)
        data_array[i, :] = d

    dis = [0, 2] 
    n = 2
    data_array = np.zeros((n+1, times.shape[0]))
    d = np.load(file_path_string+'/Speed0.npy')
    d = (d > 20000)
    data_array[0, :] = d
    d = np.load(file_path_string+'/Speed5.npy')
    d = (d > 20000)
    data_array[1, :] = d

    """
    dis =[0 ,1.2 , 0.8]
    n = 3
    data_array = np.zeros((n+1, times.shape[0]))
    d = np.load(file_path_string+'/Speed0.npy')
    d = (d > 20000)
    data_array[0, :] = d
    d = np.load(file_path_string+'/Speed3.npy')
    d = (d > 20000)
    data_array[1, :] = d

    d = np.load(file_path_string+'/Speed5.npy')
    d = (d > 20000)
    data_array[2, :] = d
    """


    # tama;o del vector de velocidad
    steps_num = data_array.shape[1]
    flag = 0
    lim = n
    nivel = 0  # nivel que esta revisando por flanco ascendente

    vel_vector = np.zeros((times.shape[0], 1))
    vel_calculated = 1.8
    show = False
    for i in range(1, steps_num):
        data = data_array[:, i - 1:i+1]
        if nivel > 0:
            if data[nivel-1, 1] == 1:
                if (data[nivel, 1]-data[nivel, 0]) == 1:
                    vel_calculated = dis[nivel]/(times[i]-times[flag])
                    vel_vector[flag:i] = vel_calculated
                    flag = i
                    nivel = nivel+1
            else:
                nivel = nivel -1
        else:
            vel_calculated = 1.8
            if (data[0, 1]-data[0, 0]) == 1:
                nivel = 1
                flag = i
            else:
                nivel = 0
        vel_vector[i] = vel_calculated
        #print flag
        if show:
            plt.ion()
            plt.cla()
            for j in range(n):
                if i > 100:
                    plt.plot(range(i-100, i), data_array[j, i-100:i]+j)
                else:
                    plt.plot(data_array[j, 0:i]+j)

            plt.plot(i, nivel, 'ro')
            plt.grid()
            plt.draw()
            plt.show()



    vel_lim = 6
    #plt.plot(times,vel_vector)
    vel_vector[vel_vector>vel_lim] = vel_lim
    fil_size = 10
    filter_data = np.ones((fil_size, 1))/fil_size
    r = np.convolve(vel_vector[:, 0], filter_data[:, 0])
    
    r=r[:-fil_size+1]
    vel_vector = vel_vector.reshape( (np.product(vel_vector.shape),))
    r = r.reshape((np.product(r.shape),))
    vel_no_fil = np.zeros((r.shape[0],2))
    vel_no_fil[:,0]=times
    vel_no_fil[:,1]=vel_vector
    vel_fil = np.zeros((r.shape[0],2))
    vel_fil[:,0]=times
    vel_fil[:,1]=r
    plt.plot(times,vel_vector)
    plt.plot(times,r)
    #plt.plot(times,vel_vector)
    plt.grid()
    plt.draw()
    plt.show()
    return vel_no_fil, vel_fil




####################################
if __name__ == '__main__':
    DIR_LIST = ['a15_55_48', 'a12_6_9', 'a11_44_26', 'a12_37_50', 'a10_36_33', 'a15_23_54', 'a10_50_48',
                'a11_23_55', 'a15_34_13', 'a10_5_26', 'a13_29_8', 'a11_2_52', 'a15_13_8', 'a14_42_35',
                'a12_49_50', 'a13_13_33', 'a9_55_22', 'a9_52_12', 'a13_49_44', 'a11_13_49', 'a14_32_17',
                'a14_22_9', 'a14_11_54', 'a15_45_26', 'a13_0_38', 'a14_1_46', 'a11_34_4', 'a12_16_39',
                'a14_52_51', 'a11_55_14', 'a12_38_36', 'a9_24_20', 'a10_15_32', 'a13_39_24', 'a15_2_56',
                'a12_26_45', 'a10_26_30', 'a12_50_28']

    DATASET_DIR = '/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2'
    # seleccionar el direccitorio que contiene los datos
    #file_path_string = tkFileDialog.askdirectory(initialdir=DATASET_DIR)
    for dir_name in DIR_LIST:
        print dir_name  
        #try:
        vel_no_filtered, vel_filtered = calculate_velocity(DATASET_DIR+'/'+dir_name)
        np.save('/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/Velocidades/'+dir_name+'_nf', vel_no_filtered)
        np.save('/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/Velocidades/'+dir_name+'_f', vel_filtered)
        #except:
        #    print "fallido..."