import os
import sys
import cv2
import numpy as np
import Cv2Utils
import matplotlib.pyplot as plt
import tkFileDialog


class Background_Estimation:
    SamplesCount = 0
    PosNewSample = 0
    def __init__(self, points, thd, numChannels=3, limit=100):
        self.NumChannels = numChannels
        self.Points = points
        self.Fitvars = np.polyfit(self.Points[:, 1], self.Points[:, 0], 1)
        self.PixelsCount = int(self.Points[1, 1]-self.Points[0, 1])
        self.PixelsPositions = np.zeros((self.PixelsCount, 2), dtype=np.int)
        self.InitializePixelsPositions()
        self.Thd = thd
        self.Med=[]
        self.AdapThd=0
    def InitializePixelsPositions(self):
        cnt = 0
        for i in range(int(self.Points[0, 1]), int(self.Points[1, 1])):
            y = self.Fitvars[0]*i+self.Fitvars[1]
            self.PixelsPositions[cnt, :] = [y, i]
            cnt = cnt+1

    def takeNewSample(self, frame):
        cnt = 0        
        for i in range(self.PixelsCount):
            temp = frame[self.PixelsPositions[i, 1], self.PixelsPositions[i, 0]]
            cnt = cnt + temp
        self.Med.append(cnt)



if __name__ == '__main__':
    show = False #True #
    os.system('cls' if os.name == 'nt' else 'clear')
    Folder_data = '/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2'
    a = os.listdir(Folder_data)

    a= ['a10_36_33', 'a15_23_54', 'a10_50_48', 'a11_23_55', 'a15_34_13', 'a10_5_26', 'a13_29_8', 'a11_2_52',
        'a15_13_8', 'a14_42_35', 'a12_49_50', 'a13_13_33', 'a9_55_22', 'a9_52_12', 'a13_49_44', 'a11_13_49',
        'a14_32_17', 'a14_22_9', 'a14_11_54', 'a15_45_26', 'a13_0_38', 'a14_1_46', 'a11_34_4', 'a12_16_39',
        'a14_52_51', 'a11_55_14', 'a12_38_36', 'a9_24_20', 'a10_15_32', 'a13_39_24', 'a15_2_56', 'a12_26_45',
        'a10_26_30', 'a12_50_28']
#    a=['a12_37_50']
    print a
    plt.ion()
    fgbg = cv2.createBackgroundSubtractorMOG2(history=1000,varThreshold=25)
 
    for Path in a:
        Folder_data = '/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/a'
        print(Path)
        Path = Path[1:]
        videoPath = Folder_data+Path+'/video'+Path+'.mp4'
        cap = cv2.VideoCapture(videoPath)
        cap.set(1, 0)
        n = 6
        Lines = []
        p = np.array([[[315, 261], [67, 692]],
                        [[417, 263], [283, 683]],
                        [[533, 260], [512, 687]],
                        [[653, 263], [745, 691]],
                        [[759, 259], [974, 689]],
                        [[859, 257], [1176, 689]]])
        for i in range(n):
            a = p[i]
            Line1 = Background_Estimation(a, 80, 1, 150)
            Lines.append(Line1)
        cnt = 0
        if show:
            plt.figure()
        while(1):
            ret, frame = cap.read()
        #        frame =cv2.cvtColor(frame,cv2.COLOR_BGR2HSV)
            if ret == False:
                break
            fgmask = fgbg.apply(frame)
            for item in Lines:
                item.takeNewSample(fgmask)
            cnt = cnt+1
            if cnt%1000 == 0:
                print(cnt, ' ', len(Lines[0].Med))
                for i in range(len(Lines)):
                    np.save(Folder_data+Path+'/Speed'+str(i), Lines[i].Med)
            """
            k = cv2.waitKey(30) & 0xff
            if k == 27:
                break
            """        
            if show:
                cv2.imshow('frame', fgmask)
                if cnt%3 == 0 and len(Lines[0].Med) > 100:
                    plt.cla()
                    for item in Lines:
                        plt.plot((np.array(item.Med[-100:])))
                    plt.grid()
                    plt.draw()
                    plt.show()
        for i in range(len(Lines)):
            np.save(Folder_data+Path+'/Speed'+str(i), Lines[i].Med)
cap.release()
