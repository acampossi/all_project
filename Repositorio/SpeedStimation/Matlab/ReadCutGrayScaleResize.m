function Frame=ReadCutGrayScaleResize(frame,Limits,Size)

    Frame = double(rgb2gray(frame));
    L=Limits(1);R=Limits(2);
    T=Limits(3);B=Limits(4);
    step=floor(1/Size);
    Frame=Frame(T:step:B,L:step:R);
end