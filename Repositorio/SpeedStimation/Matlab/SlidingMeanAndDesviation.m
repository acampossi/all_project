function [un1,D1]=SlidingMeanAndDesviation(un,D,xn,xm,nm)
sn=D*(nm-1);
Mn=nm*un; % calculo Mn
Mn1=Mn+xn-xm; % calculo Mn+1
un1=Mn1/(nm); % calculo el promedio Un+1
sn1=sn+ (xn-un1).^2 -(xm-un1).^2+nm*(un-un1).^2;
D1=sn1/(nm-1);
end