clear all; clc;
% Read Video And initialize Variables
SpEs.Path='15_23_54'
SpEs.v = VideoReader(strcat('E:\SSI_Temp\a',SpEs.Path,'\video',SpEs.Path,'.avi'));
%SpEs.v.CurrentTime= 0;
SpEs.T=readNPY(strcat('E:\SSI_Temp\a',SpEs.Path,'\CameraTimes',SpEs.Path,'.npy'));
SpEs.nm=300;
show=1;
SpEs.T=SpEs.T-SpEs.T(1);
SpEs.thd=1/2;
Temp=1;
h = fspecial('average', 3)
%% leer primer frame y seleccionar puntos de interes usando ginput
frame=readFrame(SpEs.v);

frame=frame(250:700,50:1200,:);
frame=imfilter(frame,h);
SpEs.lengthh=size(frame,1)*size(frame,2);
imshow(frame,[]);
impixelinfo;
hold on;
a.Points=floor(ginput(2)); %[X Y]
a.fitvars = polyfit(a.Points(:,1),a.Points(:,2), 1);
plot(a.Points(:,1),a.Points(:,2),'m','LineWidth',3);
a.SamplesCount=0;
a.LastPos=0;
a.limit=120;
a.Samples=zeros(a.Points(2)-a.Points(1)+1,a.limit,3);
a.New=zeros(a.Points(2)-a.Points(1)+1,1,3);
a.mean=zeros(size(a.New));
a.Dev=zeros(size(a.New));
a.std=zeros(size(a.New));

Temp=1;
for i=a.Points(1):a.Points(2)-1
y=floor(a.fitvars(1)*i+a.fitvars(2));
a.Pos(Temp,:)=[i y];
a.New(Temp,:)=frame(y,i,:);
Temp=Temp+1;
end

[a,med]=GaussianModelStimation(a,SpEs.thd);

%% Ciclo  que termina cuando se acaban los frames
MedTemp=1;
while hasFrame(SpEs.v)
frame=readFrame(SpEs.v);
frame=frame(250:700,50:1200,:);
frame=imfilter(frame,h);
for i=1:size(a.Pos,1)
a.New(i,:)=frame(a.Pos(i,2),a.Pos(i,1),:);
end
[a,med(MedTemp)]=GaussianModelStimation(a,SpEs.thd);
MedTemp=MedTemp+1;
%% display images
if (show==1)
    subplot(3,1,1:2);
    imshow(frame,[]);   
    %title(num2str(med(MedTemp-1)));
    hold on;
    plot(a.Points(:,1),a.Points(:,2),'m','LineWidth',3);
    hold off;
    subplot(3,1,3);
    if mod(MedTemp,1)==0
    plot(100*med/3);
    grid on;
    end
    drawnow
end

end