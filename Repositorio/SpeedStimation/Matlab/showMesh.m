% clear all; clc;
% a=readNPY('E:\SSI_Temp\a13_50_32\data13_50_32.npy');
% T=a(:,1);
% Data=a(:,2:end);
% Angle=[-45:0.25:225];
% X=bsxfun(@times,Data,cosd(Angle));
% Y=bsxfun(@times,Data,sind(Angle));
% Z=meshgrid([1:4560]*2/40,1:1081)';
% scatter3(X(1:10:end),Y(1:10:end),Z(1:10:end))
Data=1;
mr(1)=Data(1);
sr(1)=0;
n=1;
nm=10;
for i=2:nm
Data(i)=i;
m(i)=mean(Data);
s(i)=std(Data)^2;
[mr(i),sr(i)]=MeanAndDesviation(mr(i-1),sr(i-1),Data(i),n);
n=n+1;
end
for i=nm+1:100
Data(i)=i;
DataL=Data(end-10+1:end);
m(i)=mean(DataL);
s(i)=std(DataL)^2;
[mr(i),sr(i)]=SlidingMeanAndDesviation(mr(i-1),sr(i-1),DataL(end),Data(end-10),nm);
n=n+1;
end


%%
clear all; clc;
SpEs.Path='14_52_51'
SpEs.v = VideoReader(strcat('E:\SSI_Temp\a',SpEs.Path,'\video',SpEs.Path,'.avi'));
SpEs.T=readNPY(strcat('E:\SSI_Temp\a',SpEs.Path,'\CameraTimes',SpEs.Path,'.npy'));
frame1=readFrame(SpEs.v);
frame1=frame1(250:700,50:1200,:);
frame2=readFrame(SpEs.v);
frame2=frame2(250:700,50:1200,:);
imshow(sum(abs(frame2-frame1),3)>30)
