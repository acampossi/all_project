function [un1,D1]=MeanAndDesviation(un,D,x,n)
if (n~=0)
sn=D*(n-1);
Mn=n*un; % calculo Mn
Mn1=Mn+x; % calculo Mn+1
un1=Mn1/(n+1); % calculo el promedio Un+1
sn1=sn+ ((n*x-Mn).^2)/(n*( n + 1 ));
D1=sn1/n;
else
un1=x;
D1=0;
end
end