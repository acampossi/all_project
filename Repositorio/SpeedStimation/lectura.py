import numpy as np
import matplotlib.pyplot as plt
import tkFileDialog
from mpl_toolkits.mplot3d import Axes3D

def show_points_cloud(f_data):
	f_data[:, 2]=-f_data[:, 2]
	plt.figure(2,figsize=(9,7))
	ax2=plt.gca(projection='3d')
	plt.scatter( -f_data[:, 2],f_data[:, 0],zs=-f_data[:, 1],c=f_data[:, 2],s=5)
#	plt.axis('equal')
	plt.draw()
	plt.show()


print("Seleccione una opcion: ")
print("1. Visualizar Tiempos de Camara ")
print("2. Visualizar Tiempos del laser: ")
print("3. Imprimir Primero y ultimo: ")
print("4. Visualizar datos de estimacion de velocidad ")
print("5. Visualizar nube de puntos ")

n= int(input("n: "))
	
if n==1:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)

	#plt.plot(d)
	plt.plot(d[2:]-d[1:-1])

	    #plt.plot(x,y)
	plt.xlabel('Altura con respecto al hokuyo')
	plt.ylabel('Distancia vertical entre puntos')
	plt.grid()
	plt.show()
if n==2:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)

	#plt.plot(d)
	plt.plot(d[2:,0]-d[1:-1,0])

	    #plt.plot(x,y)
	plt.xlabel('Altura con respecto al hokuyo')
	plt.ylabel('Distancia vertical entre puntos')
	plt.grid()
	plt.show()

if n==3:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)
	print("Primer tiempo Camara: ", d[0])
	print("ultimo tiempo Camara: ", d[-1])

	file_path_string = tkFileDialog.askopenfilename()
	d=np.load(file_path_string)

	#plt.plot(d)
	print("Primer tiempo Laser: ", d[0,0])
	print("ultimo tiempo laser: ", d[-1,0])

if n==4:
	file_path_string = tkFileDialog.askopenfilename(initialdir='/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2')

	d=np.load(file_path_string)

	#plt.plot(d)
	plt.plot(d)

	    #plt.plot(x,y)
	plt.xlabel('Numero de muestra')
	plt.ylabel('coincidencia')
	plt.grid()
	plt.show()



if n==5:

	file_path_string = tkFileDialog.askopenfilename(initialdir='/home/ssi_ralvarez/Documentos/all_project/Repos/JHON_ROBOT/Clusters')
	d=np.loadtxt(file_path_string,skiprows=11)
	show_points_cloud(d)






