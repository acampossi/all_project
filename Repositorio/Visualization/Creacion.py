import pcl
import math
import numpy as np

class Point_Cloud:
	 """
	 Creando
	 """

	 def __init__(self, delta, aux0, slope, interval, veloc):
	 	"""
	 	Regresa un objeto Point_cloud con los siguientes parametros:

	 	total_range = rango de lo datos medidos por haz del laser (angulo * delta) por nube de puntos
	 	delta = resolucion angular del laser
	 	numero de medidas = numero de haz del laser capturados (cada 0.25 segundos)

	 	grados = vector auxiliar para calcular los angulos correspondientes a cada medida del laser
	 	aux0 = angulo inicial

	 	x_vector = Vector auxiliar con los valores en el eje "X" de cada medida
	 	y_vector = Vector auxiliar con los valores en el eje "Y" de cada medida
	 	z_vector = Vector auxiliar con los valores en el eje "Z" de cada medida

	 	x_Matriz = Matriz final con los valores en el eje "X" de la nube de puntos
	 	y_Matriz = Matriz final con los valores en el eje "Y" de la nube de puntos
	 	z_Matriz = Matriz final con los valores en el eje "Z" de la nube de puntos

	 	velocity = Variablecon las velocidades *cambiar estaa descripcion*



	 	"""
	 	#self.data_laser = data_laser
	 	self.total_range = 1081
	 	self.delta = delta
	 	#self.numero_medidas = data_laser.shape[0]

	 	#self.vel_data = vel_data

	 	self.grados = np.zeros(shape=(self.total_range,1))
	 	self.aux0 = aux0

	 	self.x_vector = np.zeros(shape=(self.total_range,1))
	 	self.y_vector = np.zeros(shape=(self.total_range,1))
	 	self.z_vector = np.zeros(shape=(self.total_range,1))

	 	#self.x_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))
	 	#self.y_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))
	 	#self.z_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))

	 	#Cambiar este entero por el vector o variable a utilizar finalmente
	 	self.velocity = veloc
	 	print self.velocity

	 	self.slope = slope
	 	self.interval = interval

	 	self.original_pointcloud = pcl.PointCloud()
	 	self.filtered_pointcloud_d = pcl.PointCloud()
	 	self.plane_pointcloud = pcl.PointCloud()
	 	self.without_plane_pointloud = pcl.PointCloud()
	 	print "Inciando"
	 	self.final_route = ""

	 	self.error = False

	 	self.TanAngle = 0.0000
	 	self.Angle = 0.0000

	 	#self.grow_velocity()
	 	self.Angular_Vector_Create()

	 def grow_velocity(self):
	 	auxiliar_vel = 0.01
	 	k = 0
	 	for i in range(0,self.numero_medidas):
			if (self.data_laser[i,0] <= self.vel_data[k,0]):
				self.velocity[i] = auxiliar_vel
			else:
				auxiliar_vel = self.vel_data[k,1]
				self.velocity[i] = auxiliar_vel
				k = k+1



	 def Angular_Vector_Create(self):
	 	"""
	 	Crea el vector de grados para la nube de puntos

	 	"""
	 	aux = self.aux0
		for i in range(0, self.total_range):
			self.grados[i] = ((math.pi*(aux))/180.0)
			aux=aux+self.delta


	 def Put_Laser(self, data_laser, vel_data):
	 	self.error = False
	 	self.data_laser = data_laser
	 	self.numero_medidas = data_laser.shape[0]
	 	self.vel_data = vel_data

	 	self.x_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))
	 	self.y_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))
	 	self.z_matriz = np.zeros(shape=(self.numero_medidas*self.total_range,1))

	 	self.original_pointcloud = pcl.PointCloud(self.x_matriz.shape[0])

	 def Point_cloud_Create(self, name, save):
	 	"""
	 	Crea la nube de puntos a partir de las medidas de laser dadas

		save = indica si se guarda en disco la nbe de puntos
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	z_prev = 0
		for i in range(0,self.numero_medidas):
			for j in range(0,(self.total_range)):
				self.x_vector[j] = (self.data_laser[i,j+1]/1000.0)*math.cos(self.grados[j])
				self.y_vector[j] = (self.data_laser[i,j+1]/1000.0)*math.sin(self.grados[j])
				self.z_vector[j] = z_prev+self.velocity*(1.0/40.0)*(-1)

				self.y_vector[j] = self.y_vector[j]-(self.slope*self.x_vector[j])-self.interval
			z_prev = self.z_vector[j]
			self.x_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.x_vector
			self.y_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.y_vector
			self.z_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.z_vector

		matriz_final = np.asarray(self.original_pointcloud)
		matriz_final[:,0] = self.x_matriz[:,0]
		matriz_final[:,1] = self.y_matriz[:,0]
		matriz_final[:,2] = self.z_matriz[:,0]

		if (save): 
			self.original_pointcloud.to_file(name[0:-4]+".pcd")

	 def Filter_distance(self, input_pointcloud, x_limit, y_limit, name, save):
	 	"""
		Lleva a cabo un filtrado de distancias a partir de los limites dados en "x" y "y"

		x_limit = limites en el eje x
		y_limit = limites en el eje y
		save = indica si se guarda en disco la nbe de puntos filtrada
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	#self.original_pointcloud = pcl.load(name[0:-4]+".pcd")
	 	filter_pass = input_pointcloud.make_passthrough_filter()
	 	filter_pass.set_filter_field_name("x")
		filter_pass.set_filter_limits(x_limit[0], x_limit[1])
		self.filtered_pointcloud_d = filter_pass.filter()

		filter_pass2 = self.filtered_pointcloud_d.make_passthrough_filter()
		filter_pass2.set_filter_field_name("y")
		filter_pass2.set_filter_limits(y_limit[0], y_limit[1])
		self.filtered_pointcloud_d = filter_pass2.filter()	

		if (save): 
			self.filtered_pointcloud_d.to_file(name[0:-4]+"_filtered.pcd")
			self.final_route = name[0:-4]+"_NoGround"


	 def Ground_extraction_Normal(self, filtered_pointcloud, name, save):
	 	segment_ext = filtered_pointcloud.make_segmenter_normals(ksearch=50)
	 	segment_ext.set_optimize_coefficients(True)
	 	segment_ext.set_model_type(pcl.SACMODEL_PERPENDICULAR_PLANE)
	 	segment_ext.set_method_type(pcl.SAC_RANSAC)
	 	segment_ext.set_eps_angle(30.0 * (math.pi/180.0))
	 	segment_ext.set_axis(0.0, 1.0, 0.0)
	 	segment_ext.set_max_iterations(300)
	 	segment_ext.set_distance_threshold(0.05)

	 	nr_points = int(filtered_pointcloud.size)


	 	#while ((nr_points*0.4)>(self.without_plane_pointloud.size)):
 		indices, model = segment_ext.segment()
 		if (len(indices)==0):
 			print "No se pudo calcular un modelo del suelo"

 		print "El modelo del suelo es:", model
 		self.plane_pointcloud = filtered_pointcloud.extract(indices,negative=False)
 		self.without_plane_pointloud = filtered_pointcloud.extract(indices,negative=True)

	 	if (save and (self.without_plane_pointloud.size>0)):
	 		self.without_plane_pointloud.to_file(name[0:-4]+"_NoGround.pcd")
	 		self.final_route = name[0:-4]+"_NoGround"
	 	else:
	 		self.error = True


	 def CalculateAngle(self, TanAngleIn):
		self.Angle= math.atan2(TanAngleIn,1)
		angleTD = math.degrees(self.Angle)



