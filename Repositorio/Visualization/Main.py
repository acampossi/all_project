#!/usr/bin/python
import time
import pcl#
import math
import numpy as np
import datetime
import tkFileDialog
import Creacion
import glob
import glob
import os,select,sys
import cv2
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg


def leer_datos_filtrar():
	print("Seleccione el archivo de laser original ")
	file_path_string = tkFileDialog.askopenfilename()
	File = os.path.split(file_path_string)[1]
	Ruta = "Preprocesados/"+File[0:-4]

	print("Seleccione un archivo de parametros ")
	file_path_string3 = tkFileDialog.askopenfilename()
	parameters = np.loadtxt(file_path_string3, skiprows=1, usecols=(1,))


	x_limit = np.array([parameters[0], parameters[1]])
	y_limit = np.array([parameters[2], parameters[3]])

	new_p = Creacion.Point_Cloud(-0.25, 135, parameters[4], parameters[5], parameters[9])

	data_laser = np.load(file_path_string)
	new_p.Put_Laser(data_laser, 0)
	new_p.Point_cloud_Create(file_path_string, True)
	new_p.Filter_distance(new_p.original_pointcloud, x_limit, y_limit, file_path_string, True)
	new_p.Ground_extraction_Normal(new_p.filtered_pointcloud_d,file_path_string,True)

	print "finalizado..."


def show_points_cloud_fixed_size(f_data):
	h,w = f_data.shape
	print "leido...."
	plt.ion()
	fig = plt.figure()
	#ax = fig.add_subplot(111, projection='3d')
	sp=5
	n=1000*5/sp
	diff=n*sp
	step=50
	idxmax=n*sp
	idxmin=idxmax-diff
	while idxmax<h:
	#	plt.subplot(121)
		plt.cla()
		plt.scatter( -f_data[idxmin:idxmax :sp, 2],-f_data[idxmin:idxmax :sp, 1],s=0.8)
	#	plt.xlim(math.fabs(f_data[idxmax, 2])-5,math.fabs(f_data[idxmax, 2]))
		plt.ylim(-2,0.5)
		plt.yticks([])
		plt.xticks([])	
		plt.set_yticklabels([])
		plt.set_xticklabels([])
		plt.set_zticklabels([])
		
		#	plt.axis('off')
		#	ax.axis('off')
		plt.draw()
		plt.show()
		idxmax=idxmax+step*sp
		idxmin=idxmax-diff
	#	plt.subplot(122)
	#	plt.cla()
	#	plt.plot(-f_data[idxmin:idxmax :sp, 2])




#

def show_points_cloud_fixed_width(f_data,times,video):
	fr =0
	cnt = fr
	video.set(1, fr)
	h,w = f_data.shape
	plt.ion()
	isp=2  # submuestreo de datos
	dis=3.5 # distancia a mostrar en cada despliegue
	idxmax=0	
	idxmin=0
	br=0
	f_data[:, 2]=-f_data[:, 2]
	ret, frame = video.read()
	plt.figure(1,figsize=(9,7)) #plt.subplot(121)
	ax=plt.imshow(frame[::2,::2,[0,1,2]])
	plt.figure(2,figsize=(9,7))
	ax2=plt.gca(projection='3d')
	ax2.spines['top'].set_visible(False)
	ax2.spines['right'].set_visible(False)
	ax2.spines['bottom'].set_visible(False)
	ax2.spines['left'].set_visible(False)
	
#	ax2.view_init(azim=90, elev=80)
	while (f_data[idxmax, 2])<1.6*times[cnt]:
		idxmax=idxmax+1
		if idxmax>h:
			break
		
	while idxmax<h:
		cnt=cnt+1
		video.set(1, cnt)
		ret, frame = video.read()
		if ret==0:
			bre
		plt.figure(1) #plt.subplot(121)
		ax.set_data(frame[::2,::2,[2,1,0]])
		plt.yticks([])
		plt.xticks([])	
#		cv2.imshow('frame', frame)

		plt.figure(2) #plt.subplot(122)
		if times[cnt]*1.6>dis: 
			while (f_data[idxmax, 2]-f_data[idxmin, 2])>dis:
				idxmin=idxmin+1
		else:
			idxmin=0
		
		print (idxmax-idxmin)			
		if (idxmax-idxmin)<6140:
			sp=40
		else:
			sp=isp
		plt.cla()
		plt.scatter( -f_data[idxmin:idxmax :sp, 2],f_data[idxmin:idxmax :sp, 0],zs=-f_data[idxmin:idxmax :sp, 1],c=f_data[idxmin:idxmax :sp, 2],s=0.5)
		plt.axis('equal')
		plt.xlim(-f_data[idxmax, 2],-f_data[idxmin, 2])
		plt.ylim(2,5)
		ax2.set_zlim(-2,0.5)
		ax2.set_xticklabels([])
		ax2.set_yticklabels([])
		ax2.set_zticklabels([])
		plt.draw()
#		plt.show()

# 	plt.show()

		temp=f_data[idxmin, 2]
		
		while (f_data[idxmax, 2])<1.6*times[cnt]:
			idxmax=idxmax+1
			if idxmax>h:
				break
				br = 1
		if br:
			break




def show_image_fixed_time(video,z):
	
	t=z/1.6
	fr=int(t/30)
	video.set(1, fr)
	ret, frame = video.read()
	imshow( "Display window", image ); 


"""
f_data=np.load('data15_55_48.npy')
print(f_data[0,0])
plt.plot(f_data[1:,0]-f_data[:-1,0])
#plt.show()
print(times[0])
#plt.plot(times[1:]-times[:-1])
"""
#leer_datos_filtrar()
#f_data=np.loadtxt('data15_5_5.pcd',skiprows=11)
#f_data=np.loadtxt('data15_5_5_NoGround.pcd',skiprows=11)
#f_data=np.loadtxt('data15_55_48_NoGround.pcd',skiprows=11)

f_data=np.loadtxt('/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/a15_55_48/data15_55_48_filtered.pcd',skiprows=11)
#f_data=np.loadtxt('data15_55_48_NoGround.pcd',skiprows=11)

times=np.load('/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/a15_55_48/CameraTimes15_55_48.npy')
cap = cv2.VideoCapture('/home/ssi_ralvarez/Documentos/Base_toma_datos/Dataset/C_12_05_17/C_12_05_17-2/a15_55_48/video15_55_48.avi')
#cap = cv2.VideoCapture('video15_55_48.mp4')

show_points_cloud_fixed_width(f_data,times,cap)


#show_points_cloud_fixed_size(f_data)


