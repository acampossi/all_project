import cv2
import Hokuyo
import numpy as np
import time
import os,select,sys
import multiprocessing
import datetime
def get_image():
 retval, im = camera.read()
 return im

def create_directory(route):
	if not os.path.exists(route):
		os.makedirs(route)


def laserobt(Ini,Tiempo,limit):

    Laser=Hokuyo.Hokuyo_utm('type=serial,device=/dev/ttyACM0,timeout=1')
    data=[]
    
    print ("Tiempo ", Tiempo)
    while ((time.time()-Ini)<limit):
	    Laser.GetRangeScan()
	    T=time.time()-Ini
	    x=[]
	    x.append(T)
	    for i in range(1081):
	       x.append(Laser.Data.range(i))
	    data.append(x)
    #print(data)

    np.save('a'+Tiempo+'/data'+Tiempo,data)
    #np.savetxt('data4.txt',data)
    Laser.LaserDispose()
    #jk = np.load('data4npy')
    #print (jk[:,0])
    print "acabe laser"

def cameraobt(Ini,Tiempo,limit):
    camera_port = 1
    camera = cv2.VideoCapture(camera_port)
    camera.set(3,1280)
    camera.set(4,720)
    Times=[]
    Captures=[]

    video = cv2.VideoWriter('a'+Tiempo+"/video"+Tiempo+".avi", cv2.cv.CV_FOURCC('M','J','P','G'), 30.0,(1280, 720),True) 
    print ("Presione enter para deterner captura")
    while ((time.time()-Ini)<limit):
	    Times.append(time.time()-Ini)
	    retval, im = camera.read()
	    video.write(im)
        #Captures.append(im)

    print "acabe camara"
    now = datetime.datetime.now()
    np.save('a'+Tiempo+'/CameraTimes'+Tiempo,Times)
    #np.savetxt('CameraTimes.txt',Times)
    #for i in range(len(Captures)):
        #video.write(Captures[i])

    video.release()
    cv2.destroyAllWindows()
    del(camera)
    print "acabe video"

limit=float(input("Ingrese tiempo de captura: "))
Ini=time.time()
now = datetime.datetime.now()
Tiempo=str(now.hour)+"_"+str(now.minute)+"_"+str(now.second)
create_directory('a'+Tiempo)
try:
	camera_p = multiprocessing.Process(name='camera', target=cameraobt, args=(Ini,Tiempo,limit))
	laser_p = multiprocessing.Process(name='laser', target=laserobt, args=(Ini, Tiempo,limit))
	laser_p.start()
	camera_p.start()
except:
   print "Error: unable to start thread"


while 1:
    if sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
    	line = raw_input()
    	break
   




