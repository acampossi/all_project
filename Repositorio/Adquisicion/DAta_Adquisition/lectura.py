import numpy as np
import matplotlib.pyplot as plt
import tkFileDialog

print("Seleccione una opcion: ")
print("1. Visualizar Tiempos de Camara ")
print("2. Visualizar Tiempos del laser: ")
print("3. Imprimir Primero y ultimo: ")
n= int(input("n: "))
	
if n==1:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)

	#plt.plot(d)
	plt.plot(d[2:]-d[1:-1])

	    #plt.plot(x,y)
	plt.xlabel('Altura con respecto al hokuyo')
	plt.ylabel('Distancia vertical entre puntos')
	plt.grid()
	plt.show()
if n==2:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)

	#plt.plot(d)
	plt.plot(d[2:,0]-d[1:-1,0])

	    #plt.plot(x,y)
	plt.xlabel('Altura con respecto al hokuyo')
	plt.ylabel('Distancia vertical entre puntos')
	plt.grid()
	plt.show()

if n==3:
	file_path_string = tkFileDialog.askopenfilename()

	d=np.load(file_path_string)
	print("Primer tiempo Camara: ", d[0])
	print("ultimo tiempo Camara: ", d[-1])

	file_path_string = tkFileDialog.askopenfilename()
	d=np.load(file_path_string)

	#plt.plot(d)
	print("Primer tiempo Laser: ", d[0,0])
	print("ultimo tiempo laser: ", d[-1,0])
