
#!/usr/bin/env python
import os
import hokuyoaist
from optparse import OptionParser
import math
from time import time
import numpy as np
import matplotlib.pyplot as plt


os.system('cls' if os.name=='nt' else 'clear')

class SingleScan: 
    data=[]
    time= None


class Hokuyo_utm:
    laser=None # laser object using the hokuyoaist library 
    Samples=[]  #Buffer  
    portParameters= None  #Hokuyo port name
    
    def __init__(self,PortName):
        self.laser=hokuyoaist.Sensor()
        self.portParameters=PortName
        self.laser.open(self.portParameters)
        self.laser.set_power(True)
#        self.laser.reset()
#        self.laser.set_motor_speed(0)
        self.Data=hokuyoaist.ScanData()
        
    def GetNewRangeScan(self):
        self.laser.get_new_ranges(self.Data, -1, -1, 1)
        
    def GetRangeScan(self):
        self.laser.get_ranges(self.Data, -1, -1, 1)

    def LaserDispose(self):
        self.laser.close()

if __name__ == "__main__":
    a=Hokuyo('type=serial,device=/dev/ttyACM0,timeout=1')

    inicial=time()
    #for i in range(100):
    a.GetRangeScan()
        #a.GetNewRangeScan()  tiene un mayor tiempo de retardo
    #    print(a.Data.range(1))
    x=[]
    y=[]
    d=[]
    angle=-45
    for i in range(1081):
        d.append(a.Data.range(i))
        x.append(a.Data.range(i)*math.cos(angle*math.pi/180))
        y.append(a.Data.range(i)*math.sin(angle*math.pi/180))
        angle+=0.25

    #final=time()
    #print (final-inicial)
    #print a.Data.as_string()
    np.save('d',d)
    a.LaserDispose()

    #print(a.Data.ranges_length())
    #print(a.Data.range(1))
    plt.plot(d)
    #plt.plot(x,y)
    plt.xlabel('Altura con respecto al hokuyo')
    plt.ylabel('Distancia vertical entre puntos')
    plt.grid()
    plt.show()
