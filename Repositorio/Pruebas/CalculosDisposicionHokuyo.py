import math
import matplotlib.pyplot as plt

d=100
A=440
Ah=100
Dmax=A-Ah

th=math.atan2(Dmax,d)
dth=0.25*math.pi/180
Dis=[]
H=[]
while th>=0: 
   dis=d*(math.tan(th)-math.tan(th-dth))
   H.append(d*(math.tan(th)))
   Dis.append(dis)
   th+=-dth
for i in range(len(Dis)):
    print(H[i]/100,' , ',Dis[i])

plt.plot(H,Dis)

plt.xlabel('Altura con respecto al hokuyo')
plt.ylabel('Distancia vertical entre puntos')

plt.show()
