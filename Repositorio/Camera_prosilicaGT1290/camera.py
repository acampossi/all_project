
"""
Created on Mon Jul 07 14:59:03 2014
@author: derricw
Same as the other liveview example, but displays in color.
Obviously you want to use a camera that has a color mode like BGR8Packed
OpenCV is expecting color images to be in BGR8Packed by default.  It can work
    with other formats as well as convert one to the other, but this example
    just uses its default behavior.
"""


from __future__ import absolute_import, print_function, division
from pymba import *
import numpy as np
import cv2
import time
import sys
import logging

CARRILES = ["Izquierdo","Derecho"]
AUTO_AJUST = {0:"Off" , 1:"Once" , 2:"Continuous"}
class Cameras():
    """
    Objeto que permite la deteccion, apertura, captura de imagenes y cierre de las camaras AlliedVision GIGE
    la clase esta basada  en el API Vimba desarrollada en C  y su respectivo envoltorio para python pymba
    Atributos:
        camerasIds:  Id de las camaras existenes 
        Cameras_Dic: Diccionario que contiene las instancias de las camaras
    """
    camerasIds = None
    Cameras_Dic = {}
    Frames = {}
    
    def __init__(self):
        self.RunVimba()
    def RunVimba(self):
        self.vimba = Vimba()
        self.vimba.startup()
        self.system = self.vimba.getSystem()
        

    def detectCameras(self):
        """        Detecta las camaras conectadas y guarda sus ID en la variable camerasIds"""
        
        if self.system.GeVTLIsPresent:
            self.system.runFeatureCommand("GeVDiscoveryAllOnce")
            time.sleep(0.2)
        self.cameraIds = self.vimba.getCameraIds()    

    def GetCamerasId(self):
        """Detecta las camaras conectadas, guarda y retorna el valor encontrado"""
        
        self.detectCameras()
        return self.cameraIds

    def openCameras(self):
        """ Abre la conexion con todas las camaras disponebles"""
        self.detectCameras()
        for cameraId in self.cameraIds:
            self.OpenCamera(cameraId)
            
    def OpenCamera(self,cameraId):
        """ Abre la conexion con la camara con el id entregado si este existe
        Atributos:
        camerasIds:  Id de las camara 
        """
        if self.cameraIds != None:
            if cameraId in self.cameraIds:
                camera = self.vimba.getCamera(cameraId)
                camera.openCamera()
                camera.PixelFormat = "BGR8Packed"  # OPENCV DEFAULT
                frame = camera.getFrame()
                frame.announceFrame()
                camera.startCapture()
                camera.runFeatureCommand("AcquisitionStart")
                camera.Carril=CARRILES[0]
                self.Cameras_Dic[cameraId]=camera
                self.Frames[cameraId] =frame


    def GetCamera(self,cameraId):
        """ Retorna la instancia de la camara con el id entregado si este existe
        Atributos:
        camerasIds:  Id de las camara 
        """
        if self.cameraIds != None:
            if cameraId in self.cameraIds:
                return self.Cameras_Dic[cameraId] 


    def getFrame(self,cameraId):
        """ Obtiene un frame de  la camara con el Id entregado
        Atributos:
        camerasIds:  Id de las camara 
        """
        img = None
        success = False    
        if cameraId in self.Cameras_Dic:
            #frame = self.Cameras_Dic[cameraId].getFrame()
            self.Frames[cameraId].queueFrameCapture()
            success =True
            # except:
            #     return success,None
            cam = self.Cameras_Dic[cameraId]
            self.Frames[cameraId].waitFrameCapture(1000)
            frame_data = self.Frames[cameraId].getBufferByteData()
            #print (len(frame_data),frame_data[0],cam.Width,cam.Height,cam.OffsetX,cam.OffsetY)
            img = np.ndarray(buffer=frame_data,
                                dtype=np.uint8,
                                shape=(cam.Height, cam.Width, self.Frames[cameraId].pixel_bytes))
            return success,img
        else:
            return success,None
    
    def ConfigureCamera(self,cameraId,Values):
        if cameraId in self.Cameras_Dic:
            Camera = self.Cameras_Dic[cameraId]
            Camera.Carril = Values["Carril"] 
            Camera.OffsetX = Values["OffsetX"]
            Camera.OffsetY = Values["OffsetY"]
            Camera.Width = Values["Width"]
            Camera.Height = Values["Height"]
            Camera.ExposureAuto = Values["ExposureAuto"]
            Camera.ExposureAutoRate = Values["ExposureAutoRate"]
            Camera.GainAuto = Values["GainAuto"]
            Camera.GainAutoRate = Values["GainAutoRate"]
            Camera.BalanceWhiteAuto = Values["BalanceWhiteAuto"]
            Camera.BalanceWhiteAutoRate = Values["BalanceWhiteAutoRate"]
         

    def SetCameraExposure(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].ExposureAuto= AUTO_AJUST[value]

    def GetCameraExposure(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].ExposureAuto)]


    def SetCameraGain(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].GainAuto= AUTO_AJUST[value]

    def GetCameraGain(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].GainAuto)]


    def SetCameraBalanceWhite(self,cameraId,value):
        if value in AUTO_AJUST:
            if cameraId in self.Cameras_Dic:
                self.Cameras_Dic[cameraId].BalanceWhiteAutoAuto= AUTO_AJUST[value]

    def GetCameraBalanceWhite(self,cameraId):
        if cameraId in self.Cameras_Dic:
            return AUTO_AJUST.keys()[AUTO_AJUST.values().index(self.Cameras_Dic[cameraId].BalanceWhiteAuto)]


    def DisposeCamera(self,cameraId):
        """ Cierra la conexion con una camara
        Atributos:
        camerasIds:  Id de las camara 
        """
     
        if cameraId in self.Cameras_Dic:
            self.Cameras_Dic[cameraId].runFeatureCommand("AcquisitionStop")
            self.Cameras_Dic[cameraId].endCapture()
            self.Cameras_Dic[cameraId].revokeAllFrames()
            del self.cameraIds[cameraId]
    
    def Dispose(self):
        """ Cierra la conexion con todas camaras  y cierra la conexion con vimba"""
        for key in self.Cameras_Dic:
            self.Cameras_Dic[key].runFeatureCommand("AcquisitionStop")
            self.Cameras_Dic[key].endCapture()
            self.Cameras_Dic[key].revokeAllFrames()
        self.vimba.shutdown()
   
    def getLaneListType(self):
        return CARRILES

    def GetAutoAdjustIndex(self,value):
        return AUTO_AJUST.keys()[AUTO_AJUST.values().index(value)]

#############################################################################
#___________________________________________________________________________#
#############################################################################
if __name__ == "__main__":
    cv2.namedWindow("test")
    Cams =Cameras()
    Cams.openCameras()
    count = 1
    while 1:
        success, img = Cams.getFrame(Cams.cameraIds[0])
        if success:
            cv2.imshow("test", img)
            k = cv2.waitKey(1)
            if k == 0x1b:
                cv2.destroyAllWindows()
                break
            elif k%256 == 32:
                cv2.imwrite("image"+str(count)+".png", img)
                count = count+1
    Cams.Dispose()
    
"""
    # with Vimba() as vimba:
    #     system = vimba.getSystem()

    #     system.runFeatureCommand("GeVDiscoveryAllOnce")
    #     time.sleep(0.2)

    #     camera_ids = vimba.getCameraIds()

    #     for cam_id in camera_ids:
    #         print("Camera found: ", cam_id)

    #     c0 = vimba.getCamera(camera_ids[0])
    #     c0.openCamera()



    #     frame = c0.getFrame()
    #     frame.announceFrame()

    #     c0.startCapture()
    #     c0.runFeatureCommand("AcquisitionStart")

    #     framecount = 0
    #     droppedframes = []

    #     while 1:
    #         try:
    #             frame.queueFrameCapture()
    #             success = True
    #         except:
    #             droppedframes.append(framecount)
    #             success = False
    #         #c0.runFeatureCommand("AcquisitionStop")
    #         frame.waitFrameCapture(1000)
    #         frame_data = frame.getBufferByteData()
    #         if success:
    #             img = np.ndarray(buffer=frame_data,
    #                             dtype=np.uint8,
    #                             shape=(frame.height, frame.width, frame.pixel_bytes))
    #             cv2.imshow("test", img)
    #         framecount += 1
    #         k = cv2.waitKey(1)
    #         if k == 0x1b:
    #             cv2.destroyAllWindows()
    #             print("Frames displayed: %i" % framecount)
    #             print("Frames dropped: %s" % droppedframes)
    #             break


    #     c0.endCapture()
    #     c0.revokeAllFrames()
 
    #     c0.closeCamera()"""