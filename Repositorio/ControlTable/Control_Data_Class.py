import sqlite3
import time
import sys, select
from multiprocessing import Process, Pipe
import numpy as np 
class Vehicle_Classification_Data:


    def __init__(self, nube_puntos, tiempo_inicial, tiempo_final, carril, sentido):
        self.Nube_puntos = nube_puntos
        #NubePrep VARCHAR(255), "\
        #Caracteristicas VARCHAR(255), "\
        self.Fecha = time.strftime("%y-%m-%d")
        self.tiempo_inicial = tiempo_inicial
        self.tiempo_final = tiempo_final
        self.placa_vehiculo = None
        self._foto_path = None
        self.carril = carril
        self.sentido = sentido
        self.clase = -1
        self.esta_preprocesado = False
        self.esta_segmentado = False
        self.esta_clasificado = False
        self.esta_registrado = False



"""
def f(conn):
    start = time.time()
    conn.send(np.random.rand(1081,900,3))
    end = time.time()
    print "time: ",end - start
    conn.send(np.random.rand(1081,900,3))
    conn.send(np.random.rand(1081,900,3))
    conn.send(np.random.rand(1081,900,3))
    print 'acabe'
    conn.close()


if __name__ == '__main__':
    parent_conn, child_conn = Pipe()
 q = Queue()
    p = Process(target=f, args=(child_conn,))
    p.start()
    while  parent_conn.poll():
        start = time.time()
        temp = parent_conn.recv()   # prints "[42, None, 'hello']"
        end = time.time()
        print end - start
    p.join()
"""


from multiprocessing import Process, Queue

def f(q):
    print q.get()    # prints "[42, None, 'hello']"
    print q.get()    # prints "[42, None, 'hello']"

if __name__ == '__main__':
    q = Queue()
    start = time.time()
    q.put(np.random.rand(1081,1))
    end = time.time()
    print "time: ",end - start
    q.put(np.random.rand(1081,1))
    print "acabe"
    p = Process(target=f, args=(q,))
    p.start()
    time.sleep(1)
    p.join()