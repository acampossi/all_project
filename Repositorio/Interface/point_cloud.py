# -*- coding: utf-8 -*-
# vispy: gallery 10
# Copyright (c) 2015, Vispy Development Team.
# Distributed under the (new) BSD License. See LICENSE.txt for more info.

""" Demonstrates use of visual.Markers to create a point cloud with a
standard turntable camera to fly around with and a centered 3D Axis.
"""
import vispy.app as vapp
import numpy as np
import vispy.scene
from vispy.scene import visuals
import tkFileDialog
import os
#vapp.use_app("PyQt4")
#0
# Make a canvas and add simple view
#

if __name__ == '__main__':
	import sys
	path1=tkFileDialog.askdirectory(initialdir="/home/jhon-kevin/Documentos/Beta_Repo/beta_project/Adquisicion/Adquisicion_Hokuyo_Rplidar_bodega/guardar/Nubes")
	dir_list = os.listdir(path1)
	for file_path in dir_list:
		sp1,sp2 = os.path.split(file_path)
		print sp1, sp2
		print sp2[-13:]
		if sp2[-13:]  == "_filtered.pcd":
			canvas = vispy.scene.SceneCanvas(keys='interactive', show=True)
			print canvas.app
			view = canvas.central_widget.add_view()
			canvas.bgcolor = (0.5,0.5,0.5,1)
			#pos= np.loadtxt('/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/vispy/examples/basics/scene/Clusters/data15_55_48/data15_55_48_02_cluster_filtered0.pcd',skiprows=11)
			print("Seleccione un archivo laser ")
			#file_path_string = tkFileDialog.askopenfilename(initialdir = "/home/jhon-kevin/Documentos/Beta_Repo/beta_project/Adquisicion/Adquisicion_Hokuyo_Rplidar_bodega/guardar/Nubes")
			file_path_string = path1+"/"+file_path
			# data_laser = np.load(file_path_string)
			print file_path_string
			print ""
			pos= np.loadtxt(file_path_string,skiprows=11)

			# generate data
			#print a.shape
			#pos = np.random.normal(size=(100000, 3), scale=0.2)
			pos = pos[:,[1, 2, 0]]
			pos[:,1] = -pos[:,1]

			# one could stop here for the data generation, the rest is just to make the
			# data look more interesting. Copied over from magnify.py
			"""centers = np.random.normal(size=(50, 3))
			indexes = np.random.normal(size=100000, loc=centers.shape[0]/2.,
								scale=centers.shape[0]/3.)
			indexes = np.clip(indexes, 0, centers.shape[0]-1).astype(int)
			scales = 10**(np.linspace(-2, 0.5, centers.shape[0]))[indexes][:, np.newaxis]
			pos *= scales
			pos += centers[indexes]
			"""
			# create scatter object and fill in the data
			scatter = visuals.Markers()

			scatter.set_data(pos, edge_color=(1, 1, 1, 1), face_color=(1, 1, 1, .5), size=2)

			view.add(scatter)

			view.camera = 'arcball' #'turntable'  # or try 

			# add a colored 3D axis for orientation
			axis = visuals.XYZAxis(parent=view.scene)
			if sys.flags.interactive != 1:
					vispy.app.run()

