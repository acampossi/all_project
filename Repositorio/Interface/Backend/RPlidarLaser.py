import logging
import sys
import time
import codecs
import serial
import struct
import numpy as np

SYNC_BYTE = b'\xA5'
SYNC_BYTE2 = b'\x5A'

GET_INFO_BYTE = b'\x50'
GET_HEALTH_BYTE = b'\x52'
GET_SAMPLE_BYTE = b'\x59'

STOP_BYTE = b'\x25'
RESET_BYTE = b'\x40'

SCAN_BYTE = b'\x20'
EXPRESS_SCAN = b'\x82'
FORCE_SCAN_BYTE = b'\x21'

DESCRIPTOR_LEN = 7
# INFO_LEN = 20
# HEALTH_LEN = 3

# INFO_TYPE = 4
# HEALTH_TYPE = 6
# SCAN_TYPE = 129

#Constants & Command to start A2 motor
MAX_MOTOR_PWM = 1023
DEFAULT_MOTOR_PWM = 660
SET_PWM_BYTE = b'\xF0'

_HEALTH_STATUSES = {
    0: 'Good',
    1: 'Warning',
    2: 'Error',
}


class RPLidarException(Exception):
    """Basic exception class for RPLidar"""


def _b2i(byte):
    """Converts byte to integer (for Python 2 compatability)"""
    return byte if int(sys.version[0]) == 3 else ord(byte)

def _process_scan(raw):
    """Processes input raw data and returns measurment data"""
    new_scan = bool(_b2i(raw[0]) & 0b1)
    inversed_new_scan = bool((_b2i(raw[0]) >> 1) & 0b1)
    quality = _b2i(raw[0]) >> 2
    
    check_bit = _b2i(raw[1]) & 0b1
    if check_bit != 1:
        raise RPLidarException('Check bit not equal to 1')
    angle = ((_b2i(raw[1]) >> 1) + (_b2i(raw[2]) << 7)) / 64.
    distance = (_b2i(raw[3]) + (_b2i(raw[4]) << 8)) / 4.
    # if new_scan:
    #     print angle
    return new_scan, quality, angle, distance
SCAN_DSIZE =5

class RPLidar(object):
    """Class for communicating with RPLidar rangefinder scanners"""

    _serial_port = None  #: serial port connection
    port = ''  #: Serial port name, e.g. /dev/ttyUSB0
    timeout = 1  #: Serial port timeout
    motor = False  #: Is motor running?
    baudrate = 115200  #: Baudrate for serial port
    
    def __init__(self, port, baudrate=115200, timeout=1, logger=None):
        """Initilize RPLidar object for communicating with the sensor.
        Parameters
        ----------
        port : str
            Serial port name to which sensor is connected
        baudrate : int, optional
            Baudrate for serial connection (the default is 115200)
        timeout : float, optional
            Serial port connection timeout in seconds (the default is 1)
        logger : logging.Logger instance, optional
            Logger instance, if none is provided new instance is created
        """
        self._serial_port = None
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.motor_running = None
        if logger is None:
            logger = logging.getLogger('rplidar')
        self.logger = logger
        self.data = []
        self.scan_d = []
        self.velocity = []
        self.new_scan = False
        self.scans_processor = Scans_Processor()
    def connect(self):
        """Connects to the serial port with the name `self.port`. If it was
        connected to another serial port disconnects from it first."""
        if self._serial_port is not None:
            self.disconnect()
        try:
            self._serial_port = serial.Serial(
                self.port, self.baudrate,
                parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                timeout=self.timeout, dsrdtr=True)
        except serial.SerialException as err:
            raise RPLidarException('Failed to connect to the sensor '
                                   'due to: %s' % err)
        

    def is_open(self):
        if self._serial_port == None:
            return False
        else:
            return True
            

    def disconnect(self):
        """Disconnects from the serial port"""
        if self._serial_port is None:
            return
        self._serial_port.close()
        
    def set_pwm(self, pwm):
        assert(0 <= pwm <= MAX_MOTOR_PWM)
        payload = struct.pack("<H", pwm)
        self._send_payload_cmd(SET_PWM_BYTE, payload)

    def set_pwm_max(self):
        payload = struct.pack("<H", MAX_MOTOR_PWM)
        self._send_payload_cmd(SET_PWM_BYTE, payload)


    def start_motor(self):
        """Starts sensor motor"""
        if self._serial_port is None:
            return
        self.logger.info('Starting motor')
        # For A1
        self._serial_port.dtr = False

        # For A2
        self.set_pwm(DEFAULT_MOTOR_PWM)
        self.motor_running = True

    def stop_motor(self):
        """Stops sensor motor"""
        if self._serial_port is None:
            return
        self.logger.info('Stoping motor')
        # For A2
        self.set_pwm(0)
        time.sleep(.001)
        # For A1
        self._serial_port.dtr = True
        self.motor_running = False

    def _send_payload_cmd(self, cmd, payload):
        """Sends `cmd` command with `payload` to the sensor"""
        size = struct.pack('B', len(payload))
        req = SYNC_BYTE + cmd + size + payload
        checksum = 0
        for v in struct.unpack('B'*len(req), req):
            checksum ^= v
        req += struct.pack('B', checksum)
        self._serial_port.write(req)
        self.logger.debug('Command sent: %s' % req)

    def _send_cmd(self, cmd):
        """Sends `cmd` command to the sensor"""
        req = SYNC_BYTE + cmd
        self._serial_port.write(req)
        self.logger.debug('Command sent: %s' % req)

    def _read_descriptor(self):
        """Reads descriptor packet"""
        descriptor = self._serial_port.read(DESCRIPTOR_LEN)
        # self.logger.debug('Recieved descriptor: %s', descriptor)
        if descriptor.startswith(SYNC_BYTE + SYNC_BYTE2):
            dsize = _b2i(descriptor[2])
            is_single = _b2i(descriptor[-2]) == 0
            dtype = _b2i(descriptor[-1])
            derror = False
        else :
            # self.logger.info('Hubo algun error, revisar')
            dsize = 0
            is_single = False
            dtype = 0
            derror = True
        return dsize, is_single, dtype, derror


    def _read_response(self, dsize):
        """Reads response packet with length of `dsize` bytes"""
        self.logger.debug('Trying to read response: %d bytes', dsize)
        data = self._serial_port.read(dsize)
        self.logger.debug('Recieved data: %s', data)
        return data

    def get_info(self):
        """Get device information
        Returns
        -------
        dict
            Dictionary with the sensor information
        """
        self.clear_input()
        self._send_cmd(GET_INFO_BYTE)
        time.sleep(0.002)
        dsize, is_single, dtype, derror = self._read_descriptor()

        raw = self._read_response(dsize)
        serialnumber = codecs.encode(raw[4:], 'hex').upper()
        serialnumber = codecs.decode(serialnumber, 'ascii')
        data = {
            'model': _b2i(raw[0]),
            'firmware': (_b2i(raw[2]), _b2i(raw[1])),
            'hardware': _b2i(raw[3]),
            'serialnumber': serialnumber,
        }
        return data

    def get_health(self):
        """Get device health state. When the core system detects some
        potential risk that may cause hardware failure in the future,
        the returned status value will be 'Warning'. But sensor can still work
        as normal. When sensor is in the Protection Stop state, the returned
        status value will be 'Error'. In case of warning or error statuses
        non-zero error code will be returned.
        Returns
        -------
        status : str
            'Good', 'Warning' or 'Error' statuses
        error_code : int
            The related error code that caused a warning/error.
        """
        self.clear_input()
        self._send_cmd(GET_HEALTH_BYTE)
        time.sleep(0.002)
        dsize, is_single, dtype, derror = self._read_descriptor()

        raw = self._read_response(dsize)
        status = _HEALTH_STATUSES[_b2i(raw[0])]
        error_code = (_b2i(raw[1]) << 8) + _b2i(raw[2])
        return status, error_code

    def get_sample_rate(self):

        self.clear_input()
        self._send_cmd(GET_SAMPLE_BYTE)
        time.sleep(0.002)
        dsize, is_single, dtype, derror = self._read_descriptor()

        raw = self._read_response(dsize)
        sample_standar = (_b2i(raw[0]) << 8) + _b2i(raw[1])
        sample_express = (_b2i(raw[2]) << 8) + _b2i(raw[3])
        return sample_standar, sample_express

    def clear_input(self):
        """Clears input buffer by reading all available data"""
        self._serial_port.read_all()

    def stop(self):
        """ Stops scanning process, disables laser diode and the measurment
        system, moves sensor to the idle state. """
        self.logger.info('Stoping scanning')
        self._send_cmd(STOP_BYTE)
        time.sleep(.001)
        self.clear_input()

    def reset(self):
        """Resets sensor core, reverting it to a similar state as it has
        just been powered up."""
        self.logger.info('Reseting the sensor')
        self._send_cmd(RESET_BYTE)
        time.sleep(.002)

    def init_read_scans(self):

        self.clear_input()
        self._send_cmd(SCAN_BYTE)
        time.sleep(0.001)
        dsize, is_single, dtype, derror = self._read_descriptor()
        self.state = 0
        return dsize

    def read_scans(self,timeout =0.1):
        ini = time.time()
        exit_while = False
        while not exit_while:
            if time.time()-ini>timeout:
                return []
            buffersize =self._serial_port.in_waiting
            raws = self._serial_port.read(buffersize)
            for raw in raws:
                scan_l = self.scans_processor.processScan(raw)
                if scan_l != None:
                    new_scan, quality, angle, distance = scan_l
                    self.new_scan = new_scan
                    self.scan_d.append([quality, angle, distance])
                    if new_scan:
                        np_scan = np.array(self.scan_d)
                        self.scan_d = []
                        exit_while = True
        return np_scan

    def read_scans_hilos(self, dsize, veloc, timeC):
        # data_out = []
        limite = self._serial_port.in_waiting/dsize
        # print limite, self._serial_port.in_waiting
        raw = self._serial_port.read(dsize*limite)
        for j in range(0,limite-1):
            data_list = raw[j*dsize:(dsize*j)+(dsize)]
            scan_l = _process_scan(data_list)
            new_scan, quality, angle, distance = scan_l
            self.scan_d.append((quality, angle, distance))
            if new_scan:
                np_scan = np.array(self.scan_d)
                self.data.append(np_scan)
                self.velocity.append((veloc,timeC))
                self.scan_d = []

        return limite




class Scans_Processor:
     
    def __init__(self):
        self.state = 0
        self.quality =0
        self.angle = 0
        self.distance = 0
        self.new_scan = False
        self.angle_lower = 0
   
   
    def processScan(self,raw):
        """Processes input raw data and returns measurment data"""
        
        if self.state == 0:
            self.new_scan = bool(_b2i(raw) & 0b1)
            inversed_new_scan = bool((_b2i(raw) >> 1) & 0b1)
            self.quality = _b2i(raw) >> 2
            if self.new_scan != inversed_new_scan:
                self.state =1
        elif self.state ==1:
            
            check_bit = _b2i(raw) & 0b1
            if check_bit == 1:
                self.angle_lower = (_b2i(raw) >> 1) 
                self.state = 2
            else:
                self.angle = 0
                self.state = 0

        elif self.state == 2:
            self.angle = (self.angle_lower+ (_b2i(raw) << 7)) / 64.
            if self.angle<=360.:
                self.state = 3
            else:
                self.state = 1

        elif  self.state ==3 :
            self.distance_lower =_b2i(raw)
            self.state = 4

        elif self.state ==4:
            distance = (self.distance_lower + (_b2i(raw) << 8)) / 4.
            self.state = 0
            return self.new_scan, self.quality, self.angle, distance
 
        return None


