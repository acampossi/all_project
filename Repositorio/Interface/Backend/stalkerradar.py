import logging
import sys
import time
import codecs
import serial
import struct

"""CONSTANTES DE LA LIBRERIA PARA COMUNICACION CON RADAR: """

NO_RESPONSE = b'\xFF'

START_ID = b'\xEF'

DESTINATION_ID = b'\x02'

SOURCE_ID = b'\x01'

PACKET_TYPE = b'\x00'

ANTENNA_NUMBER = b'\x00'

PAYLOAD_LENGTH_LSB = b'\x03'

PAYLOAD_LENGTH_MSB = b'\x00'

SET_SENSIBILITY = b'\x84'
GET_SENSIBILITY = b'\x04'

SET_FINE_SENSIBILITY = b'\x86'
GET_FINE_SENSIBILITY = b'\x06'


GET_UNITS = b'\x14'
SET_UNITS = b'\x94'

GET_MIN_GAIN = b'\x31'
SET_MIN_GAIN = b'\xB1'

GET_MAX_GAIN = b'\x30'
SET_MAX_GAIN = b'\xB0'

LECTURE_FLAG = b'\x00'

LOGGER_DIC = {SET_SENSIBILITY : "SET_SENSIBILITY",
    GET_SENSIBILITY : "GET_SENSIBILITY",
    SET_FINE_SENSIBILITY : "SET_FINE_SENSIBILITY", 
    GET_FINE_SENSIBILITY : "GET_FINE_SENSIBILITY",
    GET_UNITS : "GET_UNITS",
    SET_UNITS : "SET_UNITS",
    GET_MIN_GAIN : "GET_MIN_GAIN",
    SET_MIN_GAIN : "SET_MIN_GAIN",
    GET_MAX_GAIN : "GET_MAX_GAIN",
    SET_MAX_GAIN : "SET_MAX_GAIN",
    LECTURE_FLAG : "LECTURE_FLAG"
    }



def _b2i(byte):
    """Convertir byte a entero (for Python 2 compatability)"""
    return byte if int(sys.version[0]) == 3 else ord(byte)

def int_2_byte(n):
    """Convertir  entero a byte"""
    bytes_list = list(struct.pack(">I", n))
    return bytes_list[3]

## Clase para la lectura y configuracion del sensor de velocidad  STALKER RADAR.
# 
# @param port         Puerto en el que se encuentra conectado el radar.
# @param baudrate     Tasa de baudios para la comunicacion serial, el valor por  defecto es 9600, para valores diferentes se debe          realizar planteado en el manual de usuario en la seccion 5.4
# @param timeout      tiempo de espera para el establecimiento de la comunicacion serial 
# @param Plugged      Estado de la conexion del sensor
# @param _sensibility Configuracion de la sensibilidad del sensor 
# @param __maxGain    Ganancia maxima a configurar el sensor, para configurarla se debe utilizar el metodo de configuracion
# @param __minGain    Ganancia minima a configurar el sensor, para configurarla se debe utilizar el metodo de configuracion
# @param __tens       Variable interna para la lectura de velocidad
# @param __ones       Variable interna para la lectura de velocidad
# @param __tenths     Variable interna para la lectura de velocidad
# @param __state      estado de la maquina de estados utilizada para medir la velocidad
# @param __IsBufferBlocked  informa si el buffer esta siendo usado en la configuracion de parametros
# @param _serial_port      Instancia de la conexion con el puerto serial
# @param Carril      Carril asociado al radar con respecto al laser.

class StalkerRadar:

    ## Constructor
    # @param port Puerto en el que se buscara el sensor conectado
    # @param Carril al que se encuentra asociado el sensor, por defecto el carril Izquierdo
    # @param port Puerto en el que se buscara el sensor conectado
    # @param baudrate Tasa de baudios en la cual se entabla la comunicacion con el sensor, por defecto 9600
    # @param timeout Tiempo en segundos por el cual se intenta entablar conexion con el sensor antes de generar una excepcion,por defecto 1 segundo
    # @param sensibility  Sensibilidad inicial configurada para el sensor, por defecto 8 
    # @param minGain  Ganacia minima  para el sensor, por defecto 0 
    # @param maxGain  Ganacia maxima  para el sensor, por defecto 0 
    # @param plugged  Estado de la conexion con el sensor, por defecto desconectado 
    def __init__(self, port,Carril="Izquierdo", baudrate=9600, timeout=1,sensibility =8,maxGain = 7, minGain =0,Plugged=False):
        
    
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        self.Plugged = Plugged
        self.Carril = Carril
        self._sensibility = sensibility
        self._serial_port = None
        self.__maxGain = maxGain
        self.__minGain = minGain
        self.__tens = 0.0
        self.__ones = 0.0
        self.__tenths = 0.0
        self.__state = 0
        self.__IsBufferBlocked = False    
    
    ## abre el puerto serial con el nombre port.  si se encuentra conextado a otro puerto primero se desconeta## 
    def connect(self):
        if self._serial_port is not None:
            self.disconnect()
        self._serial_port = serial.Serial(
            self.port, self.baudrate,
            parity=serial.PARITY_NONE,bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE,
            timeout=self.timeout, dsrdtr=False)
   
    ##Desconexion del puerto serial
    def disconnect(self):
        if self._serial_port is None:
            return
        self._serial_port.close()

    
    ##Se realizan las configuraciones iniciales del sensor:
    # 1. Mode : Cambiar modo de trabajo a  estacionario
    # 2. Leading Zero Character: cambiar espacios por ceros
    # 3. Serial Port Output Format : Se dispone el formato A el cual entrega velocidad del objetivo mas fuerte
    # 4. Units: Entregar medidas en metros/segundo
    # 5. Unit Resolution: se dispone la velocidad en decenas, unidades, centecimas
    # 6. Sensibility and Fine Sensibility: Se configura la sensibilidad y la sensibilidad fina de acuerto a la sensibilidad definida en self._sensibility
    def configSensor(self):
        if self._serial_port != None:
            try:
                self._send_cmd_read_response(b'\x81',b'\x00')  # Mode : cambiar movimiento a estacionario
                self._send_cmd_read_response(b'\x97',b'\x01')  # Leading Zero Character: cambiar espacios por ceros
                self._send_cmd_read_response(b'\x9E',b'\x06') # Serial Port Output Format A: entrega velocidad del objetivo mas fuerte
                self._send_cmd_read_response(b'\x94',b'\x03') #  Units: Entregar medidas en metros/segundo
                self._send_cmd_read_response(b'\x95',b'\x01') # Unit Resolution: se dispone la velocidad en decenas, unidades, centecimas
                self.SetSensibility(self._sensibility)
                self.SetGainRange(self.__minGain,self.__maxGain)
                self.clearBuffer()
                
            except:
                logging.error("Configuracion del sensor fallida")
        else:
            logging.debug("El puerto serial no ha sido abierto")
    

    ## Configuracion del rango  de ganancia del sensor 
    # @param minGain Ganacina minima a configurar la cual debe ser mayor a 0 menor a 7 y menor al ala ganancia maxima
    # @param maxGain  Ganacina maxima a configurar la cual debe ser mayor a 0 menor a 7 y mayor al ala ganancia minima
    def SetGainRange(self,minGain,maxGain):
        if self._serial_port != None:
           if minGain <= maxGain and minGain >=0  and maxGain <=7:
                self.__minGain = minGain
                self._send_cmd_read_response(SET_MIN_GAIN,int_2_byte(minGain))  # minGain : Ganancia Minina
                self.__maxGain = maxGain
                self._send_cmd_read_response(SET_MAX_GAIN,int_2_byte(maxGain))  #maxGain : Ganancia Maxima
                self.clearBuffer()
        else:
            logging.debug("No se ha abierto el puerto de conexion con el sensor")     


    ##Se realizan las configuraciones iniciales del sensor:
    # @param sensibilty sensibilidad a configurar, el valor debera estar entre 15 y 0
    def SetSensibility(self,sensibility):
        if self._serial_port != None:
       
            if sensibility <=15 and sensibility >0:
                self._sensibility = sensibility
                Sen = int(sensibility)/4
                FineSen = int(sensibility)%4
                self._send_cmd_read_response(SET_SENSIBILITY,int_2_byte(Sen))  # Sensivility : cambiar Sensibilidad
                self._send_cmd_read_response(SET_FINE_SENSIBILITY,int_2_byte(FineSen))  #Fine Sensitivity Adjust : cambiar Sensibilidad
        else: 
            logging.debug("No se ha abierto la conexion con el sensor")        
    

    ## Retorna la ganancia minima configurada para el sensor
    # @return  GananciaMinima del sensor
    def GetMinGain(self):
        return _b2i(self._send_cmd_read_response(GET_MIN_GAIN,LECTURE_FLAG)) 
    
    ## Retorna la ganancia maxima configurada para el sensor
    # @return  Ganancia Maxima del sensor
    def GetMaxGain(self):
        return _b2i(self._send_cmd_read_response(GET_MAX_GAIN,LECTURE_FLAG)) 


    ## Retorna la sensibilidad configurada para el sensor
    # @return sensibilidad del sensor
    def GetSensibility(self):
        sen = _b2i(self._send_cmd_read_response(GET_SENSIBILITY,LECTURE_FLAG))
        fine_sen = _b2i(self._send_cmd_read_response(GET_FINE_SENSIBILITY,LECTURE_FLAG)) 
        return sen*4+sen


    ## Se envia una trama al sensor con el comando y el valor definido
    # @param command_id  comando a configurar
    # @param conf_value  valor a configurar
    def _send_cmd(self, command_id,conf_value):

        req = START_ID +DESTINATION_ID+SOURCE_ID+ PACKET_TYPE+PAYLOAD_LENGTH_LSB+PAYLOAD_LENGTH_MSB+command_id+ANTENNA_NUMBER+conf_value
        req = self._checksum_Calculator(req)
        self._serial_port.write(req)


    ##Se envia una trama al sensor con el comando y el valor definido y se obtiene la respuesta del sensor
    # @param command_id Comando a configurar
    # @param conf_value Valor a configurar
    # @param timeout    Tiempo de espera de respuesta por parte del sensor
    # @return     Se retorna la respues del sensor la cual debe ser igual al comando de configuracion enviado en caso de no recibir respuesta se retorna la bandera NO_RESPONSE (xFF)
    def _send_cmd_read_response(self,command,value,timeout=0.2, info = True):

        self.clearBuffer()
        self._send_cmd(command,value)
        result = self._read_response(command,timeout=timeout)
        if info:
            if result ==value:
                logging.info("Configuracion de radar: " + LOGGER_DIC[command] + "con valor: " + str(_b2i(value)))
        return result

    ## Se lee la respuesta  de un comando determinado en el buffer la cual se espera por un timeout
    # @param command  Comando  del cual se espera respuesta
    # @param timeout  tiempo de espera de la respuesta
    # @return comando encontrado en la respuesta, en caso de no encontrar respuesta se retorna xFF 
    def _read_response(self,command,timeout =0.2):
        state = 0
        begin = time.time()
        self.__IsBufferBlocked =True
        while time.time() - begin <timeout:
            data= self._serial_port.read(self._serial_port.in_waiting)
            data_list = list(data)
            for item in data_list:
                # se verifica la cabecera 
                if state == 0:
                    if item == START_ID:
                        state = 1
                    else:
                        state = 0
                    
                # se verifica el byte de source_ID
                elif state == 1:
                    if item == SOURCE_ID:
                        state = 2
                    else:
                        state = 0
                # se verifica el byte de Destination_ID
                        
                elif state == 2:
                    if item == DESTINATION_ID:
                        state = 3
                    else:
                        state = 0
                # se verifica el byte de PackedType
                elif state == 3:
                    if item == PACKET_TYPE:
                        state = 4
                    else:
                        state = 0
                # se verifica el byte de Payload_LSB
                elif state == 4:
                    if item == PAYLOAD_LENGTH_LSB:
                        state = 5
                    else:
                        state = 0

                # se verifica el byte de Payload_MSB
                elif state == 5:
                    if item == PAYLOAD_LENGTH_MSB:
                        state = 6
                    else:
                        state = 0
                # se verifica el byte de comando
                elif state == 6:
                    if item == command:
                        state = 7
                    else:
                        state = 0

                elif state == 7:
                    if item == ANTENNA_NUMBER:
                        state = 8
                    else:
                        state = 0
                elif state == 8:
                    self.__IsBufferBlocked =False                    
                    return item

        self.__IsBufferBlocked =False
        return  NO_RESPONSE   




  
     
    ## Calculo del checksum de la trama a enviar al sensor 
    # @param msg trama a entregar
    # @return trama concatenada con el checksum calculado
    def _checksum_Calculator(self, msg):
   
        msg_list = list(msg)
        if len(msg_list)%2 != 0:
            msg_list.append(b'\x00')
        checksum = 0
        for i in range(0, len(msg_list), 2):
            checksum = checksum +_b2i(msg_list[i+1])*256+_b2i(msg_list[i])
        int_mgs_list = list(struct.pack(">I", checksum))
        return msg+int_mgs_list[3]+int_mgs_list[2]


    ## permite limpiar completamente el buffer
    # @return entrega la informacion existente en el buffer
    def clearBuffer(self):
        return self._serial_port.read(self._serial_port.in_waiting)

        
    ## Lee la informacion almacenada en el buffer y la interpreta como datos de velocidad
    # @return entrega las medidas de velocidad existentes en el buffer como una lista
    def ReadData(self):
   
        vel_list = []
        if not self.__IsBufferBlocked:
            data= self._serial_port.read(self._serial_port.in_waiting)
            data_list = list(data)
            for item in data_list:
                try:
                    if self.__state == 0:
                        if item == b'\x0D':
                            self.__state = 1
                    elif self.__state == 1:
                        self.__tens = float(item)            
                        self.__state = 2
                    elif self.__state == 2:
                        self.__ones = float(item)
                        self.__state = 3
                    elif self.__state == 3:
                        self.__tenths = float(item)
                        vel = self.__tens*10 +self.__ones+self.__tenths*0.1 
                        vel_list.append(vel)
                        self.__state = 0
                except:
                    self.__state = 0
                    self.__tens = 0.0
                    self.__ones = 0.0
                    self.__tenths = 0.0
                
        return vel_list


    ## verifica si existe informacion en el buffer suficiente para tomar una medidad de velocidad
    # @return retorna true si existen 
    def samplesinbuffer(self):

        if self._serial_port.in_waiting+self.__state>4:
            return True
        else:
            return False

    
    ## Genera un diccionario con los parametros a almacenar del sensor de velocidad
    # @return  Diccionario con los parametros del sensor
    def create_parameters_dictionary(self):
        
        parameters_dic = dict(self.__dict__)
        del parameters_dic["_serial_port"]
        del parameters_dic["port"]
        return parameters_dic


if __name__ == '__main__':

    # import matplotlib.pyplot as plt 
    PORT_NAME = '/dev/ttyUSB0'
    radar = StalkerRadar(PORT_NAME)
    radar.connect()
    radar.configSensor()
    radar.clearBuffer()
    radar.create_parameters_dictionary()
    
    vel_list = []
    print "Resultado: ", radar.GetMaxGain()
    print "Resultado: ", radar.GetMinGain()
    print "Resultado: ", radar.GetSensibility()
    plt.ion()
    try:
        print('Recording measurments... Press Crl+C to stop.')
        cnt = 0
        while True:
            plt.cla()
            vel_list =vel_list + radar.ReadData()
            list_len = len(vel_list)
            if cnt%5 == 0:
                if list_len<100:
                    plt.plot(vel_list)
                else:
                    plt.plot(vel_list[list_len-100:list_len-1])
                plt.grid()
                plt.draw()
                plt.show()                

    except KeyboardInterrupt:
        print('Stoping.')
        plt.close()
        radar.disconnect()