#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import serial.tools.list_ports
import serial
import Hokuyo
import hokuyoaist
import logging
import sys
DEVICE_MODEL_BASEDIR = os.path.dirname(os.path.abspath(__file__))
from camera import Cameras
from stalkerradar import StalkerRadar
from RPlidarLaser import RPLidar
import json
import time
import numpy as np
import Preprocessing_PREP as Preprocessing
import bridge_fpfh_vfh as bridge
import SVM_classifier_evaluate as SVM_classifier
from LPR import LPR
from Hokuyo import Sample_selection


"""
Improvements : Almacenar y cargar desde un archivo JSON  las configuraciones por defecto de los sensores y borrar de

"""

"""
Definicion de las llaves para el archivo JSON  que seran usadas para cada tipo de sensor
"""
HOKUYO_KEY = "Hokuyo"
RPLIDAR_KEY = "Rplidar"
RADAR_KEY = "Radar"
CAMARA_KEY = "Camera"

"""
Definicion de  constantes para el reconocimiento del tipo de sensor conectado cada tipo de sensor

$SENSORTYPE$_PORT_TYPE: tipo de conexion con el sensor
$SENSORTYPE$_PORT_DESCRIPTION: descripcion del tipo de conexion con el sensor
"""
RPLIDAR_PORT_TYPE = "/dev/ttyUSB"
RADAR_PORT_TYPE = "/dev/ttyUSB"
HOKUYO_PORT_TYPE = "/dev/ttyACM"

RPLIDAR_PORT_DESCRIPTION = "CP2102 USB to UART Bridge Controller"
RADAR_PORT_DESCRIPTION = "USB-Serial Controller D"
HOKUYO_PORT_DESCRIPTION = "URG-Series USB Driver"

A_RW = 484

"""
Definicion de las configuraciones por defecto de los sensores 
"""



PEAJE_KEY = "Peaje"
CARRIL_KEY= "Carril"
SAMPLE_SELECTION_KEY = "Sample_Selection" 
CREATION_KEY = "Creation"
IZQ_KEY = "Izquierdo"
DER_KEY = "Derecho"
CLUSTER_KEY = "Cluster"
FPFH_KEY = "FPFH"
SVM_KEY = "SVM"

def create_directory(route):
    if not os.path.exists(route):
        os.makedirs(route)



## Clase que contiene dicionarios con los diferentes sensores a utilizar por el sistema, ademas de las  instancias
# relacionadas con el conteo y clasificacion de los vehiculos, lo cual conformara el modelo de la 
# aplicacion. Adicionalmente permitira la interacion con 2 archivos JSON necesarios para el funcionamiento del sistema
# de la siguiente manera
# JSON   parametros: almacena la configuracion de cada uno de los sensores mediante la siguiente convencion:
# JSON  parametros_por_defecto:  Almacenara la configuracion de los sensores por defecto.
# @param DefaultDic         Diccionario que contendra las configuraciones por defecto de los sensores, el cual se leera desde archivo.
# @param globalDic          Diccionario que contendra el ultimo archivo JSON parametros de configuracion de los sensores.
# @param Cameras            Objeto Camaras que contendra el listado de camaras y permitira su deteccion y configuracion, para mayor
#                           informacion revise la documentacion de la clase Cameras del archivo camera.py 
# @param Hokuyo_Dic         Diccionario que contiene los sensores Hokuyo, instancias de la clase Hokuyo_utm en el archivo Hokuyo.py 
# @param Radar_Dic          Diccionario que contiene los radares de velocidad, instancias de la clase StalkerRadar en el archivo stalkerradar.py 
# @param Laser_Dic          Diccionario que contiene los laser RPLIDAR, instancias de la clase RPLidar en el archivo RPlidarLaser.py 
# @param JSONPATH           Ruta del JSON de configuracion con nombre por defecto  "parameteros.txt"
# @param DEFAULT_JSONPATH   Ruta del JSON  de parametros de configuracion  por defecto "parametros_por_defecto.txt"
# @param Sample_C_Iz        Instancia de la clase Sample_selection del carril izquierdo
# @param Sample_C_De        Instancia de la clase Sample_selection del carril derecho
# @param Creation_C_IZQ     Instancia de la clase Point_cloud para el carril izquierdo 
# @param Creation_C_DER     Instancia de la clase Point_cloud para el carril derecho
# @param Creation_RPC_IZQ   Instancia de la clase Rplidar_Cloud para el carril derecho
# @param Creation_RPC_DER   Instancia de la clase Rplidar_Cloud para el carril derecho
# @param Segmentador_C      Instancia de la clase bridge.Cluster encargada de la clusterizacion 
# @param Descriptor_C       Instancia de la clase bridge.VFH_FPFH_C encargada de la extraccion de descriptores      
# @param SVM_C              Instancia de la clase SVM_classifier   encargada de realizar la clasificacion de los vehiculos
# @param Sample_Dic =       puntero a los parametros de configuracion del diccionario de configuracion de la seleccioncion de muestras validas
# @param Creation_Dic_Iz    puntero a los parametros de configuracion del diccionario de configuracion de la creacion de la nube de puntos del lado izquierdo
# @param Creation_Dic_De    puntero a los parametros de configuracion del diccionario de configuracion de la creacion de la nube de puntos del lado derecho
# @param Cluster_Dic        puntero a los parametros de configuracion del diccionario de configuracion de la clusterizacion
# @param FPFH_Dic           puntero a los parametros de configuracion del diccionario de configuracion de la extraccion de descriptores FPFH
# @param SVM_Dic            puntero a los parametros de configuracion del diccionario de configuracion de la clasificacion de vehiculos

class DevicesModel:
    
    DefaultDic = None
    globalDic = None  
    # diccionarios e instancias de los sensores
    Cameras = None
    Hokuyo_Dic = {}
    Radar_Dic = {}
    Laser_Dic = {}
    JSONPATH = "parametros"
    DEFAULT_JSONPATH = DEVICE_MODEL_BASEDIR + u"/default_parameters"
    
    # instancias de los objetos del software
    Sample_C_Iz = None
    Sample_C_De = None
    Creation_C_IZQ = None
    Creation_C_DER = None
    Creation_RPC_IZQ = None
    Creation_RPC_DER = None
    Segmentador_C = None
    Descriptor_C = None
    SVM_C = None
    # punteros al diccionario global
    Sample_Dic = {}
    Creation_Dic_Iz = {}
    Creation_Dic_De = {}
    Cluster_Dic = {}
    FPFH_Dic = {}
    SVM_Dic = {}
    # rutas a los diccionarios de configuracion
    
    ## Constructor.
    # @param JSONPATH Nombre del JSON de configuracion, de defecto "parametros"
    def __init__(self,JSONPATH="parametros"):
        # se lee el archivo JSON con la configuracion por defecto y se almacena en la variable DefaultDic
        self.DEFAULT_JSONPATH = DEVICE_MODEL_BASEDIR + u"/default_"+os.path.split(JSONPATH)[1]
        if os.path.exists(self.DEFAULT_JSONPATH):
        	self.DefaultDic = self.ReadJSON(self.DEFAULT_JSONPATH)
        self.JSONPATH = JSONPATH
        # se chequea si existe el archivo de configuracion de sensores
        if os.path.exists(JSONPATH):
            # se lee el archivo JSON con la ultima configuracion de los sensores y se almacena en la variable GlobalDic
            self.globalDic = self.ReadJSON(self.JSONPATH)
            # posteriormente se  registran en  los diccionarios  los sensores almacenados en el archivo JSON
            self.InitializeDictionaries()
        # Luego, se coteja la informacion con  los sensores conectados, si estan registrados y no conectados se marcan como unplugged
        self.DetectHokuyoDevices()
        self.DetectRplidarDevices()
        self.DetectRadarDevices()
        self.DetectCameras()
        self.Descriptor_C = bridge.VFH_FPFH_C()
        self.UpdateSoftwareObjects()
        self.PassCloudClasses = bridge.PassCloudClasses
        self.LPR = LPR(DEVICE_MODEL_BASEDIR+ '/PlatesDetectors/co10.xml',DEVICE_MODEL_BASEDIR+ '/PlatesReaders/my_model10.h5')
        

    ## Permite la lectura de un archivo en formato JSON y lo entrega como un diccionario en python
    # @param path Nombre del JSON de configuracion, de defecto "parametros"
    def ReadJSON(self,path):

        file_object  = open(path, "r")
        json_info = file_object.read()
        dic = json.loads(json_info)
        file_object.close()
        return dic

    ##  lee  los datos de los  Sensores y se crean sus instancias con los parametros leidos,
    #  las instancias se almacenan en los diccionarios de cada tipo de sensor
    def InitializeDictionaries(self):
        # se leen  los datos de los  Radares, se crea nuevos objetos y se escriben los parametros leidos
        for key, value in self.globalDic[RADAR_KEY].iteritems():
            item = StalkerRadar(key)
            dic = item.__dict__
            for i in value.keys():
                dic[i] = value[i]
            self.Radar_Dic[key] = item

        # se leen  los datos de los  sensores Hokuyo,, se crea nuevos obje>tos y se escriben los parametros leidos
        for key, value in self.globalDic[HOKUYO_KEY].iteritems():
            item = Hokuyo.Hokuyo_utm(key)
            dic = item.__dict__
            for i in value.keys():
                dic[i] = value[i]
            self.Hokuyo_Dic[key] = item

        # se leen  los datos de los  sensores RPlidar, se crea nuevos objetos y se escriben los parametros leidos
        for key, value in self.globalDic[RPLIDAR_KEY].iteritems():
            item = RPLidar(key)
            dic = item.__dict__
            for i in value.keys():
                dic[i] = value[i]
            self.Laser_Dic[key] = item

        self.Sample_Dic = self.globalDic[SAMPLE_SELECTION_KEY]    	
        # se leen  los datos de los  Radares, se crea nuevos objetos y se escriben los parametros leidos
        self.Creation_Dic_Iz = self.globalDic[CREATION_KEY][IZQ_KEY]      
        # se leen  los datos de los  Radares, se crea nuevos oQbjetos y se escriben los parametros leidos
        self.Creation_Dic_De = self.globalDic[CREATION_KEY][DER_KEY]
        # se leen  los datos de los  sensores Hokuyo,, se crea nuevos objetos y se escriben los parametros leidos
        self.Cluster_Dic = self.globalDic[CLUSTER_KEY]
        # se leen  los datos de los  sensores RPlidar, se crea nuevos objetos y se escriben los parametros leidos
        self.FPFH_Dic = self.globalDic[FPFH_KEY]
        # se leen  los datos de los  sensores RPlidar, se crea nuevos objetos y se escriben los parametros leidos
        self.SVM_Dic = self.globalDic[SVM_KEY]
            
    
    ## realiza la  instancia del objeto camaras  y se abren las camaras, si estan resgistradas se configuran, en caso contrario se asigna configuraciones por defecto    
    def DetectCameras(self):
        if self.Cameras ==None:
            self.Cameras =Cameras()
            self.Cameras.openCameras()
        #self.Cameras.detectCameras()
        for cameraiD in  self.Cameras.cameraIds:
            print cameraiD
            if cameraiD in self.globalDic["Cameras"]:
                Source = self.globalDic["Cameras"][cameraiD]
            else:
                Source = self.DefaultDic["Cameras"]
            self.Cameras.ConfigureCamera(cameraiD,Source)

    ## Se almacenan los datos de configuracion en el archivo JSON de configuracion.    
    def SaveParameters(self):
        # Se guarda los parametros del radar
        radar_dic = {}
        for key, value in self.Radar_Dic.iteritems():
            radar_dic[key] = {}
            dic = self.Radar_Dic[key].__dict__
            for param in self.DefaultDic[RADAR_KEY]:
                radar_dic[key][param] = dic[param]

        hokuyo_dic = {}
        for key, value in self.Hokuyo_Dic.iteritems():
            hokuyo_dic[key] = {}
            dic = self.Hokuyo_Dic[key].__dict__
            for param in self.DefaultDic[HOKUYO_KEY]:
                hokuyo_dic[key][param] = dic[param]
        
        laser_dic = {}
        for key, value in self.Laser_Dic.iteritems():
            laser_dic[key] = {}
            dic = self.Laser_Dic[key].__dict__
            for param in self.DefaultDic[RPLIDAR_KEY]:
                laser_dic[key][param] = dic[param]
        
        
        self.globalDic[HOKUYO_KEY] = hokuyo_dic
        self.globalDic[ RADAR_KEY] = radar_dic
        self.globalDic[RPLIDAR_KEY] = laser_dic
        
        file_object = open(self.JSONPATH,"w")
        str1 = json.dumps(self.globalDic, sort_keys=True,indent=4, separators=(',', ': '))
        file_object.write(str1)
        file_object.close()

    
    ##Detecta los sensores conectados y coteja con la informacion contenida en los diccionarios
    def DetectHokuyoDevices(self):
        device_port_list = []
        # se detecta  los conexiones seriales existentes
        ports = list(serial.tools.list_ports.comports())
        for port_data in ports:
            # para cada conexion se verifica si es un sensor Hokuyo
            if self.IsHokuyo(port_data):
                device_port_list.append(port_data.device)
                # Si el sensor no se encontraba registrado en el JSON se  crea una nueva instancia con valores de config por defecto
                if not (port_data.device in self.Hokuyo_Dic):
                    self.Hokuyo_Dic[port_data.device] = Hokuyo.Hokuyo_utm(port_data.device,Plugged =True) 
                    logging.debug('Sensor Hokuyo encontrado y sin registrar: '+port_data.device)
                # en caso de estar registrado se informa  y se cambia la etiqueta plugged = true
                else:
                    logging.debug('Sensor Hokuyo encontrado y registrado: '+port_data.device)
                    self.Hokuyo_Dic[port_data.device].Plugged =True
        
        # Luego a los sensores conectados no conectados y registrados se rqiquetan con plugged  = false
        for key in self.Hokuyo_Dic.keys():
            key in device_port_list
            if not (key in device_port_list):
                #del self.Hokuyo_Dic[key] 
                self.Hokuyo_Dic[key].Plugged = False 
                logging.debug(u'Registro de dispositivo Hokuyo sin conexión: '+key)
    
    

    ## Detecta los radares de velocidad conectados y coteja con la informacion contenida en los diccionarios.
    def DetectRadarDevices(self):
        device_port_list = []
        # se detecta  los conexiones seriales existentes
        ports = list(serial.tools.list_ports.comports())
        for port_data in ports:
            # para cada conexion se verifica si es un sensor Hokuyo
           if self.IsSlamtecRadar(port_data):
                device_port_list.append(port_data.device)
                # Si el sensor no se encontraba registrado en el JSON se  crea una nueva instancia con valores de config por defecto
                if not (port_data.device in self.Radar_Dic):
                    self.Radar_Dic[port_data.device] = StalkerRadar(port_data.device) 
                    logging.debug('Radar encontrado y sin registrar: '+port_data.device)
                # en caso de estar registrado se informa  y se cambia la etiqueta plugged = true
                else:
                    logging.debug('Radar encontrado y registrado: '+port_data.device)
                # se marca el sensor como conectado
                self.Radar_Dic[port_data.device].Plugged =True
        # Luego a los sensores conectados no conectados y registrados se rqiquetan con plugged  = false
        for key in self.Radar_Dic.keys():
            if not (key in device_port_list):
                #del self.Radar_Dic[key]
                if self.Radar_Dic[key].Plugged:
                    self.Radar_Dic[key].Plugged =False
                    logging.debug(u'Registro de dispositivo Radar sin conexión: '+key)

    ##Detecta los sensores RPLIDAR conectados y coteja con la informacion contenida en los diccionarios
    def DetectRplidarDevices(self):
        device_port_list = []
        # se detecta  los conexiones seriales existentes
        ports = list(serial.tools.list_ports.comports())
        for port_data in ports:
            # para cada conexion se verifica si es un sensor RPLidar
            if self.IsRplidar(port_data):
                device_port_list.append(port_data.device)
                # Si el sensor no se encontraba registrado en el JSON se  crea una nueva instancia con valores de config por defecto
                if not (port_data.device in self.Laser_Dic):
                    self.Laser_Dic[port_data.device] =  RPLidar(port_data.device) 
                    logging.debug('Sensor Rplidar encontrado: '+port_data.device)
                else:
                    logging.debug('Sensor Rplidar encontrado y registrado: '+port_data.device)
                self.Laser_Dic[port_data.device].Plugged = True
        # Luego a los sensores conectados no conectados y registrados se rqiquetan con plugged  = false
        for key in self.Laser_Dic.keys():
            if not (key in device_port_list):
                #del self.Laser_Dic[key] 
                self.Laser_Dic[key].Plugged = False 
                logging.debug(u'Registro de dispositivo Rplidar sin conexión: '+key)


    ##Detecta si un puerto esta conectado a un sensor hokuyo
    # @param port_data nombre del puerto
    # @return retorna un booleano indicando si el sensor es un hokuyo
    def IsHokuyo(self,port_data):
        
        if port_data.device[:-1] == HOKUYO_PORT_TYPE:
            if port_data.description == HOKUYO_PORT_DESCRIPTION:
                return True
        return False

    ##Detecta si un puerto esta conectado a un sensor RPLIDAR
    # @param port_data nombre del puerto
    # @return retorna un booleano indicando si el sensor es un RPLIDAR
    def IsRplidar(self,port_data):
        if port_data.device[:-1] == RPLIDAR_PORT_TYPE:
            if port_data.description == RPLIDAR_PORT_DESCRIPTION:
                return True
        return False

    ##Detecta si un puerto esta conectado a un  radar de velocidad
    # @param port_data nombre del puerto
    # @return retorna un booleano indicando si es un radar de velocidad
    def IsSlamtecRadar(self,port_data):
        """Detecta si un puerto esta conectado a un radar"""
      
        if port_data.device[:-1] == RADAR_PORT_TYPE:
            if port_data.description == RADAR_PORT_DESCRIPTION:
                return True
        return False


    ## configura  un sensor Hokuyo o RPlidar con los parametros por defecto
    # @param dev puerto asociado al sensor 
    def SetDefaultParametersLaser(self,dev):
        Update = False
        if dev in self.Hokuyo_Dic: 
            device = self.Hokuyo_Dic[dev]
            dic = self.DefaultDic["Hokuyo"]
            Update = True
        
        elif dev in self.Laser_Dic: 
            device = self.Laser_Dic[dev]
            dic = self.DefaultDic["Rplidar"]
            
            Update = True
        if Update:
            device.i_enable = dic["i_enable"]
            device.d_enable = dic["d_enable"]
            device.i_range_min = dic["i_range_min"]
            device.i_range_max = dic["i_range_max"]
            device.d_range_min = dic["d_range_min"]
            device.d_range_max = dic["d_range_max"]
            device.angle_a0 = dic["angle_a0"]

    ## Toma los parametros de la seleccion de muestras validas del diccionario de configuracion e instancia la clases
    #  Sample_selection para el lado izquierdo y derecho en los atributos Sample_C_Iz y Sample_C_De 
    def InitializeSampleClass(self):
        dic = self.Sample_Dic[IZQ_KEY] 
        self.Sample_C_Iz = Sample_selection(dic["Vector_Temporal_Size"])
        self.Sample_C_Iz.Init_params(dic["Laser_start"], dic["Laser_stop"], dic["Laser_step"],dic["Distance_min"],
            dic["Distance_max"], dic["Min_No_Vehicles"], dic["Min_Vehicles_Samples"])
        dic = self.Sample_Dic[DER_KEY]
        self.Sample_C_De = Sample_selection(dic["Vector_Temporal_Size"])
        self.Sample_C_De.Init_params(dic["Laser_stop"], dic["Laser_start"], dic["Laser_step"],dic["Distance_min"],
            dic["Distance_max"], dic["Min_No_Vehicles"], dic["Min_Vehicles_Samples"])

    ## Toma los parametros para la creacion de la nube de puntos del  diccionario de configuracion e instancia la clase
    #  Point_Cloud para el lado izquierdo y derecho en los atributos Creation_C_IZQ y Creation_C_DER 
    def InitializeCreacionClass(self):
        self.Creation_C_IZQ = Preprocessing.Point_Cloud(self.Creation_Dic_Iz["Original_Folder"], self.Creation_Dic_Iz["inicial_angle"], self.Creation_Dic_Iz["delta"], 
        	self.Creation_Dic_Iz["total_range_angle"], self.Creation_Dic_Iz["InicialOffset"], self.Creation_Dic_Iz["FinalOffset"], self.Creation_Dic_Iz["save_original"], 
        	self.Creation_Dic_Iz["save_filtered"], self.Creation_Dic_Iz["save_No_ground"], np.array(self.Creation_Dic_Iz["x_limit"]), np.array(self.Creation_Dic_Iz["y_limit"]),
            self.Creation_Dic_Iz["Right"],self.Creation_Dic_Iz["vel_limit"])

        self.Creation_C_DER = Preprocessing.Point_Cloud(self.Creation_Dic_De["Original_Folder"], self.Creation_Dic_De["inicial_angle"], self.Creation_Dic_De["delta"], 
        	self.Creation_Dic_De["total_range_angle"], self.Creation_Dic_De["InicialOffset"], self.Creation_Dic_De["FinalOffset"], self.Creation_Dic_De["save_original"], 
        	self.Creation_Dic_De["save_filtered"], self.Creation_Dic_De["save_No_ground"], np.array(self.Creation_Dic_De["x_limit"]), np.array(self.Creation_Dic_De["y_limit"]),
            self.Creation_Dic_De["Right"],self.Creation_Dic_De["vel_limit"])

        self.Creacion_Folder = self.Creation_Dic_Iz["Original_Folder"]
        create_directory(self.Creation_Dic_Iz["Original_Folder"])

        self.Creation_RPC_IZQ = Preprocessing.Rplidar_Cloud(self.Creation_Dic_Iz["Original_Folder"], self.Creation_Dic_Iz["default_vel"], 90.0, self.Creation_Dic_Iz["save_original"], 
        	self.Creation_Dic_Iz["save_filtered"], self.Creation_Dic_Iz["save_No_ground"], np.array(self.Creation_Dic_Iz["x_limit"]), np.array(self.Creation_Dic_Iz["y_limit"]),
            self.Creation_Dic_Iz["Right"],True)

        self.Creation_RPC_DER = Preprocessing.Rplidar_Cloud(self.Creation_Dic_De["Original_Folder"], self.Creation_Dic_De["default_vel"], 90.0, self.Creation_Dic_De["save_original"], 
        	self.Creation_Dic_De["save_filtered"], self.Creation_Dic_De["save_No_ground"], np.array(self.Creation_Dic_De["x_limit"]), np.array(self.Creation_Dic_De["y_limit"]),
            self.Creation_Dic_De["Right"],True)

    ## Toma los parametros para la clusterizacion del  diccionario de configuracion e instancias las clases
    #  bridge.Cluster en el atributo Segmetador_C  
    def InitializeClusterClass(self):
    	self.Segmentador_C = bridge.Cluster()
        self.Segmentador_C.set_Parameters_ALL(self.Cluster_Dic["Cluster_min"], self.Cluster_Dic["Cluster_max"], self.Cluster_Dic["Cluster_tolerance"], 
            self.Cluster_Dic["Filter_mean"], self.Cluster_Dic["Filter_Thresh"], self.Cluster_Dic["Wheel_tolerance"], self.Cluster_Dic["Wheel_size_min"], 
            self.Cluster_Dic["Wheel_size_max"], self.Cluster_Dic["Wheel_delta_ground"], self.Cluster_Dic["Wheel_distance_filter"], self.Cluster_Dic["Wheel_min_width"])
        self.Segmentador_C.Cluster_Folder = self.Cluster_Dic["Cluster_Folder"]
        self.Segmentador_C.Cluster_save = self.Cluster_Dic["save_cluster"]
        create_directory(self.Cluster_Dic["Cluster_Folder"])

    ## Toma los parametros de para la extraccion de descriptores FPFH del  diccionario de configuracion
    #  e instancias las clases 
    #  bridge.VFH_FPFH_C en el atributo Descriptor_C  
    def InitializeFPFHClass(self):
        # self.Descriptor_C = bridge.VFH_FPFH_C()
        self.Descriptor_C.set_Parameters_ALL(self.FPFH_Dic["Density_delta"], self.FPFH_Dic["Normales_radiusS"], self.FPFH_Dic["Uniform_Sampling_radius"], 
            self.FPFH_Dic["FPFH_radiusS"], self.FPFH_Dic["FPFH_radiusS"])

    ## Toma los parametros para clasificacion del  diccionario de configuracion e instancias las clases
    #  SVM_classifier en el atributo SVC_C  
    def InitializeSVMClass(self):
        self.SVM_C = SVM_classifier.Vehicle_Category_Classifier.LoadModels_M(self.SVM_Dic["Model_path"], self.SVM_Dic["km_selected"], self.SVM_Dic["svm_selected"])
        self.Vector_categorias = self.SVM_Dic["Vector_categorias"]

        
     
    ## Actualiza los objetos para el software de conteo y clasificacion
    def UpdateSoftwareObjects(self):
        self.InitializeSampleClass()
        self.InitializeCreacionClass()
        self.InitializeClusterClass()
        self.InitializeFPFHClass()
        self.InitializeSVMClass() 

    ## Cierra la conexion con los sensores
    def DisposeSensors(self):
    	for dev in self.Hokuyo_Dic:
    	    self.Hokuyo_Dic[dev].LaserDispose()
    	    logging.debug(u"Cerrada conexión con Hokuyo en"+dev)
    	for dev in self.Radar_Dic:
    	    self.Radar_Dic[dev].disconnect()
    	    logging.debug(u"Cerrada conexión con Radar en"+dev)
    	for dev in self.Laser_Dic:
    		device =self.Laser_Dic[dev]
    		if device.Plugged:
    			self.Laser_Dic[dev].stop_motor()
    			self.Laser_Dic[dev].disconnect()
    			logging.debug(u"Cerrada conexión con Rplidar en"+dev)
    	self.Cameras.Dispose()
     
    ## Abre y configura la conexion con los laser Hokuyo o RPLIDAR
    # @param dev_port Puerto en el que se encuetra el sensor a configurar
    # @return Retorna un booleano indicando si la configuracion fue exitosa
    def OpenLaser(self,dev_port):
        succes = False
        if dev_port in self.Hokuyo_Dic:
            device =self.Hokuyo_Dic[dev_port]
            if device.Plugged:
                try:
                    if not device.laser.is_open():
                        os.chmod(dev_port,484)             
                        device.OpenDevice()
                    succes = True
                except :
                    device.LaserDispose()
        if dev_port in self.Laser_Dic:
            device =self.Laser_Dic[dev_port]
            if device.Plugged:
                try:
                    if not device.is_open():
                        os.chmod(dev_port,484)             
                        device.connect()
                    device.stop_motor()
                    time.sleep(1)
                    device.start_motor()
                    time.sleep(1)
                    
                    device.init_read_scans()
                    time.sleep(1)
                    succes = True
                except:
                    device.disconnect()
        
        return succes
    

    ## Abre y configura la conexion con los radares de velocidad
    # @param dev_port Puerto en el que se encuetra el sensor a configurar
    # @return Retorna un booleano indicando si la configuracion fue exitosa
    def OpenRadar(self,dev_port):
        try:
            if dev_port in self.Radar_Dic:
                device = self.Radar_Dic[dev_port]
                if device.Plugged: 
                    os.chmod(dev_port,484)
                    device.connect()
                    device.configSensor()
                    return True
        except: 
            pass
        return False
                    
if __name__ == '__main__':
    # lidar = rplidar.RPLidar('/dev/ttyUSB0')
    # info = lidar.get_info()
    # print(info['serialnumber'])
    # lidar.disconnect()
    logging.getLogger().setLevel(logging.DEBUG)
    Config = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    Config.Cameras.Dispose()
    Config.SaveParameters("parametros.txt",1,3)
