import pcl
import math
import numpy as np
from pykalman import KalmanFilter

class Point_Cloud:
	 """
	 Creando
	 """

	 def __init__(self, Original_Folder, inicial_angle=-135.0, delta=0.25, total_range_angle=(1080/2)+1, InicialOffset=0.0, FinalOffset=135.0, save_original=False, save_filtered=False, save_No_ground=False, 
	 	x_limit=np.array([0.7, 5.0]), y_limit=np.array([-5.0, 5.0]), Right = True, vel_limit=6.5):
	 	"""
	 	Regresa un objeto Point_cloud con los siguientes parametros:

	 	total_range = rango de lo datos medidos por haz del laser (angulo * delta) por nube de puntos
	 	delta = resolucion angular del laser
	 	numero de medidas = numero de haz del laser capturados (cada 0.25 segundos)

	 	angles = vector auxiliar para calcular los angulos correspondientes a cada medida del laser
	 	aux0 = angulo inicial

	 	x_vector = Vector auxiliar con los valores en el eje "X" de cada medida
	 	y_vector = Vector auxiliar con los valores en el eje "Y" de cada medida
	 	z_vector = Vector auxiliar con los valores en el eje "Z" de cada medida

	 	x_Matriz = Matriz final con los valores en el eje "X" de la nube de puntos
	 	y_Matriz = Matriz final con los valores en el eje "Y" de la nube de puntos
	 	z_Matriz = Matriz final con los valores en el eje "Z" de la nube de puntos

	 	velocity = Variablecon las velocidades *cambiar estaa descripcion*



	 	"""
	 	self.total_range_angle = total_range_angle

	 	if not Right:
		 	self.InicialOffset = InicialOffset*4
		 	self.FinalOffset = (FinalOffset*4)
		 	self.delta = delta * (-1.0)
 			self.inicial_angle = inicial_angle * (-1.0)
 			self.factor_correccion = 1.0
		else:
		 	self.InicialOffset = (InicialOffset*4) - (total_range_angle-1)
		 	self.FinalOffset = (FinalOffset*4) - (total_range_angle)
		 	self.delta = delta * (-1.0)
 			self.inicial_angle = (inicial_angle + (total_range_angle/4.0))*(-1)
 			self.factor_correccion = -1.0

	 	self.total_range = self.FinalOffset-self.InicialOffset
	 	
	 	self.angles = np.zeros(shape=(self.total_range_angle,1))

	 	self.x_vector = np.zeros(shape=(self.total_range,1))
	 	self.y_vector = np.zeros(shape=(self.total_range,1))
	 	self.z_vector = np.zeros(shape=(self.total_range,1))

	 	self.original_pointcloud = pcl.PointCloud()
	 	self.filtered_pointcloud_d = pcl.PointCloud()
	 	self.plane_pointcloud = pcl.PointCloud()
	 	self.without_plane_pointloud = pcl.PointCloud()

	 	self.error = False
		self.Upsample = False

	 	self.TanAngleX = 0.0000
	 	self.TanAngleZ = 0.0000

	 	self.Set_Parameters(save_original, save_filtered, save_No_ground, Original_Folder, x_limit, y_limit, Right, vel_limit)
	 	self.Init_Kalman_Flter()
	 	self.Angular_Vector_Create()


	 def Set_Parameters(self, save_original, save_filtered, save_No_ground, Original_Folder, x_limit, y_limit, Right, vel_limit):

	 	#Parametros para Point_cloud_Create
	 	self.save_original = save_original
	 	self.Folder = Original_Folder

	 	# Parametros para Filter_distance
	 	self.x_limit = x_limit
	 	self.y_limit = y_limit
	 	self.save_filtered = save_filtered

	 	# Parametros para Ground_extraction_Normal
	 	self.save_No_ground = save_No_ground
	 	self.Right = Right

	 	# Parametros para Upsamling
	 	self.vel_limit = vel_limit

	 def Init_Kalman_Flter(self):
		SampleFreQ = 1.0/40.0
		SampleFreQd2 = (math.pow(SampleFreQ,2))/2.0
		SampleFreQd3 = (math.pow(SampleFreQ,3))/3.0
		F = [[1,SampleFreQ], [0,1]]
		H = [0,1]
		Q = [[SampleFreQd3,SampleFreQd2],[SampleFreQd2,SampleFreQ]]
		self.kf = KalmanFilter(transition_matrices = F, observation_matrices =H, transition_covariance=Q )


	 def Angular_Vector_Create(self):
	 	"""
	 	Crea el vector de angles para la nube de puntos

	 	"""
	 	aux = self.inicial_angle
		for i in range(0, self.total_range_angle):
			self.angles[i] = ((math.pi*(aux))/180.0)
			aux=aux+self.delta

	 def Put_Laser(self, data_laser):
	 	self.error = False
	 	self.Upsample = False
	 	self.data_laser = data_laser
	 	self.samples_number = data_laser.shape[0]
	 	self.vel_data = self.data_laser[:,0]
	 	self.Kalman_Velocty(self.vel_data)

	 	self.x_matriz = np.zeros(shape=(self.samples_number*self.total_range,1))
	 	self.y_matriz = np.zeros(shape=(self.samples_number*self.total_range,1))
	 	self.z_matriz = np.zeros(shape=(self.samples_number*self.total_range,1))


	 def Kalman_Velocty(self, vel_data):
		offset = vel_data[0]
		data = vel_data - offset
	 	(S_filtered_state_means, S_filtered_state_covariances) = self.kf.smooth(data)
	 	self.Kalman_vel = S_filtered_state_means[:,1] + offset


	 def Point_cloud_Create(self, name, save):
	 	"""
	 	Crea la nube de puntos a partir de las medidas de laser dadas

		save = indica si se guarda en disco la nbe de puntos
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	z_prev = 0
		for i in range(0,self.samples_number-1):
			# self.velocity = self.data_laser[i,0]
			self.velocity = self.Kalman_vel[i]

			for j in range(0,self.total_range):
				self.x_vector[j] = (self.data_laser[i,j+2+self.InicialOffset]/1000.0)*math.cos(self.angles[j+self.InicialOffset])*self.factor_correccion
				self.y_vector[j] = (self.data_laser[i,j+2+self.InicialOffset]/1000.0)*math.sin(self.angles[j+self.InicialOffset])
				self.z_vector[j] = z_prev+self.velocity*(1.0/40.0)*(-1)
			z_prev = self.z_vector[j]

			if self.velocity>0:
				self.x_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.x_vector
				self.y_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.y_vector
				self.z_matriz[(i*self.total_range):((i+1)*self.total_range)] = self.z_vector

		if np.mean(self.Kalman_vel) > self.vel_limit:
			self.Upsample = True
		else:
			if np.amax(self.Kalman_vel) > self.vel_limit and ( (np.amax(self.Kalman_vel)-np.mean(self.Kalman_vel))/np.mean(self.Kalman_vel) <0.12 ):
				self.Upsample = True
			else:
				pass

		if self.Upsample:
			self.Upsampling_ALL()

		self.original_pointcloud = pcl.PointCloud(self.x_matriz.shape[0])
		matriz_final = np.asarray(self.original_pointcloud)
		matriz_final[:,0] = self.x_matriz[:,0]
		matriz_final[:,1] = self.y_matriz[:,0]
		matriz_final[:,2] = self.z_matriz[:,0]

		if (save): 
			self.original_pointcloud.to_file(self.Folder+"/"+name+".pcd")


	 def Upsampling_ALL(self):
	 	"""
	 	Crea la nube de puntos a partir de las medidas de laser dadas

		save = indica si se guarda en disco la nbe de puntos
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	z_prev = self.Kalman_vel[0]*(1.0/40.0)*(-1)

		for i in range(0,self.samples_number-2):
			self.velocity = self.Kalman_vel[i+1]

			for j in range(0,self.total_range):
				if (self.data_laser[i,j+2+self.InicialOffset]) < (self.data_laser[i+1,j+2+self.InicialOffset]):
					k = i
				else:
					k = i+1
				self.x_vector[j] = (self.data_laser[k,j+2+self.InicialOffset]/1000.0)*math.cos(self.angles[j+self.InicialOffset])*self.factor_correccion
				self.y_vector[j] = (self.data_laser[k,j+2+self.InicialOffset]/1000.0)*math.sin(self.angles[j+self.InicialOffset])
				self.z_vector[j] = z_prev+self.velocity*(1.0/80.0)*(-1)

			z_prev = float(z_prev+self.velocity*(1.0/40.0)*(-1))
			
			
			self.x_matriz = np.concatenate((self.x_matriz,self.x_vector), axis=0)
			self.y_matriz = np.concatenate((self.y_matriz,self.y_vector), axis=0)
			self.z_matriz = np.concatenate((self.z_matriz,self.z_vector), axis=0)


	 def Filter_distance(self, input_pointcloud, x_limit, y_limit, name, save):
	 	"""
		Lleva a cabo un filtrado de distancias a partir de los limites dados en "x" y "y"

		x_limit = limites en el eje x
		y_limit = limites en el eje y
		save = indica si se guarda en disco la nbe de puntos filtrada
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	#self.original_pointcloud = pcl.load(name[0:-4]+".pcd")
	 	filter_pass = input_pointcloud.make_passthrough_filter()
	 	filter_pass.set_filter_field_name("x")
		filter_pass.set_filter_limits(x_limit[0], x_limit[1])
		self.filtered_pointcloud_d = filter_pass.filter()

		filter_pass2 = self.filtered_pointcloud_d.make_passthrough_filter()
		filter_pass2.set_filter_field_name("y")
		filter_pass2.set_filter_limits(y_limit[1]*(-1), y_limit[0]*(-1))
		self.filtered_pointcloud_d = filter_pass2.filter()	

		if (self.filtered_pointcloud_d.size>0):
			if (save):
				self.filtered_pointcloud_d.to_file(self.Folder+"/"+name+"_filtered.pcd")
	 	else:
	 		self.error = True

	 def Ground_extraction_Normal(self, filtered_pointcloud, name, save):
	 	segment_ext = filtered_pointcloud.make_segmenter_normals(ksearch=50)
	 	segment_ext.set_optimize_coefficients(True)
	 	segment_ext.set_model_type(pcl.SACMODEL_PERPENDICULAR_PLANE)
	 	segment_ext.set_method_type(pcl.SAC_RANSAC)
	 	segment_ext.set_eps_angle(30.0 * (math.pi/180.0))
	 	segment_ext.set_axis(0.0, 1.0, 0.0)
	 	segment_ext.set_max_iterations(300)
	 	segment_ext.set_distance_threshold(0.05)

 		indices, model = segment_ext.segment()
 		print "El modelo del suelo es:", model

 		if not model == []:
	 		self.TanAngleX = model[2]
	 		self.TanAngleZ = model[0]*(-1)
	 	else:
	 		pass

 		# self.plane_pointcloud = filtered_pointcloud.extract(indices,negative=False)
 		self.without_plane_pointloud = filtered_pointcloud.extract(indices,negative=True)

	 	if (self.without_plane_pointloud.size>0):
	 		if (save):
		 		self.without_plane_pointloud.to_file(self.Folder+"/"+name+"_NoGround.pcd")
	 	else:
	 		self.error = True


	 def CalculateAngle(self, TanAngleIn):
	 	Angle = 0.0000
		Angle= math.atan2(TanAngleIn,1)
		return Angle

class Rplidar_Cloud:
	 """
	 Creando
	 """

	 def __init__(self, Original_Folder, velocity, inicial_angle, save_original=False, save_filtered=False, save_No_ground=False, 
	 	x_limit=np.array([0.7, 5.0]), y_limit=np.array([-5.0, 5.0]), Right = True, Upsample = True):
	 	"""
	 	Regresa un objeto Point_cloud con los siguientes parametros:


	 	inicial_angle = angulo inicial

	 	x_vector = Vector auxiliar con los valores en el eje "X" de cada medida
	 	y_vector = Vector auxiliar con los valores en el eje "Y" de cada medida
	 	z_vector = Vector auxiliar con los valores en el eje "Z" de cada medida

	 	x_Matriz = Matriz final con los valores en el eje "X" de la nube de puntos
	 	y_Matriz = Matriz final con los valores en el eje "Y" de la nube de puntos
	 	z_Matriz = Matriz final con los valores en el eje "Z" de la nube de puntos

	 	velocity = elocidad a utilizar



	 	"""

	 	self.inicial_angle = inicial_angle

	 	self.velocity = velocity

	 	# print "La velocidad de prueba es",self.velocity

	 	self.original_pointcloud = pcl.PointCloud()
	 	self.filtered_pointcloud_d = pcl.PointCloud()
	 	self.plane_pointcloud = pcl.PointCloud()
	 	self.without_plane_pointloud = pcl.PointCloud()


	 	self.error = False
		self.Upsample = Upsample

	 	self.TanAngleX = 0.0000
	 	self.TanAngleZ = 0.0000

	 	#self.grow_velocity()
	 	self.Set_Parameters(save_original, save_filtered, save_No_ground, Original_Folder, x_limit, y_limit, Right)
	 	
	 	
	 def Set_Parameters(self, save_original, save_filtered, save_No_ground, Original_Folder, x_limit, y_limit, Right):

	 	#Parametros para Point_cloud_Create
	 	self.save_original = save_original
	 	self.Folder = Original_Folder

	 	# Parametros para Filter_distance
	 	self.x_limit = x_limit
	 	self.y_limit = y_limit
	 	self.save_filtered = save_filtered

	 	# Parametros para Ground_extraction_Normal
	 	self.save_No_ground = save_No_ground
	 	self.Right = Right


	 def Angular_Vector_Create(self, range_t, escaneo):
	 	"""
	 	Crea el vector de grados para la nube de puntos


	 	"""
	 	i = escaneo
	 	self.grados = np.zeros(shape=(range_t,1))
	 	aux = self.inicial_angle
		for j in range(0, range_t):
			self.grados[j] = ((math.pi*(self.data_laser[i][j][1]+aux))/180.0)
			if self.Right:
				self.correccion_Right = -1.0
			else:
				self.correccion_Right = 1.0
			# print self.data_laser[i][j][1], math.degrees(self.grados[j]), math.cos(self.grados[j])*self.correccion_Right, math.sin(self.grados[j])*(-1)

	 def Put_Laser(self, data_laser):
	 	self.error = False
	 	self.data_laser = data_laser
	 	self.numero_medidas = data_laser.shape[0]

	 	self.x_matriz = np.zeros(shape=(1,))
	 	self.y_matriz = np.zeros(shape=(1,))
	 	self.z_matriz = np.zeros(shape=(1,))


	 def Point_cloud_Create(self, name, save):
	 	"""
	 	Crea la nube de puntos a partir de las medidas de laser dadas

		save = indica si se guarda en disco la nbe de puntos
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	


	 	z_prev = 0
	 	# print self.numero_medidas, "MEDIDDAS"
		for i in range(0,self.numero_medidas):
			self.total_range = self.data_laser[i].shape[0]
			if self.total_range>0:
				self.x_vector = np.zeros(shape=(self.total_range,))
				self.y_vector = np.zeros(shape=(self.total_range,))
				self.z_vector = np.zeros(shape=(self.total_range,))
				self.Angular_Vector_Create(self.total_range, i)
				# print self.numero_medidas, self.total_range
				for j in range(0,(self.total_range)):
					self.x_vector[j] = (self.data_laser[i][j][2]/1000.0)*math.cos(self.grados[j])*(self.correccion_Right)
					self.y_vector[j] = (self.data_laser[i][j][2]/1000.0)*math.sin(self.grados[j])*(-1)
					self.z_vector[j] = z_prev+self.velocity*(1.0/10.0)

				z_prev = self.z_vector[-1]
				# print z_prev, self.z_vector[j]
				self.x_matriz = np.hstack((self.x_matriz,self.x_vector))
				self.y_matriz = np.hstack((self.y_matriz,self.y_vector)) 
				self.z_matriz = np.hstack((self.z_matriz,self.z_vector))
			else:
				pass

		if self.Upsample:
			self.Upsampling_ALL()
		else:
			pass
			
		self.original_pointcloud = pcl.PointCloud(self.x_matriz.shape[0])
		matriz_final = np.asarray(self.original_pointcloud)

		matriz_final[:,0] = self.x_matriz
		matriz_final[:,1] = self.y_matriz
		matriz_final[:,2] = self.z_matriz

		if (save): 
			self.original_pointcloud.to_file(self.Folder+"/"+name+".pcd")


	 def Upsampling_ALL(self):
	 	"""
	 	Crea la nube de puntos a partir de las medidas de laser dadas

		save = indica si se guarda en disco la nbe de puntos
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	z_prev = self.velocity*(1.0/10.0)

		for i in range(0,self.numero_medidas-1):
			self.total_range = self.data_laser[i].shape[0]
			range_comparate = self.data_laser[i+1].shape[0]
			self.x_vector = np.zeros(shape=(self.total_range,))
			self.y_vector = np.zeros(shape=(self.total_range,))
			self.z_vector = np.zeros(shape=(self.total_range,))
			self.Angular_Vector_Create(self.total_range, i)
			# print self.numero_medidas, self.total_range, range_comparate-1, range_comparate, type(range_comparate-1), type(i+1)
			for j in range(0,self.total_range-1):
				if (range_comparate-1 >= j):
					if (self.data_laser[i][j][2]/1000.0) < (self.data_laser[i+1][j][2]/1000.0):
						k = i
					else:
						k = i+1
				else:
					k = i
				self.x_vector[j] = (self.data_laser[k][j][2]/1000.0)*math.cos(self.grados[j])*(self.correccion_Right)
				self.y_vector[j] = (self.data_laser[k][j][2]/1000.0)*math.sin(self.grados[j])*(-1)
				self.z_vector[j] = z_prev+self.velocity*(1.0/20.0)
			# print self.z_vector[j], "EHHH"
			z_prev = float(z_prev+self.velocity*(1.0/10.0))
			
			
			self.x_matriz = np.concatenate((self.x_matriz,self.x_vector), axis=0)
			self.y_matriz = np.concatenate((self.y_matriz,self.y_vector), axis=0)
			self.z_matriz = np.concatenate((self.z_matriz,self.z_vector), axis=0)


	 def Filter_distance(self, input_pointcloud, x_limit, y_limit, name, save):
	 	"""
		Lleva a cabo un filtrado de distancias a partir de los limites dados en "x" y "y"

		x_limit = limites en el eje x
		y_limit = limites en el eje y
		save = indica si se guarda en disco la nbe de puntos filtrada
	 	name: nombre del archivo pd a guardar (si save=True)
	 	"""
	 	#self.original_pointcloud = pcl.load(name[0:-4]+".pcd")
	 	filter_pass = input_pointcloud.make_passthrough_filter()
	 	filter_pass.set_filter_field_name("x")
		filter_pass.set_filter_limits(x_limit[0], x_limit[1])
		self.filtered_pointcloud_d = filter_pass.filter()

		filter_pass2 = self.filtered_pointcloud_d.make_passthrough_filter()
		filter_pass2.set_filter_field_name("y")
		filter_pass2.set_filter_limits(y_limit[1]*(-1), y_limit[0]*(-1))
		self.filtered_pointcloud_d = filter_pass2.filter()	
		print self.filtered_pointcloud_d.size, x_limit, "debug"
		if (self.filtered_pointcloud_d.size>0):
			if (save):
				self.filtered_pointcloud_d.to_file(self.Folder+"/"+name+"_filtered.pcd")
	 	else:
	 		self.error = True

	 def Ground_extraction_Normal(self, filtered_pointcloud, name, save):
	 	segment_ext = filtered_pointcloud.make_segmenter_normals(ksearch=50)
	 	segment_ext.set_optimize_coefficients(True)
	 	segment_ext.set_model_type(pcl.SACMODEL_PERPENDICULAR_PLANE)
	 	segment_ext.set_method_type(pcl.SAC_RANSAC)
	 	segment_ext.set_eps_angle(30.0 * (math.pi/180.0))
	 	segment_ext.set_axis(0.0, 1.0, 0.0)
	 	segment_ext.set_max_iterations(300)
	 	segment_ext.set_distance_threshold(0.05)

 		indices, model = segment_ext.segment()
 		print "El modelo del suelo es:", model
 		if not model == []:
	 		self.TanAngleX = model[2]
	 		self.TanAngleZ = model[0]*(-1)
	 	else:
	 		pass
 		# self.plane_pointcloud = filtered_pointcloud.extract(indices,negative=False)
 		self.without_plane_pointloud = filtered_pointcloud.extract(indices,negative=True)

	 	if (self.without_plane_pointloud.size>0):
	 		if (save):
		 		self.without_plane_pointloud.to_file(self.Folder+"/"+name+"_NoGround.pcd")
	 	else:
	 		self.error = True


	 def CalculateAngle(self, TanAngleIn):
	 	Angle = 0.0000
		Angle= math.atan2(TanAngleIn,1)
		return Angle
