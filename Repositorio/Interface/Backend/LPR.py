#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2 
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt


def GetBiggestPlate(results):
    plates,Positions = results.keys(),np.asarray(results.values())
    if  len(Positions)!= 0:
        idx = np.argmax(Positions[:,2])
        return {plates[idx]:Positions[idx]}
    else:
        return {"NOPLATE":[0,0,0,0]}
    
    

def onehot2string(plate_one_hot):
    plate = ""
    score = 1.
    for result in plate_one_hot:
        idx =np.argmax(result)
        plate = plate +label2char(idx)
        score = score*result[0,idx]
    return plate,score



def randomPlate():
    plate = np.random.uniform(size=(6,))
    plate[:3] =  plate[:3]*26 
    plate[3:] =  plate[3:]*10 +26
    plate = plate.astype(int)
    plate_string=""
    for i in range(6):
        plate_string = plate_string+label2char(plate[i])
    return plate_string

def DetectionTolerance(plates,tolerance):
    
    plates[:,1] = plates[:,1] - np.floor(plates[:,3]*tolerance+0.5)
    plates[:,3] = plates[:,3] + np.floor(plates[:,3]*tolerance+0.5)
    plates[plates<0] =0
    return plates

def showAnnotations(img,result,plate_position=None,title='img'):
    for key in result:
        x,y,w,h = result[key]
        cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),1)
        cv2.putText(img,key,(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0),thickness=2)
    if plate_position !=None:
        x,y,w,h =plate_position 
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),1)
    cv2.imshow(title,img)
    cv2.waitKey(0)

def label2char(label):
    if label<26:
        return chr(label+65)
    else:
        return chr(label+47-25)

class LPR:
    
    def __init__(self,platedetector,platelector):
        self.platedetector = cv2.CascadeClassifier(platedetector)
        # self.chardetector  = cv2.CascadeClassifier('coc2.xml')
        # self.charclassifier = load_model('my_model1.h5')
        self.PlateLector = load_model(platelector)
    
    
    def RecognizeLetters(self,img_crop,size):
        
        # img_crop = cv2.cvtColor(img_crop, cv2.COLOR_BGR2GRAY)
        img_crop = cv2.resize(img_crop,dsize=size)
        img_crop=img_crop.reshape(-1,size[1],size[0],1)
        plate,score = onehot2string(self.PlateLector.predict(img_crop, batch_size=32))
        return plate,score


    def RecognizePlatesWithCNN(self, img,size=(64,32),scale=1.1,neighbors=5,show=True,tolerance=0.0,min_size=(0,0),scorethd =0.0,biggestPlate=False):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        plates = np.asarray(self.platedetector.detectMultiScale(gray, scale, neighbors,0,min_size)).reshape((-1,4))
        # print plates
        plates = DetectionTolerance(plates,tolerance)
        # print plates
        results ={}
        for (x,y,w,h) in plates:
            img_crop = gray[y:y+h,x:x+w]
            plate,score = self.RecognizeLetters(img_crop,size)
            if score >scorethd:
                results[plate] = [x,y,w,h]
            if show:
                cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),1)
                cv2.putText(img,plate,(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255),thickness=2)


        if show:
            cv2.imshow('result',img)
            cv2.waitKey(0)
            # plt.imshow(img[:,:,::-1],cmap="gray" )
            # plt.show()
        if GetBiggestPlate:
            results = GetBiggestPlate(results)
        return results
    
if __name__ == "__main__":
    import glob
    import time
    import os
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
    os.environ["CUDA_VISIBLE_DEVICES"] = ""
    # lpr = LPR('my_model3.h5')
    # lpr = LPR('co10.xml','my_model9.h5')
    lpr = LPR('PlatesDetectors/co10.xml','PlatesReaders/my_model10.h5')
        
    files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/validate_chars_detection/*.png')
    # files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/Univalle3/*.png')
    files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/Descargadas/*.jpg')
    files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/Carros google/*.jpg')

    files  = glob.glob('/home/ssi_ralvarez/Documentos/Dataset_placas/Anexo 10 Imagenes Pruebas de integraciвn/pos2/*.png')
    for  file in files:
        img = cv2.imread(file)
        # rows,cols,_ = img.shape
        # M = cv2.getRotationMatrix2D((cols/2,rows/2),0,1)
        
        # img = cv2.warpAffine(img,M,(cols,rows))
        # ini = time.time()        
        # img =changeCharsOrder(img,result)
        # result = lpr.DetectPlates(img)
        # print result
        # showAnnotationsAndChars(img,result)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)

        results = lpr.RecognizePlatesWithCNN(img,size=(96,48),scale=1.1,neighbors=2,show=False,scorethd =0.3)
        showAnnotations(img,results)
        # cv2.imshow('img',img)
        # cv2.waitKey(0)
        