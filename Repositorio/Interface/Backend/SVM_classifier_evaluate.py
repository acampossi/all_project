import os
import sys
import numpy as np
import math
import time
from  PyQt4 import QtGui,QtCore
import glob
from sklearn.cluster import MiniBatchKMeans, KMeans
from sklearn.decomposition import PCA
from sklearn import svm
from sklearn.metrics import confusion_matrix
import itertools
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn import (manifold, datasets, decomposition, ensemble,
                     discriminant_analysis, random_projection)
# os.system("taskset -p 0xff %d" % os.getpid()) 
# pylint: disable=line-too-long
# -*- coding: utf-8 -*-
# -*- coding: 850 -*-

## la clase Vehicle_Category_Classifier  permite entrenar, evaluar, guardar  o cargar  modelos para la clasificacion
#  basada en bolsa de caracteristicas mediante K means y maquinas de soporte vectorial
# @param n_Clusters  numero de clusters del clasificador
# @param n_Classes   numero de clases del clasificadoor
# @param k_means_dic Diccionario que contiene los  modelos Kmeans entrenados
# @param SVM_dic     Diccionario que contiene las maquinas de vectores de soporte entrenadas
# @param histogram_index bins asociados a cada uno de los centroides de los  kmeans
# @param km_selected LLave asociada al kmeans a utilizar de acuerdo al algoritmo de seleccion de centroides
# @param svm_selected  LLave asociada a svm a utilizar de acuerdo al kernel
# @param prototype  LLave que informa si la clasificacion se realiza para nubes capturadas con el sensor RPLIDAR
class Vehicle_Category_Classifier:
    
    ## Constructor 
    # @param n_clusters numero de clusters a crear para el entrenamiento de k means, por defecto 20
    # @param n_classes  numero de clases a crear, por defecto 2
    # @param km_selected LLave asociada al kmeans a utilizar de acuerdo al algoritmo de seleccion de centroides
    # @param svm_selected  LLave asociada a svm a utilizar de acuerdo al kernel
    # @param prototype  LLave que informa si la clasificacion se realiza para nubes capturadas con el sensor RPLIDAR
    def __init__(self, n_clusters=20, n_classes=2, km_selected=None, svm_selected=None, prototype=False):
        
        self.n_Clusters = n_clusters #numero de clusters del clasificador
        self.n_Classes = n_classes   #numero de clases del clasificadoor
        self.k_means_dic = {}
        self.SVM_dic = {}
        self.histogram_index = range(self.n_Clusters+1)
        self.km_selected= km_selected
        self.svm_selected = svm_selected
        self.prototype = prototype


    ## Entrena k means con centroides seleccionados usando: k-means++, PCA y Seleccion aleatoria.
    # Posteriormente entrena maquinas de soporte vectorial RBF,Lineales y polinomiales y las almacena en diccionarios para ser evaluados
    # @param Vehicle_Descriptors  arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
    #                             [sum(descriptors_labels), Numero_caracteristicas_descriptor]
    # @param descriptors_labels   arreglo tipo columna con el numero de descriptores por cada vehiculo
    # @param Vehicle_Descriptors  arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
    # @param geometry_descriptors  matriz con el largo y la altura (en el prmer eje) de cada vehiculo
    # @param class_weight  se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
    #                      por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
    def train(self, Vehicle_Descriptors, descriptors_labels, vehicle_labels, geometry_descriptors = None, class_weight=None):

        # entrenar los kmeans mediante diferentes metodos
        self.trainKmeans(Vehicle_Descriptors)
        # por cada kmeans se realiza el entrenamiento de los svms
        self.trainsvms(Vehicle_Descriptors, descriptors_labels, vehicle_labels, geometry_descriptors, class_weight=None)

    ## Entrenar maquinas de soporte vectorial con todas las combinaciones posibles
    # @param Vehicle_Descriptors  arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
    #                             [sum(descriptors_labels), Numero_caracteristicas_descriptor]
    # @param descriptors_labels   arreglo tipo columna con el numero de descriptores por cada vehiculo
    # @param vehicle_labels  arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
    # @param geometry_descriptors  matriz con el largo y la altura (en el prmer eje) de cada vehiculo
    # @param class_weight  se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
    #                      por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
    def trainsvms(self, Vehicle_Descriptors, descriptors_labels, vehicle_labels, geometry_descriptors = None, class_weight=None):
        # por cada kmeans se realiza el entrenamiento de los svms
        for key, kmean in self.k_means_dic.iteritems():
            # se evaluan los datos para cada  K-means  y se generan los histogramas
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, Vehicle_Descriptors, descriptors_labels, geometry_descriptors)
            # Se entrenan los SVM con los histogramas generados
            self.train_svm_kmeans_selected(key, svm_input, vehicle_labels, class_weight=class_weight)



    ## Entrena maquinas de soporte vectorial con  los histogramas obtenidos mediante k means ya decodificados por k means
    # @param input_data  Histogramas obtenidos mediante la evaluacion de K -means
    # @param labels  Etiquetas de los vehiculos a clasificar
    # @param C      Penalizacion maquinas de soporte vectorial para datasets desvalanceados
    # @param Gamma  ancho de la gaussiana para la maquina de soporte vectorial con kernel de base radial, por defecto 0.7
    # @param Degree  Grado del polinomio para la maquina de soporte vectorial con kernel polinomial
    # @param class_weight  se definen que ponderacion se le da a las clases en el momento de clasificar los cuales seran
    #                      por defecto None, los pesos se entregan como un diccionario con el nombre de la clase y el peso
    def train_svm_kmeans_selected(self, key, input_data, labels, C=1, Gamma=0.05, Degree=3, class_weight=None):
        print "Entrenando clasificadores..."
        # Se entrenan SVMs con distintas topologias:
        # lineal...
        self.SVM_dic[(key, 'lin')] = svm.SVC(kernel='linear', C=C, class_weight=class_weight, probability=False).fit(input_data, labels)
        # rbf...
        # self.SVM_dic[(key, 'rbf')] = svm.SVC(kernel='rbf', gamma=Gamma, C=C, class_weight=class_weight).fit(input_data, labels)
        # Polinomial
        self.SVM_dic[(key, 'poly')] = svm.SVC(kernel='poly', degree=Degree, C=C, class_weight=class_weight, probability=False).fit(input_data, labels)
        # lineal segunda topologia
        self.SVM_dic[(key, 'lin-SVC')] = svm.LinearSVC(C=C, class_weight=class_weight).fit(input_data, labels)
        print "Finalizado.."


    ## Entrena modelos de K means utilizando los descriptores extraidos a los vehiculos a clasificar
    # @param Vehicle_Descriptors  matriz con descriptores de los vehiculos  de dimensiones [numero_descriptores,dimensiones_descritor]
    def trainKmeans(self, Vehicle_Descriptors):
        batch_size = 64
        print "Entrenando Kmeans..."
        # Entrenar  con centroides iniciales definidos mediante K-means++
        self.k_means_dic['k-means++'] = MiniBatchKMeans(init='k-means++', n_clusters=self.n_Clusters, n_init=10, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        #self.k_means_dic['k-means++'] = KMeans(init='k-means++', n_clusters=self.n_Clusters, n_init=10, max_iter=2000).fit(Vehicle_Descriptors)
        # Entrenar  con centroides iniciales definidos aleatoriamente con 10 intentos
        self.k_means_dic['random'] = MiniBatchKMeans(init='random', n_clusters=self.n_Clusters, n_init=10, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        #self.k_means_dic['random'] = KMeans(init='random', n_clusters=self.n_Clusters, n_init=10, max_iter=2000).fit(Vehicle_Descriptors)
        # Definicion de centroides iniciales mediante analisis de componentes principales
 
        pca = PCA(n_components=self.n_Clusters).fit(Vehicle_Descriptors)
        # # Entrenar K means utilizando los centroides definidos
        self.k_means_dic['PCA-Components'] = MiniBatchKMeans(init=pca.components_, n_clusters=self.n_Clusters, n_init=1, batch_size=batch_size, max_no_improvement=10, verbose=0).fit(Vehicle_Descriptors)
        # #self.k_means_dic['PCA-Components'] = KMeans(init=pca.components_, n_clusters=self.n_Clusters, n_init=1, max_iter=2000).fit(Vehicle_Descriptors)
        print "Finalizado.."


    ## Evalua los  k means ya entrenados y genera los vectores de caracteristicas incluyendo los descriptores geometricos
    # @param vehicle_descriptors  matriz con descriptores de los vehiculos  de dimensiones [numero_descriptores,dimensiones_descritor]
    # @param descriptors_labels   arreglo tipo columna con el numero de descriptores por cada vehiculo
    # @param geometry_descriptors matriz con el largo y la altura (en el prmer eje) de cada vehiculo
    # @return Vectores de caracteristicas para ser envaluados por las SVM
    
    def EvaluateKmeans_CreateHistograms(self, k_means, vehicle_descriptors, descriptors_labels, geometry_descriptors = None):
        # evaluar Kmeans
        k_result = k_means.predict(vehicle_descriptors)
        # concatenar un 0 a los descriptors_labels para separar las caracteristicas de cada muestre y crear los histogramas
        c_limits = np.concatenate((np.zeros(1), np.cumsum(descriptors_labels)), axis=0)
        # prepara el vector de descriptores geometricos para su posterior concatenacion
        if geometry_descriptors is not None:
            desc_geometry = np.array(geometry_descriptors)

            if self.prototype:
                svm_input = np.zeros((descriptors_labels.__len__(), self.n_Clusters+1))
            else:
                # crear matriz de ceros para guardar las  caracteristicas  de entrenamiento de los svms
                svm_input = np.zeros((descriptors_labels.__len__(), self.n_Clusters+desc_geometry.shape[1]))
        else:
	        # crear matriz de ceros para guardar las  caracteristicas  de entrenamiento de los svms
	        svm_input = np.zeros((descriptors_labels.__len__(), self.n_Clusters))
        # iterar sobre los decriptors labels:
        for i in range(1, len(c_limits)):
            # crear los histogramas
            top = int(c_limits[i-1])
            bottom = int(c_limits[i])
            sample_descriptors, bins = np.histogram(k_result[top:bottom], bins=self.histogram_index)
            # guardar el histograma en la matriz de ceros
            # svm_input[i-1, :] = sample_descriptors     #retornar la matriz de caracteristicas
            if geometry_descriptors is not None:

                if self.prototype:
                    desc_geometryAUX = np.zeros((1,1))
                    desc_geometryAUX[0]= desc_geometry[i-1]
                    svm_input[i-1, :] =  np.concatenate((sample_descriptors, desc_geometryAUX[0]), axis=0)
                    # print sample_descriptors.shape, desc_geometryAUX[0].shape, desc_geometryAUX.shape, "OIGAN"
                else:
                    svm_input[i-1, :] =  np.concatenate((sample_descriptors, desc_geometry[i-1]), axis=0)    #retornar la matriz de caracteristicas
            else:
                svm_input[i-1, :] = sample_descriptors
        return  svm_input

    ## Retorna  la tasa de acierto de los clasificadores entrenados
    # @param vehicle_descriptors  arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
    #                      [sum(descriptors_labels), Numero_caracteristicas_descriptor]
    # @param descriptors_labels   arreglo tipo columna con el numero de descriptores por cada vehiculo
    # @param vehicle_labels  arreglo tipo columna con las etiquetas de los vehiculos a utilizar para entrenar
    # @param geometry_descriptors  matriz con el largo y la altura (en el prmer eje) de cada vehiculo
    # @retun tasa de acierto de los clasificadores entrenados
    def Calculate_Accuracy_Classifiers(self, vehicle_descriptors, descriptors_labels, vehicle_labels, geometry_descriptors = None):
        # cacular el porcentaje de acierto del clasificador de 0 a 1
        Acc_Dict = {}
        for key, kmean in self.k_means_dic.iteritems():
            # evaluar K - means
            svm_input = self.EvaluateKmeans_CreateHistograms(kmean, vehicle_descriptors, descriptors_labels, geometry_descriptors)
            for keysvm, svms in self.SVM_dic.iteritems():
                # evaluar unicamente en que el K means coincida con el SVM
                if key == keysvm[0]:
                    # evaluar SVM
                    Result = svms.predict(svm_input)
                    # calcular el porcentaje de acierto del clasificador
                    acc = accuracy_score(Result, vehicle_labels)
                    Acc_Dict[keysvm] = acc
        return Acc_Dict

    ## Guardar los modelos de k means y maquinas de soporte vectorial entrenadas, en caso de no hber sido entrenadas o  cargadas no se guardara nada
    # @param path Ruta a la carpeta donde se desea guardar los clasificadores, en caso de no existir sera creada
    def SaveModels(self, path):

        create_directory(path)
        for key, kmean in self.k_means_dic.iteritems():
            p = path +"/"+key+".pkl"
            joblib.dump(kmean, p)
            print p

        for keysvm, svms in self.SVM_dic.iteritems():
            p = path +"/"+keysvm[0]+'_'+keysvm[1]+".pkl"
            joblib.dump(svms, p)


    
    ## cargar modelos preentrenados y entrega una instalcia de un objeto Vehicle_Category_Classifier
    # @param path Ruta a la carpeta donde se almacenados los clasificadores
    # @param km_selected LLave asociada al kmeans a utilizar de acuerdo al algoritmo de seleccion de centroides
    # @param svm_selected  LLave asociada a svm a utilizar de acuerdo al kernel
    @staticmethod
    def LoadModels(path, km_selected, svm_selected):
        k_m = ['k-means++', 'random', 'PCA-Components']
        s_vm = ['lin-SVC', 'rbf', 'lin', 'poly']
        k_means_dic = {}
        n_clusters = 0
        n_classes = 0
        SVM_dic = {}
        for k in k_m:
            k_means_dic[k] = joblib.load(path + '/' + k+'.pkl')
            n_clusters = k_means_dic[k].n_clusters

            for svm in s_vm:
                SVM_dic[(k, svm)] = joblib.load(path + '/' + k+'_'+svm+'.pkl')
                n_classes = SVM_dic[(k, svm)].classes_.size
        
        r = Vehicle_Category_Classifier(n_classes=n_classes, n_clusters=n_clusters, km_selected = km_selected, svm_selected =svm_selected)
        r.SVM_dic = SVM_dic
        r.k_means_dic = k_means_dic
        return r



    ## cargar modelos preentrenados y entrega una instalcia de un objeto Vehicle_Category_Classifier
    # @param path Ruta a la carpeta donde se almacenados los clasificadores
    # @param km_selected LLave asociada al kmeans a utilizar de acuerdo al algoritmo de seleccion de centroides
    # @param svm_selected  LLave asociada a svm a utilizar de acuerdo al kernel
    @staticmethod
    def LoadModels_M(path, km_selected, svm_selected):

        k_m = ['k-means++', 'random']
        s_vm = ['lin', 'poly']
        k_means_dic = {}
        n_clusters = 0
        n_classes = 0
        SVM_dic = {}
        for k in k_m:
            k_means_dic[k] = joblib.load(path + '/' + k+'.pkl')
            n_clusters = k_means_dic[k].n_clusters

            for svm in s_vm:
                SVM_dic[(k, svm)] = joblib.load(path + '/' + k+'_'+svm+'.pkl')
                n_classes = SVM_dic[(k, svm)].classes_.size
        
        r = Vehicle_Category_Classifier(n_classes=n_classes, n_clusters=n_clusters, km_selected = km_selected, svm_selected =svm_selected)
        r.SVM_dic = SVM_dic
        r.k_means_dic = k_means_dic
        return r

    ## cargar modelos preentrenados y entrega una instalcia de un objeto Vehicle_Category_Classifier
    # @param path Ruta a la carpeta donde se almacenados los clasificadores
    @staticmethod
    def LoadModels_Gl(path):
        k_m = ['key']
        s_vm = ['lin-SVC', 'lin', 'poly']
        k_means_dic = {}
        n_clusters = 0
        n_classes = 0
        SVM_dic = {}
        for k in k_m:
            pass
            for svm in s_vm:
                SVM_dic[(k, svm)] = joblib.load(path + '/' + k+'_'+svm+'.pkl')
                n_classes = SVM_dic[(k, svm)].classes_.size
        
        r = Vehicle_Category_Classifier(n_classes=n_classes)
        r.SVM_dic = SVM_dic
        r.k_means_dic = k_means_dic
        return r

    ## Evalua el clasificador y entrega la clase a la cual pertenecen los descriptores entregados
    # @param vehicle_descriptors  arreglo con los descriptores de los vehiculos el cual debe tener dimensiones
    #                      [sum(descriptors_labels), Numero_caracteristicas_descriptor]
    # @kmean  Kmean a utilizar
    # @param svm maquina de soporte vectorial a utilizar
    # @param geometry_descriptors  matriz con el largo y la altura (en el prmer eje) de cada vehiculo
    # @retun clase a la cual pertenece el objeto evaluado
    def predict(self, vehicle_descriptors, kmean,svm, geometry_descriptors = None):
        svm_input = self.EvaluateKmeans_CreateHistograms(self.k_means_dic[kmean], vehicle_descriptors,[vehicle_descriptors.shape[0]], geometry_descriptors)
        Result = self.SVM_dic[(kmean,svm)].predict(svm_input)
        try:
                dummy_R = self.SVM_dic[(kmean,svm)].predict_log_proba(svm_input)
                dummy_R2 = self.SVM_dic[(kmean,svm)].predict_proba(svm_input)
        except Exception as e:
            pass
        else:
            pass
        finally:
            pass
        return Result     

    ## permite realizar un  submuestreo de los datos de entrenamiento disciminando por clases
    # @param features descritores
    # @param etiquetas  de los descriptores entregados
    # @param proportion diccionario que relaciona las clases con la cantidad de submuestreo que se desea realizar
    # @return Entrega las  caracteristicas y las clases con el submuestreo definido
    def SubSample(self,features,labels,proportion):
        labels_out = []
        features_out  = []
        for key in proportion:
            feature_class= features[np.asarray(labels)==key,:]
            if proportion[key]>1:
                np.random.shuffle(feature_class)
                feature_class=feature_class[::proportion[key],:]
            labels_out =labels_out + [key]*feature_class.shape[0]
            if features_out !=[]:
                features_out =np.concatenate((features_out,feature_class))
            else:
                features_out = np.copy(feature_class)
        return features_out,labels_out

    
    # def concatenate_pca_global_geo(self, Operacion, Data, nombre, N_C, pca_b =False, geometry_descriptors = None):
    #     if pca_b:
    #         if Operacion == "Train":
    #             # pca = PCA(n_components=N_C)
    #             pca = PCA(n_components=0.90,svd_solver = 'full' )
    #             pca.fit(Data)
    #             create_directory(nombre)
    #             joblib.dump(pca, nombre+"/"+os.path.split(nombre)[1]+'_pca.pkl')
    #             DataX = pca.transform(Data)
    #             print pca.n_components_, "PCA"

    #         elif Operacion == "Validate":
    #             pca = joblib.load(nombre+"/"+os.path.split(nombre)[1]+'_pca.pkl')
    #             DataX = pca.transform(Data)

    #         else:
    #             print ("ERROR, Operacion debe ser 'Train' o 'Validate'. dado el error"
    #                 +" Se ignorara el PCA")
    #             DataX = Data
    #     else:
    #         DataX = Data

    #     if geometry_descriptors is not None:
    #         desc_geometry = np.array(geometry_descriptors)

    #         if self.prototype:
    #             desc_geometryAUX= desc_geometry.reshape((desc_geometry.shape[0],1))
    #             data_out = np.concatenate((DataX,desc_geometryAUX), axis=1)
    #         else:
    #             data_out = np.concatenate((DataX,desc_geometry), axis=1)
    #     else:
    #         # crear matriz de ceros para guardar las  caracteristicas  de entrenamiento de los svms
    #         data_out = DataX
    #     return data_out



## leer  descriptores y sus etiquetas desde archivo
# @param des  si es 1 busca descriptores con nombre FPFH si es 0 busca descriptores con nombre NARF 
# @param d  descriptores
# @param dc  numero de descriptores por vehiculo
# @param l  etiquetas de los vehiculos
# @param geo  geometria de los vehiculos
def read_and_return_data(des=1, dir_f="./", name_d = ''):
    dlg = QtGui.QFileDialog()
    path1 = str(dlg.getExistingDirectory(caption= "Seleccionar Carpeta...",directory=dir_f))
    dir_list = os.listdir(path1)
    l = []
    dc = []
    d = []
    geo = []
    aux = 0
    for dir_a in dir_list:
        print path1+'/'+dir_a
    	aux = aux+1
        l.extend(np.loadtxt(path1+'/'+dir_a+'/data.txt', usecols=(1, )))
        dc.extend(np.loadtxt(path1+'/'+dir_a+'/Descriptores_Cluster.txt'))
        geo.extend(np.loadtxt(path1+'/'+dir_a+'/geometria.txt'))
        if des:
            files = glob.glob(path1+"/"+dir_a+'/*.pcd')
            files.sort()
            if path1+"/"+dir_a+'/FPFH.pcd' in files:
                print "FPFH"
                d.extend(np.loadtxt(path1+'/'+dir_a+'/FPFH.pcd', skiprows=11))
            else:
                print "Else"
                for fnames in files:
                    d.extend(np.loadtxt(fnames, skiprows=11))
        else:
            print name_d
            if name_d == "NARF":
                d.extend(np.loadtxt(path1+'/'+dir_a+'/NARF.pcd', skiprows=11))
            else:
                files = glob.glob(path1+"/"+dir_a+'/*.pcd')
                files.sort()
                kl = 0
                for fnames in files:
                    kl = kl +1
                    d.append(np.loadtxt(fnames, skiprows=11))

        print np.array(d).shape, len(l)
            #Data=np.array(np.loadtxt(path1+'/'+dir_a+'/Narf.pcd',skiprows=11))
    d = np.array(d)
    print "Lei ", aux, " carpetas"
    return d, dc, l, geo


## verifica la existencia de la carpeta en la direccion definida, en caso de que no exista la crea
# @param path  direccion donde se desea crear la carpeta
def create_directory(path):
    if not os.path.exists(path):
        os.makedirs(path)






if __name__ == '__main__':
	pass
    
    # if len(sys.argv) == 4:
    #     name_p = str(sys.argv[1])
    #     pca_b = str(sys.argv[2])
    #     nombre = INDEX_UPDIR+"/Clasificacion/Clasificadores/"+name_p
    #     print "NOMBRE: ", nombre
    #     print ""

    #     if str(sys.argv[3]) == "Train":

    #         Data, descriptors_count, labels, geometry_descriptors = Classifier_Bag_SVM.read_and_return_data(des=1, dir_f=INDEX_UPDIR+"/Clasificacion/Dataset/FPFH")
    #         clasifier = Classifier_Bag_SVM.Vehicle_Category_Classifier(n_classes=5,n_clusters=20)
    #         clssnames=['noVeh','veh', 'busP', 'busG', 'Camion+6E ', 'Camion2E']
    #         if pca_b == "True":
    #             Data2, descriptors_count2, labels2, geometry_descriptors2 = Classifier_Bag_SVM.read_and_return_data(des=0, dir_f=INDEX_UPDIR+"/Clasificacion/Dataset/", 
    #                 name_d = 'VFH')
    #             # Data2, rnor = normalize(Data2, norm='max', axis=0, return_norm=True)
    #             geometry_descriptors = clasifier.concatenate_pca_global_geo(str(sys.argv[3]), Data2, nombre, 20, True, geometry_descriptors)
    #         else:
    #             pass

    #         clasifier.divide_samples_train_and_test(Data,descriptors_count,labels,clssnames,test_size=0.2,geometry_descriptors = geometry_descriptors, 
    #             subsampling = 1) 
    #         clasifier.SaveModels(nombre)  

    #     elif str(sys.argv[3]) == "Validate":
    #         Data, descriptors_count, labels, geometry_descriptors = Classifier_Bag_SVM.read_and_return_data(des=1, dir_f=INDEX_UPDIR+"/Clasificacion/Dataset/FPFH")
    #         clasifier = Classifier_Bag_SVM.Vehicle_Category_Classifier.LoadModels_M(nombre)
    #         clssnames=['noVeh','veh', 'busP', 'busG', 'Camion+6E ', 'Camion2E']
    #         if pca_b == "True":
    #             Data2, descriptors_count2, labels2, geometry_descriptors2 = Classifier_Bag_SVM.read_and_return_data(des=0, dir_f=INDEX_UPDIR+"/Clasificacion/Dataset/", 
    #                 name_d = 'VFH')
    #             # Data2, rnor = normalize(Data2, norm='max', axis=0, return_norm=True)
    #             geometry_descriptors = clasifier.concatenate_pca_global_geo(str(sys.argv[3]), Data2, nombre, 20, True, geometry_descriptors)

    #             clasifier.show_cofusionmatrix_classifiers(Data, descriptors_count, labels, clssnames, geometry_descriptors, True)
    #         else:
    #             pass

    #     else:
    #         print "Uso es: Classifier_Bag_SVM 'nombre' 'PCA (True o False)' 'Train o Validate' "
    # else:
    #     print "Uso es: Classifier_Bag_SVM 'nombre' 'PCA (True o False)' 'Train o Validate' "
