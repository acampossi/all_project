// #include <iostream>
// #include <pcl/ModelCoefficients.h>
// #include <pcl/point_types.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/filters/extract_indices.h>
// #include <pcl/filters/voxel_grid.h>
// #include <pcl/features/normal_3d.h>
// #include <pcl/kdtree/kdtree.h>
// #include <pcl/sample_consensus/method_types.h>
// #include <pcl/sample_consensus/model_types.h>
// #include <pcl/segmentation/sac_segmentation.h>
// #include <pcl/segmentation/extract_clusters.h>
// #include <pcl/console/parse.h>
// #include <pcl/common/common_headers.h>
// #include <pcl/filters/statistical_outlier_removal.h>

// #include <pcl/common/transforms.h>
#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
#include <pcl/segmentation/conditional_euclidean_clustering.h>

class Cluster
{
private:
	//Parametros de clusterizacion Global
	int Cluster_global_min;
	int Cluster_global_max;
	float Cluster_global_tolerance;

	//Parametros de Filtrado estadistico
	int Filter_outliers_mean;
	float Filter_outliers_Thresh;

	//Parametros de Conteo de ejes
	float Wheels_tolerance;
	int Wheels_size_min;
	int Wheels_size_max;
	float Wheels_delta_ground;
	float Wheels_distance_filter;
	float Wheels_min_width;

public:
	Cluster():
	cloud_in(new pcl::PointCloud<pcl::PointXYZ>),
	cloud_out(new pcl::PointCloud<pcl::PointXYZ>),
	cloud_aux (new pcl::PointCloud<pcl::PointXYZ>),
	cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>),
	clusters_normals (new pcl::IndicesClusters){

	}
	~Cluster(){};

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_aux;

	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals;
	pcl::IndicesClustersPtr clusters_normals;

	std::vector<pcl::PointIndices> cluster_indices_Global;
	std::vector<pcl::PointIndices> cluster_indices_Local;

	std::map<std::string,pcl::PointCloud<pcl::PointXYZ> >::const_iterator itx;
	std::map<std::string,pcl::PointCloud<pcl::PointXYZ> > my_map;

	void set_Parameters_ALL(int Cluster_min, int Cluster_max, float Cluster_tolerance, int Filter_mean, float Filter_Thresh,
		float Wheel_tolerance, int Wheel_size_min, int Wheel_size_max, float Wheel_delta_ground, float Wheel_distance_filter, float Wheel_min_width);
	
    void set_Cluster_global_min(int Cluster_global_min) { this->Cluster_global_min = Cluster_global_min; }
    int get_Cluster_global_min() const { return Cluster_global_min; }

    void set_Cluster_global_max(int Cluster_global_max) { this->Cluster_global_max = Cluster_global_max; }
    int get_Cluster_global_max() const { return Cluster_global_max; }

    void set_Cluster_global_tolerance(float Cluster_global_tolerance) { this->Cluster_global_tolerance = Cluster_global_tolerance; }
    float get_Cluster_global_tolerance() const { return Cluster_global_tolerance; }

    void set_Filter_outliers_mean(int Filter_outliers_mean) { this->Filter_outliers_mean = Filter_outliers_mean; }
    int get_Filter_outliers_mean() const { return Filter_outliers_mean; }

    void set_Filter_outliers_Thresh(float Filter_outliers_Thresh) { this->Filter_outliers_Thresh = Filter_outliers_Thresh; }
    float get_Filter_outliers_Thresh() const { return Filter_outliers_Thresh; }

    void set_Wheels_tolerance(float Wheels_tolerance) { this->Wheels_tolerance = Wheels_tolerance; }
    float get_Wheels_tolerance() const { return Wheels_tolerance; }

    void set_Wheels_size_min(int Wheels_size_min) { this->Wheels_size_min = Wheels_size_min; }
    int get_Wheels_size_min() const { return Wheels_size_min; }

    void set_Wheels_size_max(int Wheels_size_max) { this->Wheels_size_max = Wheels_size_max; }
    int get_Wheels_size_max() const { return Wheels_size_max; }

    void set_Wheels_delta_ground(float Wheels_delta_ground) { this->Wheels_delta_ground = Wheels_delta_ground; }
    float get_Wheels_delta_ground() const { return Wheels_delta_ground; }

    void set_Wheels_distance_filter(float Wheels_distance_filter) { this->Wheels_distance_filter = Wheels_distance_filter; }
    float get_Wheels_distance_filter() const { return Wheels_distance_filter; }

    void set_Wheels_min_width(float Wheels_min_width) { this->Wheels_min_width = Wheels_min_width; }
    float get_Wheels_min_width() const { return Wheels_min_width; }

	void readCloud(std::string, std::string filename0);
	void passCloud(std::string, boost::python::numeric::array Cloud);

	void GetIndicesGlobal_B(std::string cloudIn);
	void GetIndicesGlobal(std::string cloudIn, int min, float tolerance, int max);
	void GetIndicesLocal(std::string cloudIn, int min, float tolerance, int max);
	std::vector<pcl::PointIndices> GetIndices(std::string cloudIn, int min, float tolerance, int max);

	void GetClusterGlobal(std::string cloudIn, std::string cloudOut, size_t it3);
	void GetClusterLocal(std::string cloudIn, std::string cloudOut, size_t it3);
	void GetCluster(std::string cloudIn, std::string cloudOut, size_t it3, std::vector<pcl::PointIndices> cluster_indices_1);

	void DemeanCluster(std::string, std::string);

	void filter_outliers_B(std::string cloudIn, std::string cloudOut);
	void filter_outliers(std::string, std::string, int Mean, float Thresh);

	void Angle_Rotate(std::string, std::string, float thetaX, float thetaZ );
	void saveCluster(std::string, std::string newName, int j);

	float CalculateHeightFirstEje(std::string cloudIn, float posicionEje, float despl);
	float CalculateHeight(std::string cloudIn);

	void CalculateMinMax(std::string cloudIn, boost::python::numeric::array VectorMin, boost::python::numeric::array VectorMax, 
		boost::python::numeric::array VectorCentroide);

	int CountWheels_B(std::string, boost::python::numeric::array, boost::python::numeric::array);
	int CountWheels(std::string, float, int, int, float, float, float, boost::python::numeric::array, boost::python::numeric::array);

	size_t getSizeClusterGlobal();
	size_t getSizeClusterLocal();

	void passtroughtFilter(std::string cloudIn,std::string cloudOut, std::string axe, float Mindistance, float Maxdistance );

	int getCloudSize(std::string cloudIn);
	void getClusterCloud(std::string cloudIn, boost::python::numeric::array);
	void clearMymap();


};
