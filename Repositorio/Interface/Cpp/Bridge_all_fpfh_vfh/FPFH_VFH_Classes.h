#include <pcl/range_image/range_image.h>
#include <boost/thread/thread.hpp>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
// #include <pcl/features/fpfh.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/vfh.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/histogram_visualizer.h>
#include <pcl/visualization/pcl_plotter.h>

#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"

class VFH_FPFH_C {
  private:
	float Density_delta;
	float Normals_radiusS;
	float USampling_radiusS;
	float FPFH_radiusS;
	float VFH_radiusS;

  public:
  	VFH_FPFH_C():
  	basic_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>),
  	cloud_normals2 (new pcl::PointCloud<pcl::Normal>),
  	cloudNNK(new pcl::PointCloud<pcl::PointXYZ>),
  	fpfhs (new pcl::PointCloud<pcl::FPFHSignature33> ()),
  	finale (new pcl::PointCloud<pcl::FPFHSignature33> ()),
  	vfhs(new pcl::PointCloud<pcl::VFHSignature308>),
  	cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>){
  		
  	}
  	~VFH_FPFH_C() {}
	pcl::PointCloud<pcl::PointXYZ>::Ptr basic_cloud_ptr;
	pcl::PointCloud<pcl::Normal>::Ptr cloud_normals2;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudNNK;
	pcl::PointCloud<pcl::FPFHSignature33>::Ptr fpfhs;
	pcl::PointCloud<pcl::FPFHSignature33>::Ptr finale;
	pcl::PointCloud<pcl::VFHSignature308>::Ptr vfhs;
	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
	pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals;

	std::vector<int> descriptors_num;

	void set_Parameters_ALL(float delta, float Normal_radiusS, float USampling_radius, float FPFH_radius, float VFH_radius);
	
    void set_Density_delta(float Density_delta) { this->Density_delta = Density_delta; }
    float get_Density_delta() const { return Density_delta; }

    void set_Normals_radiusS(float Normals_radiusS) { this->Normals_radiusS = Normals_radiusS; }
    float get_Normals_radiusS() const { return Normals_radiusS; }

    void set_USampling_radiusS(float USampling_radiusS) { this->USampling_radiusS = USampling_radiusS; }
    float get_USampling_radiusS() const { return USampling_radiusS; }

    void set_FPFH_radiusS(float FPFH_radiusS) { this->FPFH_radiusS = FPFH_radiusS; }
    float get_FPFH_radiusS() const { return FPFH_radiusS; }

    void set_VFH_radiusS(float VFH_radiusS) { this->VFH_radiusS = VFH_radiusS; }
    float get_VFH_radiusS() const { return VFH_radiusS; }

	void readCloud(std::string filename0);
	void passCloud(boost::python::numeric::array Cloud);
	float CalculatePointDensity(float delta);
	float CalculatePointDensity_B();
	void CalculateNormals(float radiusS, bool Sviewpoint);
	void CalculateNormals_V(float radiusS, bool Sviewpoint);
	void CalculateNormals_B();
	void UniformSampling (float radiusS);
	void UniformSampling_B();
	void CalculateFPFHwithoutSampling(float radiusS);
	void CalculateFPFHwithoutSampling_B();
	void CalculateFPFHSampling(float radiusS);
	void CalculateFPFHSampling_B();
	void Appendo(std::string);
	void saveFinal(std::string,char *path);
	void save(std::string);
	int putWidth();
	float getValueFPFH(size_t, size_t);
	void ClearValues();
	void normalsVis (std::string, int, double, bool);
	boost::shared_ptr<pcl::visualization::PCLVisualizer> InitViewer(std::string, int, double);
	void CloseVis(bool);

	void CalculateVFHwithoutSampling(float, bool, bool);
	void CalculateVFHSampling(float, bool, bool);
	void VFH_Save(std::string);
	float getValueVFH(size_t, size_t);
	void CalculateVFHwithoutSampling_B();
	void CalculateVFHSampling_B();

	int getCloudSize();
	void Save_point_normals(std::string);
	void Get_point_normals(boost::python::numeric::array);
	// ~VFH_FPFH_C();
};


