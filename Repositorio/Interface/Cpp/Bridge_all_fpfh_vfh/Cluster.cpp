#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/console/parse.h>
#include <pcl/common/common_headers.h>
#include <pcl/filters/statistical_outlier_removal.h>

#include <pcl/common/transforms.h>
#include <pcl/filters/passthrough.h>

#include <pcl/segmentation/conditional_euclidean_clustering.h>

#include "Cluster.h"

#include <vector> 
#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
using namespace boost::python;

void Cluster::set_Parameters_ALL(int Cluster_min, int Cluster_max, float Cluster_tolerance, int Filter_mean, float Filter_Thresh,
	float Wheel_tolerance, int Wheel_size_min, int Wheel_size_max, float Wheel_delta_ground, float Wheel_distance_filter, float Wheel_min_width)
{
	Cluster_global_min = Cluster_min;
	Cluster_global_max = Cluster_max;
	Cluster_global_tolerance = Cluster_tolerance;
	Filter_outliers_mean = Filter_mean;
	Filter_outliers_Thresh = Filter_Thresh;
	Wheels_tolerance = Wheel_tolerance;
	Wheels_size_min = Wheel_size_min;
	Wheels_size_max = Wheel_size_max;
	Wheels_delta_ground = Wheel_delta_ground;
	Wheels_distance_filter = Wheel_distance_filter;
	Wheels_min_width = Wheel_min_width;
}
void Cluster::readCloud(std::string cloudS, std::string filename0)
{
	cloud_in->clear();
	if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename0+".pcd", *cloud_in) == -1)
	{
		PCL_ERROR ("Couldn't read file the .pcd file \n");
	}

	itx = my_map.find(cloudS);
    if (itx == my_map.end()){
    	my_map[cloudS].clear();
    }
	my_map[cloudS] =  *cloud_in;
}

void Cluster::passCloud(std::string cloudS, boost::python::numeric::array Cloud)
{
	cloud_in->clear();
	cloud_in->width = len(Cloud);
	cloud_in->height = 1;
	cloud_in->is_dense = false;
	cloud_in->points.resize (cloud_in->width * cloud_in->height);

	for (int i = 0; i < len(Cloud); ++i)
	{
		cloud_in->points[i].x = extract<float>(Cloud[i][0]);
		cloud_in->points[i].y = extract<float>(Cloud[i][1]);
		cloud_in->points[i].z = extract<float>(Cloud[i][2]);
	}
		
	itx = my_map.find(cloudS);
    if (itx == my_map.end()){
    	my_map[cloudS].clear();
    }
	my_map[cloudS] =  *cloud_in;
}

void Cluster::GetIndicesGlobal_B(std::string cloudIn)
{
	cluster_indices_Global = GetIndices(cloudIn, Cluster_global_min, Cluster_global_tolerance, Cluster_global_max);
}
void Cluster::GetIndicesGlobal(std::string cloudIn, int min, float tolerance, int max)
{
	cluster_indices_Global = GetIndices(cloudIn, min, tolerance, max);
}

void Cluster::GetIndicesLocal(std::string cloudIn, int min, float tolerance, int max)
{
	cluster_indices_Local = GetIndices(cloudIn, min, tolerance, max);
}

std::vector<pcl::PointIndices> Cluster::GetIndices(std::string cloudIn, int min, float tolerance, int max)
{

	//pcl::copyPointCloud(point_cloudo,*point_cloud_ptr);
	//////
	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada  de entrada  \n");
    }

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
	std::vector<pcl::PointIndices> cluster_indices;

	tree->setInputCloud (cloud_in);

	pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
	ec.setClusterTolerance (tolerance);
	ec.setMinClusterSize (min);
	ec.setMaxClusterSize (max);
	ec.setSearchMethod (tree);
	ec.setInputCloud (cloud_in);
	cluster_indices.clear();
	ec.extract (cluster_indices);

	return cluster_indices;
}

void Cluster::GetClusterGlobal(std::string cloudIn, std::string cloudOut, size_t it3)
{
	Cluster::GetCluster(cloudIn, cloudOut, it3, cluster_indices_Global);
}

void Cluster::GetClusterLocal(std::string cloudIn, std::string cloudOut, size_t it3)
{
	Cluster::GetCluster(cloudIn, cloudOut, it3, cluster_indices_Local);
}

void Cluster::GetCluster(std::string cloudIn, std::string cloudOut, size_t it3, std::vector<pcl::PointIndices> cluster_indices_1)
{
	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

    std::vector<pcl::PointIndices>::const_iterator it;
	it = cluster_indices_1.begin ();

	cloud_out->clear();
	for (std::vector<int>::const_iterator pit = (it+it3)->indices.begin (); pit != (it+it3)->indices.end (); ++pit)
		cloud_out->points.push_back (cloud_in->points[*pit]); //*

	cloud_out->width = cloud_out->points.size ();
	cloud_out->height = 1;
	cloud_out->is_dense = true;

	// std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;

	itx = my_map.find(cloudOut);
    if (itx == my_map.end()){
    	my_map[cloudOut].clear();
    }

	my_map[cloudOut] =  *cloud_out;
}

void Cluster::DemeanCluster(std::string cloudIn, std::string cloudOut)
{
	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

	cloud_out->clear();

	Eigen::Vector4f centroide; 
	pcl::compute3DCentroid (*cloud_in, centroide);
	pcl::demeanPointCloud<pcl::PointXYZ> (*cloud_in, centroide, *cloud_out);

	itx = my_map.find(cloudOut);
    if (itx == my_map.end()){
    	my_map[cloudOut].clear();
    }
	my_map[cloudOut] =  *cloud_out;
}

void Cluster::filter_outliers_B(std::string cloudIn, std::string cloudOut)
{
	filter_outliers(cloudIn, cloudOut, Filter_outliers_mean, Filter_outliers_Thresh);
}
void Cluster::filter_outliers(std::string cloudIn, std::string cloudOut, int Mean, float Thresh)
{
	itx = my_map.find(cloudIn);
	if (itx != my_map.end()){
		cloud_in->clear();
		*cloud_in = my_map[cloudIn];
	}
	else{
		PCL_ERROR ("No existe la Nube de entrada   \n");
	}

	pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
	sor.setInputCloud (cloud_in);
	sor.setMeanK (Mean);
	sor.setStddevMulThresh (Thresh);
	sor.filter (*cloud_out);

	my_map[cloudOut] =  *cloud_out;
}

void Cluster::Angle_Rotate(std::string cloudIn, std::string cloudOut, float thetaX, float thetaZ )
{
	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

 	Eigen::Affine3f transform_2 = Eigen::Affine3f::Identity();
 	transform_2.translation() << 0.0, 0.0, 0.0;
 	transform_2.rotate (Eigen::AngleAxisf (thetaX, Eigen::Vector3f::UnitX()));
 	transform_2.rotate (Eigen::AngleAxisf (thetaZ, Eigen::Vector3f::UnitZ()));
 	pcl::transformPointCloud (*cloud_in, *cloud_out, transform_2);

	itx = my_map.find(cloudOut);
    if (itx == my_map.end()){
    	my_map[cloudOut].clear();
    }
 	my_map[cloudOut] =  *cloud_out;
}

void Cluster::saveCluster(std::string cloudIn, std::string newName,int j)
{

 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

 	pcl::PCDWriter writer;
	std::stringstream kk;
	kk << newName << j << ".pcd";
	writer.write<pcl::PointXYZ> (kk.str (), *cloud_in, false);
}

size_t Cluster::getSizeClusterGlobal()
{
	size_t it2 = 0;
	it2 = cluster_indices_Global.size();
	return it2;
}

size_t Cluster::getSizeClusterLocal()
{
	size_t it2 = 0;
	it2 = cluster_indices_Local.size();
	return it2;
}

void Cluster::passtroughtFilter(std::string cloudIn,std::string cloudOut, std::string axe, float Mindistance, float Maxdistance )
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

	pcl::PassThrough<pcl::PointXYZ> pass;
	pass.setInputCloud (cloud_in);
	pass.setFilterFieldName (axe);
	pass.setFilterLimits (Mindistance, Maxdistance);
	pass.setFilterLimitsNegative (false);
	pass.filter (*cloud_out);

	itx = my_map.find(cloudOut);
    if (itx == my_map.end()){
    	my_map[cloudOut].clear();
    }
	my_map[cloudOut] =  *cloud_out;
}


int Cluster::CountWheels_B(std::string cloudIn, boost::python::numeric::array FistEje_V, boost::python::numeric::array LastEje_V)
{
	int contador_aux =0;
	contador_aux = CountWheels(cloudIn, Wheels_tolerance, Wheels_size_min, Wheels_size_max, Wheels_delta_ground, Wheels_distance_filter, 
		Wheels_min_width, FistEje_V, LastEje_V);
	return contador_aux;
}
int Cluster::CountWheels(std::string cloudIn, float tol, int Minu, int Maxu, float porcu, float distance, float minWheel, boost::python::numeric::array FistEje_V,
	boost::python::numeric::array LastEje_V)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

    std::string cloudOut = "cloud_seg_down";
    std::string axe = "y";
	int contador = 0;
	float FirstEjeRef = 0;
	float FirstEjeMax = 0;
	float LastEjeRef = 0;
	float LastEjeMax = 0;
	pcl::PointXYZ minPt, maxPt;
	pcl::getMinMax3D(*cloud_in, minPt, maxPt);

	passtroughtFilter(cloudIn, cloudOut, axe,maxPt.y-distance, maxPt.y);
	GetIndicesLocal(cloudOut, Minu, tol, Maxu);

	std::string cloudIn_2 = "cloud_seg_down";
	std::string cloudOut_2 = "cloud_wheels";

	size_t itf = getSizeClusterLocal();
	for (size_t it4 = 0; it4 != itf; ++it4)
	{
		GetClusterLocal(cloudIn_2, cloudOut_2, it4);

		pcl::PointXYZ minPt2, maxPt2;
		pcl::getMinMax3D(my_map[cloudOut_2], minPt2, maxPt2); 

		if (maxPt2.y > (maxPt.y-porcu))
		{
			if (((maxPt2.y-minPt2.y)>0.7*distance) && ((maxPt2.z-minPt2.z)>minWheel))
			{
				contador = contador +1;
				if (maxPt2.z>FirstEjeMax)
				{
					FirstEjeRef = minPt2.z;
					FirstEjeMax = maxPt2.z;
				}
				if (maxPt2.z<LastEjeMax)
				{
					LastEjeRef = minPt2.z;
					LastEjeMax = maxPt2.z;
				}
			}
		}
	}

	float Vector_first[2]={FirstEjeRef, FirstEjeMax};
	float Vector_last[2]={LastEjeRef, LastEjeMax};
	for (int i = 0; i < 2; ++i)
	{
		FistEje_V[i] = Vector_first[i];
		LastEje_V[i] = Vector_last[i];
	}
	return contador;
}

float Cluster::CalculateHeightFirstEje(std::string cloudIn, float posicionEje, float despl)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

	float altura = 0.0;
	std::string cloudOut = "cloud_seg_left";
	std::string axe = "z";

	passtroughtFilter(cloudIn, cloudOut, axe, posicionEje-despl, posicionEje);
	altura = CalculateHeight(cloudOut);

	return altura;
}

float Cluster::CalculateHeight(std::string cloudIn)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

	float altura = 0.0;
	pcl::PointXYZ minPt3, maxPt3;
	pcl::getMinMax3D(*cloud_in, minPt3, maxPt3); 

	altura = maxPt3.y - minPt3.y;
	return altura;
}

void Cluster::CalculateMinMax(std::string cloudIn, boost::python::numeric::array VectorMin, boost::python::numeric::array VectorMax, boost::python::numeric::array VectorCentroide)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }
    
	pcl::PointXYZ minPt3, maxPt3;
	pcl::getMinMax3D(*cloud_in, minPt3, maxPt3);

	float Vector_min[3]={minPt3.x, minPt3.y, minPt3.z};
	float Vector_max[3]={maxPt3.x, maxPt3.y, maxPt3.z};
	for (int i = 0; i < 3; ++i)
	{
		VectorMin[i] = Vector_min[i];
		VectorMax[i] = Vector_max[i];
	}

	Eigen::Vector4f centroide; 
	pcl::compute3DCentroid (*cloud_in, centroide);
	float Vector_centroide[3]={centroide(0), centroide(1), centroide(2)};
	for (int i = 0; i < 3; ++i)
	{
		VectorCentroide[i] = Vector_centroide[i];
	}
}

int Cluster::getCloudSize(std::string cloudIn)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

    int size_c = cloud_in->points.size ();
    return size_c;

}

void Cluster::getClusterCloud(std::string cloudIn, boost::python::numeric::array Cloud)
{
 	itx = my_map.find(cloudIn);
    if (itx != my_map.end()){
    	cloud_in->clear();
		*cloud_in = my_map[cloudIn];
    }
    else{
    	PCL_ERROR ("No existe la Nube de entrada   \n");
    }

	for (int i = 0; i < cloud_in->points.size (); ++i)
	{
		Cloud[i][0] = cloud_in->points[i].x;
		Cloud[i][1] = cloud_in->points[i].y;
		Cloud[i][2] = cloud_in->points[i].z;
	}
}

void Cluster::clearMymap()
{
	my_map.clear();
}