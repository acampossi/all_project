#include "FPFH_VFH_Classes.h"

#include <iostream>

#include <pcl/io/pcd_io.h>
#include <pcl/range_image/range_image.h>
#include <pcl/common/common_headers.h>
#include <pcl/common/transforms.h>
#include <boost/thread/thread.hpp>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>

#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/point_types.h>
// #include <pcl/features/fpfh.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/vfh.h>

#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/filter.h>
#include <pcl/keypoints/uniform_sampling.h>

#include <vtkRenderWindow.h>

#include <boost/python.hpp>
#include "boost/python/extract.hpp"
#include "boost/python/numeric.hpp"
using namespace boost::python;

// VFH_FPFH_C::~VFH_FPFH_C(){
// 	std::cout<<"called destructor with loc:"<< std::endl;
// 	// delete &viewer;
// }

void VFH_FPFH_C::set_Parameters_ALL(float delta, float Normal_radiusS, float USampling_radius, float FPFH_radius, float VFH_radius)
{
	Density_delta = delta;
	Normals_radiusS = Normal_radiusS;
	USampling_radiusS = USampling_radius;
	FPFH_radiusS = FPFH_radius;
	VFH_radiusS = VFH_radius;
}

void VFH_FPFH_C::ClearValues(){
	finale->clear();
	descriptors_num.clear();
	cloud_normals2->clear();
	basic_cloud_ptr->clear();

}
void VFH_FPFH_C::readCloud(std::string filename0){
	basic_cloud_ptr->clear();
	if (pcl::io::loadPCDFile<pcl::PointXYZ> (filename0+".pcd", *basic_cloud_ptr) == -1)
	{
		PCL_ERROR ("Couldn't read file the .pcd file \n");
	}
}

void VFH_FPFH_C::passCloud(boost::python::numeric::array Cloud){
	basic_cloud_ptr->clear();
	basic_cloud_ptr->width = len(Cloud);
	basic_cloud_ptr->height = 1;
	basic_cloud_ptr->is_dense = false;
	basic_cloud_ptr->points.resize (basic_cloud_ptr->width * basic_cloud_ptr->height);

	for (int i = 0; i < len(Cloud); ++i)
	{
		basic_cloud_ptr->points[i].x = extract<float>(Cloud[i][0]);
		basic_cloud_ptr->points[i].y = extract<float>(Cloud[i][1]);
		basic_cloud_ptr->points[i].z = extract<float>(Cloud[i][2]);
	}
}

float VFH_FPFH_C::CalculatePointDensity_B()
{
	float delta_aux = 0.0;
	delta_aux = CalculatePointDensity(Density_delta);
}

float VFH_FPFH_C::CalculatePointDensity(float delta)
{
	/*"""
	Calcula la densidad de la nube de puntos y entrega un radio de busqueda segun el valor de delta
	Nota: Esto lo realiza buscando la distancia entre el punto Pq y su vecino mas cercano por cada Pq que pertenece a la Nube
		  y dividiendo esta distancia sobre el total de puntos en la nube


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	delta = radio: delta*resolucion
	"""*/

	double res = 0.0;
	int n_points = 0;
	int nres;
	std::vector<int> indices1 (2);
	std::vector<float> sqr_distances (2);
	pcl::search::KdTree<pcl::PointXYZ> tree1;
	tree1.setInputCloud (basic_cloud_ptr);

	// --- Realiza un barrido por toda la nubede puntos
	for (size_t i = 0; i < basic_cloud_ptr->size (); ++i)
	{
		if (! pcl_isfinite ((*basic_cloud_ptr)[i].x))
		{
			continue;
		}
		//Considering the second neighbor since the first is the point itself.
		nres = tree1.nearestKSearch (i, 2, indices1, sqr_distances);
		if (nres == 2)
		{
			res += sqrt (sqr_distances[1]);
			++n_points;
		}
	}
	if (n_points != 0)
	{
		res /= n_points;
	}
	float model_ss_;
	float resolution = static_cast<float> (res);
	if (resolution != 0.0f)
	{
		model_ss_=resolution*delta;
	//model_ss_   *= resolution;
	}

	// cout << "resolution "<<resolution<<".\n";
	// cout << "model_ss_ "<<model_ss_<<".\n";
	return model_ss_;
}

void VFH_FPFH_C::CalculateNormals_B()
{
	std::cout<<Normals_radiusS<<"NR"<<std::endl;
	CalculateNormals(Normals_radiusS, false);
	std::cout<<basic_cloud_ptr->points.size ()<< "Size"<<std::endl;
}

void VFH_FPFH_C::CalculateNormals(float radiusS, bool Sviewpoint)
{
	/*"""
	Calcula las normales de la nube de puntos


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	cloud_normals2 = Normales de la nube
	radiusS = radio de busqueda
	Sviewpoint = true:Usar punto de vista (-5, CentroY, CentroZ)
	"""*/
	
	pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;
	ne.setInputCloud (basic_cloud_ptr);
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
	ne.setSearchMethod (tree);
	// cout << "radio busqueda "<<radiusS<<".\n";
	ne.setRadiusSearch (radiusS);
	

	if (Sviewpoint)
	{
		Eigen::Vector4f centroide2x; 
		pcl::compute3DCentroid (*basic_cloud_ptr, centroide2x);
		ne.setViewPoint (-5,centroide2x(1),centroide2x(2));
	}
	std::cout<<basic_cloud_ptr->points.size () << "SizeC"<<std::endl;
	cloud_normals2->clear();
	ne.compute (*cloud_normals2);
	std::vector<int> indicesz;
	
	// -- remueve las normales Nan de la nube de puntos
	pcl::removeNaNNormalsFromPointCloud(*cloud_normals2,*cloud_normals2,indicesz);
	pcl::copyPointCloud(*basic_cloud_ptr,indicesz,*basic_cloud_ptr);
	
	// tree->~KdTree<pcl::PointXYZ> ();
	// ne.~NormalEstimationOMP();
}

void VFH_FPFH_C::CalculateNormals_V(float radiusS, bool Sviewpoint)
{
	/*"""
	Calcula las normales de la nube de puntos


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	cloud_normals2 = Normales de la nube
	radiusS = radio de busqueda
	Sviewpoint = true:Usar punto de vista (-5, CentroY, CentroZ)
	"""*/
	
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
	ne.setInputCloud (basic_cloud_ptr);
	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());	
	ne.setSearchMethod (tree);	
	// cout << "radio busqueda "<<radiusS<<".\n";
	ne.setRadiusSearch (radiusS);
	

	if (Sviewpoint)
	{
		Eigen::Vector4f centroide2x; 	
		pcl::compute3DCentroid (*basic_cloud_ptr, centroide2x);	
		ne.setViewPoint (-5,centroide2x(1),centroide2x(2));
	
	}
	std::cout<<basic_cloud_ptr->points.size () << "SizeDD"<<std::endl;
	cloud_normals2->clear();
	ne.compute (*cloud_normals2);	
	std::vector<int> indicesz;
	
	// -- remueve las normales Nan de la nube de puntos
	pcl::removeNaNNormalsFromPointCloud(*cloud_normals2,*cloud_normals2,indicesz);	
	pcl::copyPointCloud(*basic_cloud_ptr,indicesz,*basic_cloud_ptr);
	
	// tree->~KdTree<pcl::PointXYZ> ();
	// ne.~NormalEstimationOMP();
}

void VFH_FPFH_C::UniformSampling_B()
{
	UniformSampling(USampling_radiusS);
}

void VFH_FPFH_C::UniformSampling (float radiusS)
{
	/*"""
	Calcula Los punos de interes a traves de un muestreo uniforme


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	radiusS = radio del voxel
	cloudNNK = nube de puntos final despues del muestreo
	"""*/

	pcl::PointCloud<int> sampled_indices;
	pcl::UniformSampling<pcl::PointXYZ> uniform_sampling;
	uniform_sampling.setInputCloud (basic_cloud_ptr);
	uniform_sampling.setRadiusSearch (radiusS);
	uniform_sampling.compute (sampled_indices);
	pcl::copyPointCloud (*basic_cloud_ptr, sampled_indices.points, *cloudNNK);
}

void VFH_FPFH_C::CalculateFPFHwithoutSampling_B()
{
	CalculateFPFHwithoutSampling(FPFH_radiusS);
}

void VFH_FPFH_C::CalculateFPFHwithoutSampling(float radiusS)
{
	/*"""
	Calcula los descriptores FPFH sin utilizar keypoints


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	cloud_normals2 = Normales de la nube
	fpfh = matriz con descriptores FPFH
	radiusS = radio de busqueda
	"""*/

	// pcl::FPFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
	pcl::FPFHEstimationOMP<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
	fpfh.setInputCloud (basic_cloud_ptr);
	fpfh.setInputNormals (cloud_normals2);

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree5 (new pcl::search::KdTree<pcl::PointXYZ> ());
	fpfh.setSearchMethod (tree5);


	fpfh.setRadiusSearch (radiusS);

	fpfh.compute (*fpfhs);
}

void VFH_FPFH_C::CalculateFPFHSampling_B()
{
	CalculateFPFHSampling(FPFH_radiusS);
}

void VFH_FPFH_C::CalculateFPFHSampling(float radiusS)
{
	/*"""
	Calcula los descriptores FPFH Utilizando keypoints


	Parametros:

	cloudNNK = nube de puntos de entrada
	basic_cloud_ptr = superficie de busqueda original
	cloud_normals2 = Normales de la nube
	fpfh = matriz con descriptores FPFH
	radiusS = radio de busqueda
	"""*/

	pcl::FPFHEstimationOMP<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
	fpfh.setInputCloud (cloudNNK);
	fpfh.setInputNormals (cloud_normals2);
	fpfh.setSearchSurface(basic_cloud_ptr);

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree5 (new pcl::search::KdTree<pcl::PointXYZ> ());
	fpfh.setSearchMethod (tree5);


	fpfh.setRadiusSearch (radiusS);

	fpfh.compute (*fpfhs);
}


void VFH_FPFH_C::Appendo(std::string nombrearchivo)
{
	pcl::PCDWriter writer;
	writer.write<pcl::FPFHSignature33>(nombrearchivo+"_FPFH.pcd", *fpfhs, false);

	// std::cout << "Finalizado el proceso para" << nombrearchivo << std::endl;
	// std::cout << std::endl;

	descriptors_num.push_back(fpfhs->width);

	for (int i = 0; i < fpfhs->width; ++i)
	{
		finale->push_back(fpfhs->points[i]);
	}
}

void VFH_FPFH_C::saveFinal(std::string nombrearchivo, char *path)
{
	pcl::PCDWriter writer4;
	writer4.write<pcl::FPFHSignature33>(nombrearchivo+"/FPFH.pcd", *finale, false);

	std::ofstream output_file(path);
	std::ostream_iterator<int> output_iterator(output_file, "\n");
	std::copy(descriptors_num.begin(), descriptors_num.end(), output_iterator);
	output_file.close();
}

int VFH_FPFH_C::putWidth(void)
{
	int tmp = fpfhs->width;
	descriptors_num.push_back(fpfhs->width);
	return tmp;
}

float VFH_FPFH_C::getValueFPFH(size_t i, size_t j){
	float valuetmp = fpfhs->points[i].histogram[j];
	return valuetmp;

}

void VFH_FPFH_C::save(std::string nombrearchivo)
{
	pcl::PCDWriter writer;
	writer.write<pcl::FPFHSignature33>(nombrearchivo+"_FPFH.pcd", *fpfhs, false);
}

void VFH_FPFH_C::normalsVis (std::string nombre, int level, double scale, bool InitView)
{
	/*"""
	Muestra las normales en un visualzador


	Parametros:

	cloud = nube de punto original
	normals = normales de la nube de puntos
	"""*/

  // --------------------------------------------------------
  // -----Open 3D viewer and add point cloud and normals-----
  // --------------------------------------------------------

  if (InitView)
  {
  	viewer = InitViewer(nombre, level, scale);
  }

  else
  {
  	viewer->resetStoppedFlag();
	viewer->removeAllPointClouds();
	viewer->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (basic_cloud_ptr, cloud_normals2, level, scale, "normals");

  }
  	
	while (!viewer->wasStopped ())
	{
		viewer->spinOnce (100);

	}
	// viewer->getRenderWindow()->Delete();

}

boost::shared_ptr<pcl::visualization::PCLVisualizer> VFH_FPFH_C::InitViewer(std::string nombre, int level, double scale)
{
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer2 (new pcl::visualization::PCLVisualizer (nombre));
  viewer2->setBackgroundColor (0, 0, 0);

  //viewer2->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
  viewer2->addPointCloudNormals<pcl::PointXYZ, pcl::Normal> (basic_cloud_ptr, cloud_normals2, level, scale, "normals");
  viewer2->addCoordinateSystem (1.0);
  viewer2->initCameraParameters ();

	Eigen::Affine3f viewer2_pose (Eigen::Affine3f::Identity ());
	Eigen::Quaternion<float> rotateLaser;
	Eigen::Vector4f centroide2x; 
	pcl::compute3DCentroid (*basic_cloud_ptr, centroide2x);

	// rotateLaser = Eigen::Quaternion<float>(-0.1379,-0.6935,0.1379,-0.6935);
	// rotateLaser = Eigen::Quaternion<float>(-0.1379,-0.6935,0.1379,-0.6935);

	rotateLaser = Eigen::Quaternion<float>(0.707,0,0.707,0);
	viewer2_pose = Eigen::Affine3f (Eigen::Translation3f (-3.5, 0,0)) * Eigen::Affine3f (rotateLaser);



	Eigen::Vector3f pos_vector = viewer2_pose * Eigen::Vector3f (0, 0, 0);
	Eigen::Vector3f look_at_vector = viewer2_pose.rotation () * Eigen::Vector3f (0, 0, 1) + pos_vector;
	Eigen::Vector3f up_vector = viewer2_pose.rotation () * Eigen::Vector3f (0, -1, 0);
  viewer2->setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
                            look_at_vector[0], look_at_vector[1], look_at_vector[2],
                            up_vector[0], up_vector[1], up_vector[2]);
  
	
  return viewer2;
}

void VFH_FPFH_C::CloseVis(bool InitView)
{

  	// viewer->close();
  	  std::cout << vtkVersion::GetVTKSourceVersion() << std::endl;
  std::cout << vtkVersion::GetVTKMajorVersion() << std::endl;
  std::cout << vtkVersion::GetVTKMinorVersion() << std::endl;
  	// viewer->getRenderWindow()->Finalize();
  // boost::shared_ptr<pcl::visualization::PCLVisualizer>::swap(viewer->getRenderWindow());
  viewer->setSize(1,1);
  

 // viewer->~PCLVisualizer(); 


}

void VFH_FPFH_C::CalculateVFHwithoutSampling_B()
{
	CalculateVFHwithoutSampling(VFH_radiusS, true, true);
}

void VFH_FPFH_C::CalculateVFHwithoutSampling(float radiusS, bool centroide_v, bool normalize_b)
{
	/*"""
	Calcula los descriptores FPFH sin utilizar keypoints


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	cloud_normals2 = Normales de la nube
	fpfh = matriz con descriptores FPFH
	radiusS = radio de busqueda
	"""*/

	// pcl::FPFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
	pcl::VFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::VFHSignature308> vfh;
	vfh.setInputCloud(basic_cloud_ptr);
	vfh.setInputNormals(cloud_normals2);

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree5 (new pcl::search::KdTree<pcl::PointXYZ> ());
	vfh.setSearchMethod(tree5);

	if (centroide_v)
	{
		Eigen::Vector4f centroide2x; 
		pcl::compute3DCentroid (*basic_cloud_ptr, centroide2x);
		vfh.setViewPoint (-5,0,0);
	}
	// Optionally, we can normalize the bins of the resulting histogram,
	// using the total number of points.
	vfh.setNormalizeBins(normalize_b);
	// Also, we can normalize the SDC with the maximum size found between
	// the centroid and any of the cluster's points.
	vfh.setNormalizeDistance(false);

	vfh.compute(*vfhs);
}

void VFH_FPFH_C::CalculateVFHSampling_B()
{
	CalculateVFHSampling(VFH_radiusS, true, true);
}

void VFH_FPFH_C::CalculateVFHSampling(float radiusS, bool centroide_v, bool normalize_b)
{
	/*"""
	Calcula los descriptores FPFH sin utilizar keypoints


	Parametros:

	basic_cloud_ptr = Nube de puntos de entrada
	cloud_normals2 = Normales de la nube
	fpfh = matriz con descriptores FPFH
	radiusS = radio de busqueda
	"""*/

	// pcl::FPFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::FPFHSignature33> fpfh;
	pcl::VFHEstimation<pcl::PointXYZ, pcl::Normal, pcl::VFHSignature308> vfh;
	vfh.setInputCloud(cloudNNK);
	vfh.setInputNormals(cloud_normals2);
	vfh.setSearchSurface(basic_cloud_ptr);

	pcl::search::KdTree<pcl::PointXYZ>::Ptr tree5 (new pcl::search::KdTree<pcl::PointXYZ> ());
	vfh.setSearchMethod(tree5);

	if (centroide_v)
	{
		Eigen::Vector4f centroide2x; 
		pcl::compute3DCentroid (*basic_cloud_ptr, centroide2x);
		vfh.setViewPoint (-5,0,0);
	}
	// Optionally, we can normalize the bins of the resulting histogram,
	// using the total number of points.
	vfh.setNormalizeBins(normalize_b);
	// Also, we can normalize the SDC with the maximum size found between
	// the centroid and any of the cluster's points.
	vfh.setNormalizeDistance(false);

	vfh.compute(*vfhs);
}

void VFH_FPFH_C::VFH_Save(std::string nombrearchivo)
{
	pcl::PCDWriter writer;
	writer.write<pcl::VFHSignature308>(nombrearchivo+"_VFH.pcd", *vfhs, false);
}


float VFH_FPFH_C::getValueVFH(size_t i, size_t j){
	float valuetmp = vfhs->points[i].histogram[j];
	return valuetmp;

}

void VFH_FPFH_C::Save_point_normals(std::string nombrearchivo)
{
	pcl::copyPointCloud(*basic_cloud_ptr, *cloud_with_normals);
	pcl::copyPointCloud(*cloud_normals2, *cloud_with_normals);

	// pcl::io::savePCDFileASCII("test.pcd", *cloud_with_normals);
	pcl::PCDWriter writer;
	writer.write<pcl::PointNormal>(nombrearchivo+"_withNormals.pcd", *cloud_with_normals, false); 
}

int VFH_FPFH_C::getCloudSize()
{

    int size_c = basic_cloud_ptr->points.size ();
    return size_c;

}

void VFH_FPFH_C::Get_point_normals(boost::python::numeric::array Cloud)
{
	cloud_with_normals->clear();
	pcl::copyPointCloud(*basic_cloud_ptr, *cloud_with_normals);
	pcl::copyPointCloud(*cloud_normals2, *cloud_with_normals);

	for (int i = 0; i < cloud_with_normals->points.size (); ++i)
	{
		Cloud[i][0] = cloud_with_normals->points[i].x;
		Cloud[i][1] = cloud_with_normals->points[i].y;
		Cloud[i][2] = cloud_with_normals->points[i].z;
		Cloud[i][3] = cloud_with_normals->points[i].normal_x;
		Cloud[i][4] = cloud_with_normals->points[i].normal_y;
		Cloud[i][5] = cloud_with_normals->points[i].normal_z;
	}
}