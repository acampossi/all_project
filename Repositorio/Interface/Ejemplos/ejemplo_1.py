import numpy as np
from PyQt4 import QtGui, QtCore  # can also use pyside
from functools import partial

from vispy.app import Timer
from vispy.scene.visuals import Text
from vispy.scene.widgets import ViewBox
from vispy.scene import SceneCanvas
import vispy.scene
from vispy.scene import visuals

import time

def on_resize(canvas, vb, event):
    vb.pos = 1, 1
    vb.size = (canvas.size[0] - 2, canvas.size[1] - 2)


class Window(QtGui.QWidget):
    def __init__(self):
        super(Window, self).__init__()


        box = QtGui.QBoxLayout(QtGui.QBoxLayout.LeftToRight, self)
        self.resize(500, 200)
        self.setLayout(box)

        self.canvas_0 = SceneCanvas(bgcolor='w')
        self.canvas_0 = SceneCanvas(keys='interactive', show=True,size=(100, 100))
        view = self.canvas_0.central_widget.add_view()
        #pos= np.loadtxt('15_12_46_filtered.pcd',skiprows=11)
        pos= np.loadtxt('/home/ssi_ralvarez/Documentos/all_project/RepositoriosExternos/vispy/examples/basics/scene/Clusters/data15_55_48/data15_55_48_02_cluster_filtered0.pcd',skiprows=11)
        scatter = visuals.Markers()
        scatter.set_data(pos, edge_color=(1, 1, 1, 1), face_color=(1, 1, 1, .5), size=2)
        view.add(scatter)
        view.camera = 'arcball' #'turntable'  # or try 
#        self.update()
# add a colored 3D axis for orientation
        axis = visuals.XYZAxis(parent=view.scene)
        
        box.addWidget(self.canvas_0.native)

        l_nbr_steps = QtGui.QLabel("Nbr Steps ")
        self.nbr_steps = QtGui.QSpinBox()
        self.nbr_steps.setMinimum(3)
        self.nbr_steps.setMaximum(100)
        self.nbr_steps.setValue(6)
        self.nbr_steps.valueChanged.connect(self.update_param)


        self.show()

    def update_param(self, option):
        self.signal_objet_changed.emit()



#       scatter.set_data(pos[:,[1,0,2]], edge_color=(1, 1, 1, 1), face_color=(1, 1, 1, .5), size=2)
#        self.update()
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()
        elif event.key() == QtCore.Qt.Key_F11:
            self.showNormal() if self.isFullScreen() else self.showFullScreen()

if __name__ == '__main__':
    qt_app = QtGui.QApplication([])
    ex = Window()
    qt_app.exec_()