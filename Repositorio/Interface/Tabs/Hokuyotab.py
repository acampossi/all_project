#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
HOKUYO_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, HOKUYO_BASEDIR+u'/../VisualizationWidgets')

from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets  import PlotDataWidget,MyButton,TextAndSpinBox,ResetButton,MyQComboBox,MyQSpinBox,MyQCheckBox,HelpButton,HelpWindow,ConfigurationWidget
#from VisualizationWidgets import *
from Constants import *
from Utils import polar2rec

IZQ_KEY = 0
IZQ_COLOR = (1,0,0,1)

DER_KEY = 1
DER_COLOR = (0,0,1,1)

class HokuyoController:
    """
    Clase que interconecta el modelo del sistema con la interfaz grafica para el control de las sensor Laser( Hokuyo)
    Atributos:
            DevicesModel        : Conexion con el modelo del sistema 
            Hokuyotab           : Conexiones con las pesta;as de configuracion de las camaras de la interfaz grafica
            lastdevice          : El ultimo dispositivo escogido por el ususario
            timer               : temporizador que generara una interrupcion cada cierto intervalo cuando la  pesa;a este siendo visualizada
    """
    DevicesModel = None
    Hokuyotab = None
    lastdevice = None
    timer = None
    UpdatingParameters =False
    cnt = 0
    def __init__(self, Hokuyotab, DevicesModel,timerinterval ='auto'):
        # se guarda el modelo y la vista en los atributos de las clases
        self.Hokuyotab = Hokuyotab

        self.DevicesModel = DevicesModel
        # Se definen los rangos de los spinbox
        self.SetSpinboxsRanges()

        # se actualizan los puertos existentes
        self.update_device_list()
        self.update_device()
        # Evento cuando se elije un dispositivo diferente
        self.Hokuyotab.menuWidget.puertos.currentIndexChanged.connect(self.update_device)
        # evento cuando las configuraciones cambian
        self.Hokuyotab.menuWidget.signal_objet_changed.connect(self.update_param)
        # se  crea un reloj que permitira actualizar la adquisicion de datos de os sensores emulando una interupcion y se guarda el intervalo predefinido
        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        self.Hokuyotab.menuWidget.resetButton.clicked.connect(self.reset)

        self.Hokuyotab.menuWidget.helpButton.clicked.connect(self.help)
        self.helpWindow = HelpWindow(HOKUYO_BASEDIR+u'/../VisualizationWidgets/help_hokuyotab.png',parent=self)
        
        self.polar2rec =polar2rec(-45)


    def update_device(self):
        """Evento que se ejecuta cuando se selecciona otro dispositivo en la interfaz grafica. En caso de que este cerrado se 
        abre la conexion y se actualiza los parametros presentes en la interfaz y se cierra la conexion con el ultimo sensor 
        conectado"""
        dev_port = self.getcurrentDevice()
        if dev_port !='':
            if dev_port in self.DevicesModel.Hokuyo_Dic:
                self.SetSpinboxsRanges()
            
            elif dev_port in self.DevicesModel.Laser_Dic:
                self.SetSpinboxsRanges(0,180,180,360)
            
            self.DevicesModel.OpenLaser(dev_port)
            self.update_menuwidget()
        
        if self.lastdevice != None and self.lastdevice!=dev_port:
            if dev_port in self.DevicesModel.Hokuyo_Dic:
                self.DevicesModel.Hokuyo_Dic[self.lastdevice].LaserDispose()
            

    def update_device_list(self):
        List = []
        for key, value in self.DevicesModel.Hokuyo_Dic.iteritems():
            if value.Plugged:
                List.append(key)

        for key, value in self.DevicesModel.Laser_Dic.iteritems():
            if value.Plugged:
                List.append(key)

        self.Hokuyotab.setSesorsList(List)          
        self.update_menuwidget()


    def update_param(self):
            
        if not self.UpdatingParameters: 
            dev = self.getcurrentDevice()
            Update = False
            if dev !='':
                if dev in self.DevicesModel.Hokuyo_Dic:
                    device =self.DevicesModel.Hokuyo_Dic[dev]
                    
                    Update = True
                elif dev in self.DevicesModel.Laser_Dic:
                    device =self.DevicesModel.Laser_Dic[dev]
                    Update = True
                if Update:
                    device.i_enable = self.Hokuyotab.menuWidget.izquiero.isChecked()
                    device.d_enable = self.Hokuyotab.menuWidget.derecho.isChecked()
                    device.i_range_min = self.Hokuyotab.menuWidget.rang_min_iz.value()
                    device.i_range_max = self.Hokuyotab.menuWidget.rang_max_iz .value()
                    device.d_range_min = self.Hokuyotab.menuWidget.rang_min_der.value()
                    device.d_range_max = self.Hokuyotab.menuWidget.rang_max_der.value()
                    device.angle_a0 = self.Hokuyotab.menuWidget.angulo_az_0.value()

                
            else:
                self.DevicesModel.DetectHokuyoDevices() 

    def update_menuwidget(self):
        dev = self.getcurrentDevice()
        self.UpdatingParameters =True
        Update = False
        if dev !='':
            if dev in self.DevicesModel.Hokuyo_Dic:
                device =self.DevicesModel.Hokuyo_Dic[dev]
                Update = True
            elif dev in self.DevicesModel.Laser_Dic:
                device =self.DevicesModel.Laser_Dic[dev]
                Update = True

            if Update:
                #se define el maximo y minimo para el lado izquierdo 
                self.Hokuyotab.menuWidget.rang_min_iz.setValue(device.i_range_min)            
                self.Hokuyotab.menuWidget.rang_max_iz.setValue(device.i_range_max)            
                self.Hokuyotab.menuWidget.rang_min_der.setValue(device.d_range_min)            
                self.Hokuyotab.menuWidget.rang_max_der.setValue(device.d_range_max)            
                self.Hokuyotab.menuWidget.angulo_az_0.setValue(device.angle_a0)            
                
                self.Hokuyotab.menuWidget.izquiero.setChecked(device.i_enable)
                self.Hokuyotab.menuWidget.derecho.setChecked(device.d_enable)
            
        self.UpdatingParameters =False

    def timerevent(self,event):
        dev_port = self.getcurrentDevice()
        #print dev_port
        if dev_port !='':
            if dev_port in self.DevicesModel.Hokuyo_Dic:
                device =self.DevicesModel.Hokuyo_Dic[dev_port]
                success = True
                if device.laser.is_open():
                    try:
                        data = device.getRangeScanAndAppend(0,0)
                    except:
                        success = False 
                    if success:
                        X,Y = self.polar2rec.pol2rec(data,device.angle_a0)
                        if (self.Hokuyotab.menuWidget.izquiero.checkState()):
                            self.Hokuyotab.plotCanvas.CreateMarkers(IZQ_KEY,X[device.i_range_min:device.i_range_max],Y[device.i_range_min:device.i_range_max],IZQ_COLOR)
                        else:
                            self.Hokuyotab.plotCanvas.CreateMarkers(IZQ_KEY,np.zeros(2),np.zeros(2),IZQ_COLOR)
                        
                        if (self.Hokuyotab.menuWidget.derecho.checkState()):
                            self.Hokuyotab.plotCanvas.CreateMarkers(DER_KEY,X[device.d_range_min:device.d_range_max],Y[device.d_range_min:device.d_range_max],DER_COLOR)
                        else:
                            self.Hokuyotab.plotCanvas.CreateMarkers(DER_KEY,np.zeros(2),np.zeros(2),DER_COLOR)
                else:
                    self.update_device()
            if dev_port in self.DevicesModel.Laser_Dic:
                device =self.DevicesModel.Laser_Dic[dev_port]
                success = True
                if device.is_open():
                    #try:
                    data = device.read_scans()
                    if data == []:
                        success = False
                        self.cnt +=1
                        if self.cnt >=25:
                            self.cnt = 0
                            device.reset()
                            device.start_motor()
                            device.init_read_scans()
                        
                    data = np.array(data)

                    #except:
                    #    success = False 
                    if success:
                        if data != []:
                            X,Y = polar2rec.Pol2Rec(data[:,1],data[:,2],xaxisAngle=device.angle_a0)
                            
                            iz_idx =  np.logical_and(data[:,1]>=device.i_range_min,data[:,1]<=device.i_range_max)
                            de_idx =  np.logical_and(data[:,1]>=device.d_range_min,data[:,1]<=device.d_range_max)

                            if (self.Hokuyotab.menuWidget.izquiero.checkState()):
                                self.Hokuyotab.plotCanvas.CreateMarkers(IZQ_KEY,X[iz_idx],Y[iz_idx],IZQ_COLOR)
                            else:
                                self.Hokuyotab.plotCanvas.CreateMarkers(IZQ_KEY,np.zeros(2),np.zeros(2),IZQ_COLOR)

                            if (self.Hokuyotab.menuWidget.derecho.checkState()):
                                self.Hokuyotab.plotCanvas.CreateMarkers(DER_KEY,X[de_idx],Y[de_idx],DER_COLOR)
                            else:
                                self.Hokuyotab.plotCanvas.CreateMarkers(DER_KEY,np.zeros(2),np.zeros(2),DER_COLOR)
                
                else:
                    self.update_device()

    def SetSpinboxsRanges(self,i_min=0,i_max=540,d_min=540,d_max=1080):
            menuWidget = self.Hokuyotab.menuWidget

            menuWidget.rang_min_iz.setMinimum(i_min)
            menuWidget.rang_min_iz.setMaximum(i_max)

            menuWidget.rang_max_iz.setMinimum(i_min)
            menuWidget.rang_max_iz.setMaximum(i_max)

            menuWidget.rang_min_der.setMinimum(d_min)
            menuWidget.rang_min_der.setMaximum(d_max)
            
            menuWidget.rang_max_der.setMinimum(d_min)
            menuWidget.rang_max_der.setMaximum(d_max)


            menuWidget.angulo_az_0.setMinimum(-180)
            menuWidget.angulo_az_0.setMaximum(180)

    def reset(self):
        self.UpdatingParameters =True
        dev = self.getcurrentDevice()
        if dev !='':
            self.DevicesModel.SetDefaultParametersLaser(dev)
            self.update_menuwidget()
    
        else:
            self.DevicesModel.DetectHokuyoDevices() 
            self.update_device_list()
            self.update_device()
            self.update_menuwidget()
        self.UpdatingParameters =False
    

    def help(self):
        self.helpWindow.show() 
    
    def getcurrentDevice(self):
        port = str(self.Hokuyotab.menuWidget.puertos.currentText())
        return port

    def startAdquisition(self):
        self.timer.start()

    def stopAdquisition(self):
        self.timer.stop()
    


class LaserHarwtab(QtGui.QWidget):
    
    def __init__(self, parent=None,timerinterval=0.025):
        
        super(LaserHarwtab, self).__init__(parent)
        # se crea el layout a utilizar y se disponen los elementos

        F_layout = QtGui.QHBoxLayout() 
        self.plotCanvas = PlotDataWidget()
        self.plotCanvas.create_native()

        self.menuWidget = LaserMenuWidget()
        self.plotCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.plotCanvas.native)
        self.Next = MyButton("Siguiente",tooltip=CLOSE_CONFIGURATION)
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)


        #self.Next.clicked.connect(self.next_tab)
        # se crean los eventos a utlizar  y se inicializa el timer para capturar los datos de los sensores

    def setSesorsList(self, HokuyoList):
        self.menuWidget.puertos.addItems(HokuyoList)




class LaserMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=300):
        super(LaserMenuWidget, self).__init__(label=u"Panel de Control")

        l_puertos = QtGui.QGroupBox(u"Seleccionar Láser:")
        l_puertos_l = QtGui.QHBoxLayout()
        self.puertos = MyQComboBox()
        self.puertos.setToolTip(SELECT_LASER)
        self.puertos.currentIndexChanged.connect(self.update_param)
        l_puertos_l.addWidget(self.puertos)
        l_puertos.setLayout(l_puertos_l)
        self.gbox.addRow(l_puertos)

        l_angle0 = QtGui.QGroupBox(u"Ángulo Haz 0:")

        l_angle0_l = QtGui.QFormLayout()

        #Rangos minimo Carril Izquierdo
        self.angulo_az_0 = MyQSpinBox(tooltip=ANGLE_0_TOOLTIP)

        self.angulo_az_0.valueChanged.connect(self.update_param)
        l_angle0_l.addRow(TextAndSpinBox(u"Ángulo(°):",self.angulo_az_0,-180,180))

        l_angle0.setLayout(l_angle0_l)
        self.gbox.addRow(l_angle0)
        
        
        # group Box de Carriles
        l_roi = QtGui.QGroupBox("Carriles:")
        l_roi_l = QtGui.QFormLayout()
        
        # checkbox carril iquierdo
        self.izquiero = MyQCheckBox('Carril Izquierdo',tooltip=LEFT_LINE_ENABLE)        
        l_roi_l.addRow(self.izquiero)
        self.izquiero.stateChanged.connect(self.update_param)

        #Rangos minimo Carril Izquierdo
        self.rang_min_iz = MyQSpinBox(tooltip=LEFT_LINE_MIN_RANGE)
        self.rang_min_iz.valueChanged.connect(self.update_param)
        l_roi_l.addRow(TextAndSpinBox("Rango min:",self.rang_min_iz,0,540))
        
        self.rang_max_iz = MyQSpinBox(tooltip=LEFT_LINE_MAX_RANGE)
        self.rang_max_iz.valueChanged.connect(self.update_param)
        l_roi_l.addRow(TextAndSpinBox("Rango max:",self.rang_max_iz,0,540))
        
        # checkbox carril Derecho
        self.derecho = MyQCheckBox('Carril Derecho',tooltip=RIGHT_LINE_ENABLE)        
        l_roi_l.addRow(self.derecho)
        self.derecho.stateChanged.connect(self.update_param)

        #Rangos minimo Carril Derecho
        self.rang_min_der = MyQSpinBox(tooltip=RIGHT_LINE_MIN_RANGE)
        self.rang_min_der.valueChanged.connect(self.update_param)
        l_roi_l.addRow(TextAndSpinBox("Rango min:",self.rang_min_der,541,1080))
        
        self.rang_max_der = MyQSpinBox(tooltip=RIGHT_LINE_MIN_RANGE)
        self.rang_max_der.valueChanged.connect(self.update_param)
        l_roi_l.addRow(TextAndSpinBox("Rango max:",self.rang_max_der,541,1080))
        l_roi.setLayout(l_roi_l)
        self.gbox.addRow(l_roi)


if __name__ == '__main__':
    
 
    os.system('cls' if os.name == 'nt' else 'clear')
    sys.path.insert(0, './Backend')
    from Backend.DeviceModel import DevicesModel
    import logging
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    model = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    ex = LaserHarwtab()
    Controller = HokuyoController(ex,model)
    ex.show()
    Controller.startAdquisition()
    sys.exit(app.exec_())

#    os.system('cls' if os.name == 'nt' else 'clear')
#     sys.path.insert(0, './Backend')
#     from Configuration import DeviceConfiguration
#     import logging
#     logging.getLogger().setLevel(logging.DEBUG)
#     Config = DeviceConfiguration("./Backend/parametros")
#     Config.DetectHokuyoDevices()
    
#     app = QtGui.QApplication(sys.argv)
#     ex = LaserHarwtab(devConfig=Config)
#     ex.show()
#     ex.startAdquisition()
#     sys.exit(app.exec_())
