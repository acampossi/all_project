import numpy as np
import pcl
import copy
from  PyQt4 import QtGui,QtCore
from Constants import *
class polar2rec:

    def __init__(self,angle):
        self.PI = np.pi
        self.angle = angle
        self.POL2RECSIN= np.sin(np.arange(self.angle,self.angle+270.25,0.25)*self.PI/180) 
        self.POL2RECCOS= np.cos(np.arange(self.angle,self.angle+270.25,0.25)*self.PI/180) 
    def pol2rec(self,data,angle,scale=1):
        if angle!=self.angle:
            self.angle = angle
            self.POL2RECSIN= np.sin(np.arange(self.angle,self.angle+270.25,0.25)*self.PI/180) 
            self.POL2RECCOS= np.cos(np.arange(self.angle,self.angle+270.25,0.25)*self.PI/180) 
        X = self.POL2RECCOS*data[-1081:]
        Y = self.POL2RECSIN*data[-1081:]
        if scale !=1:
            X = X/scale
            Y = Y/scale
        return X,Y
    
    @staticmethod
    def Pol2Rec(angle,data,xaxisAngle=90,scale=1):
        PI = np.pi
        POL2RECSIN= np.sin((angle+xaxisAngle)*PI/180) 
        POL2RECCOS= np.cos((angle+xaxisAngle)*PI/180) 
        X = POL2RECCOS*data
        Y = POL2RECSIN*data
        if scale !=1:
            X = X/scale
            Y = Y/scale
        return X,Y

class ClasificationViewers:

    def __init__(self,DevicesModel):
        self.DevicesModel = DevicesModel
        self.start = True
        self.cloud_original = "cloud_original"
        self.cloud_cluster = "cloud_cluster"
        self.cloud_clustered_filtered = "cloud_clustered_filtered"

        self.color = ['g', 'r', 'b', 'y', 'c', 'm']

        self.CloudBase = pcl.PointCloud(1)
        self.CloudBase_array = np.zeros((1,3), dtype=np.float64)
        self.CloudCluster_array = np.zeros((1,3), dtype=np.float64)
        self.CloudFiltered_array =np.zeros((1,3), dtype=np.float64)
        self.CloudFPFH = np.zeros((1,6), dtype=np.float64)

        self.largoBase = 0.0
        self.CloudSet = False
        self.setAllCloud()


    def setBaseCloud(self):
        try:
            dlg = QtGui.QFileDialog()
            cloud_path = str(dlg.getOpenFileName(caption= "Seleccionar Nube...", directory="./Nubes_Puntos", filter=("PCD files (*NoGround.pcd)")))
            self.CloudBase = pcl.load(cloud_path)
        except Exception as e:
            pass
        self.setAllCloud()

    def setAllCloud(self):

        self.CloudBase_array = np.array(self.CloudBase)
        self.largoBase = (-1)*(np.max(self.CloudBase_array[:,2])+np.amin(self.CloudBase_array[:,2]))/2
        self.CloudBase_array[:,2] = np.add(self.CloudBase_array[:,2],self.largoBase)

        self.CloudCluster_array , self.Color_cluster= self.SegmentCreator(self.largoBase)
        self.CloudFiltered_array = self.FilterOutliersCreator(self.largoBase)
        self.CloudFPFH = self.FPFHCreator()


    def setClusterCloud(self):
        self.CloudSet = True
        self.CloudCluster_array , self.Color_cluster= self.SegmentCreator(self.largoBase)

    def setFilteredCloud(self):
        self.CloudFiltered_array = self.FilterOutliersCreator(self.largoBase)

    def  setFPFHCloud(self):
        self.CloudFPFH = self.FPFHCreator()
        
    def SegmentCreator(self, largoXY):
        params = self.DevicesModel.globalDic["Cluster"]
        nube_pasar = np.asarray(self.CloudBase, dtype=np.float64)
        CloudCluster = np.zeros((1,3), dtype=np.float64)

        # self.DevicesModel.Segmentador_C.clearMymap()
        self.DevicesModel.Segmentador_C.passCloud(self.cloud_original, nube_pasar)
        self.DevicesModel.Segmentador_C.GetIndicesGlobal(self.cloud_original,params["Cluster_min"], params["Cluster_tolerance"],params["Cluster_max"])
        self.ClusterIndicesSize = self.DevicesModel.Segmentador_C.getSizeClusterGlobal()
        ColorVector = ['w']

        it3 = 0
        for it3 in xrange(0,self.ClusterIndicesSize):
            if (self.ClusterIndicesSize>1):
                self.DevicesModel.Segmentador_C.GetClusterGlobal(self.cloud_original,self.cloud_cluster,it3)
                cloud_cluster_aux = self.cloud_cluster
                # self.DevicesModel.Segmentador_C.DemeanCluster(self.cloud_cluster,self.cloud_cluster)
            else:
                # self.DevicesModel.Segmentador_C.DemeanCluster(self.cloud_original,self.cloud_cluster)
                self.DevicesModel.Segmentador_C.GetClusterGlobal(self.cloud_original,self.cloud_cluster,it3)
                cloud_cluster_aux = self.cloud_cluster


            Auxiliar_cloud = np.zeros((self.DevicesModel.Segmentador_C.getCloudSize(cloud_cluster_aux),3), dtype=np.float64)

            ColorPos = it3%6
            ColorVector = ColorVector + [self.color[ColorPos]]*self.DevicesModel.Segmentador_C.getCloudSize(cloud_cluster_aux)

            self.DevicesModel.Segmentador_C.getClusterCloud(cloud_cluster_aux, Auxiliar_cloud);
            Auxiliar_cloud[:,2] = np.add(Auxiliar_cloud[:,2], largoXY)

            CloudCluster = np.concatenate((CloudCluster,Auxiliar_cloud), axis=0)


        return CloudCluster, ColorVector

    def FilterOutliersCreator(self, largoXY):
        params = self.DevicesModel.globalDic["Cluster"]
        CloudFiltered = np.zeros((1,3), dtype=np.float64)

        it3 = 0
        for it3 in xrange(0,self.ClusterIndicesSize):
            if (self.ClusterIndicesSize>1):
                self.DevicesModel.Segmentador_C.GetClusterGlobal(self.cloud_original,self.cloud_cluster,it3)
                # self.DevicesModel.Segmentador_C.DemeanCluster(self.cloud_cluster,self.cloud_cluster)
                cloud_cluster_aux = self.cloud_cluster
            else:
                # self.DevicesModel.Segmentador_C.DemeanCluster(self.cloud_original,self.cloud_cluster)
                cloud_cluster_aux = self.cloud_original
            self.DevicesModel.Segmentador_C.filter_outliers(cloud_cluster_aux,self.cloud_clustered_filtered, int(params["Filter_mean"]), params["Filter_Thresh"])

            Auxiliar_cloud = np.zeros((self.DevicesModel.Segmentador_C.getCloudSize(self.cloud_clustered_filtered),3), dtype=np.float64)

            self.DevicesModel.Segmentador_C.getClusterCloud(self.cloud_clustered_filtered, Auxiliar_cloud);
            Auxiliar_cloud[:,2] = np.add(Auxiliar_cloud[:,2], largoXY)

            CloudFiltered = np.concatenate((CloudFiltered,Auxiliar_cloud), axis=0)
 

            vectorMin = np.zeros((3,), dtype=np.float64)
            vectorMax = np.zeros((3,), dtype=np.float64)
            vectorCent = np.zeros((3,), dtype=np.float64)
            self.DevicesModel.Segmentador_C.CalculateMinMax(self.cloud_clustered_filtered, vectorMin, vectorMax, vectorCent)
            largoXY = largoXY -  (vectorMin[2])

        return CloudFiltered

    def FPFHCreator(self):
        params = self.DevicesModel.globalDic["FPFH"]

        self.DevicesModel.Descriptor_C.passCloud(self.CloudFiltered_array)
        self.DevicesModel.Descriptor_C.CalculateNormals_V(params["Normales_radiusS"],False)

        CloudSize = self.DevicesModel.Descriptor_C.getCloudSize()        
        CloudFPFH = np.zeros((CloudSize,6), dtype=np.float64)
        self.DevicesModel.Descriptor_C.Get_point_normals(CloudFPFH)
        self.DevicesModel.Descriptor_C.ClearValues()
        return CloudFPFH

     

def angle2haz(angle):
    return 4*angle+180        


