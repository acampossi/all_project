#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
LP_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, LP_BASEDIR+'/../VisualizationWidgets')
from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import CameraCanvas,MyButton,TextAndSpinBox,ResetButton,SearchButton,SearchWidget,MyQComboBox,MyQCheckBox
#

class LPtab(QtGui.QWidget):
    
    def __init__(self,Controller=None, parent=None,timerinterval=0.025):
        super(LPtab, self).__init__(parent)
    
        F_layout = QtGui.QHBoxLayout() 
        self.CameraCanvas = CameraCanvas()
        self.CameraCanvas.create_native()
        self.menuWidget = LPMenuWidget()
        self.CameraCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.CameraCanvas.native)
        self.Next = MyButton("Siguiente")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)


    def getcurrentDevice(self):
        port = str(self.menuWidget.puertos.currentText())
        return port
    
    def setCameraList(self,cameraList):
        self.menuWidget.puertos.addItems(cameraList)

    def setLaneList(self,Carriles):
        self.menuWidget.Carril.addItems(Carriles)


class LPMenuWidget(QtGui.QGroupBox):
    """
    Widget for editing OBJECT parameters
    """
    signal_objet_changed = QtCore.pyqtSignal(name='objectChanged')

    def __init__(self, parent=None,maxWidth=300):
        super(LPMenuWidget, self).__init__(u"Panel de Control", parent=parent)

        self.setMaximumWidth(maxWidth)
        gbox = QtGui.QFormLayout()


        l_puertos = QtGui.QGroupBox(u"Seleccionar Cámara:")
        l_puertos_l = QtGui.QHBoxLayout()
        self.puertos = MyQComboBox()
        self.puertos.currentIndexChanged.connect(self.update_param)
        l_puertos_l.addWidget(self.puertos)
        l_puertos.setLayout(l_puertos_l)
        gbox.addRow(l_puertos)
        
                

        # group Box de Carriles
        l_almacenamiento = QtGui.QGroupBox(u"Almacenamiento de datos:")
        l_almacenamiento_l = QtGui.QFormLayout()

        #Rangos minimo Carril Izquierdo
        self.save_images = MyQCheckBox(u'Almacenar imágenes')        
        l_almacenamiento_l.addRow(self.save_images)
        self.save_video = MyQCheckBox('Almacenar Video')        
        l_almacenamiento_l.addRow(self.save_video)
        self.Detect_LP = MyQCheckBox('Detectar Placas')        
        l_almacenamiento_l.addRow(self.Detect_LP)
        self.Ruta = QtGui.QTextEdit(u"Imágenes")
        self.select_Ruta = SearchButton()
        l_almacenamiento_l.addWidget(SearchWidget(self.Ruta,self.select_Ruta))
        l_almacenamiento.setLayout(l_almacenamiento_l)
        gbox.addRow(l_almacenamiento)
        
        
        vbox = QtGui.QVBoxLayout()
        vbox.addLayout(gbox)
        vbox.addStretch(1.0)
        # se adiciona el boton de reset        
        self.resetButton = ResetButton()
        ResetButton_l =QtGui.QHBoxLayout()
        ResetButton_l.addStretch(1)
        ResetButton_l.addWidget(self.resetButton)
        vbox.addLayout(ResetButton_l)
       
        self.setLayout(vbox)
        
        



        



    def update_param(self, option):
        self.signal_objet_changed.emit()


if __name__ == '__main__':
    # Import  para script de prueba
    sys.path.insert(0, './Backend')
    from Backend.DeviceModel import DevicesModel
    import logging

    #configuracion del logger
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    model = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    ex = LPtab()
    ex.show()
    sys.exit(app.exec_())
