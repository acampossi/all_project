#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
PROSILICA_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, PROSILICA_BASEDIR+'../VisualizationWidgets')
from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import CameraCanvas,MyButton,TextAndSpinBox,ResetButton,RadButtonandslider,MyQComboBox,MyQSpinBox,HelpButton,HelpWindow,ConfigurationWidget
from Constants import *

class CamerasController:
    """
    Clase que interconecta el modelo del sistema con la interfaz grafica para el control de las camaras (Controlador)
    Atributos:
            DevicesModel        : Conexion con el modelo del sistema 
            Prosilicatab        : Conexiones con las pesta;as de configuracion de las camaras de la interfaz grafica
            lastdevice          : El ultimo dispositivo escogido por el ususario
            timer               : temporizador que generara una interrupcion cada cierto intervalo cuando la  pesa;a este siendo visualizada
    """
    DevicesModel = None
    Prosilicatab = None
    lastdevice = None
    timer = None
    UpdatingParameters =False
        
    def __init__(self, Prosilicatab, DevicesModel,timerinterval ='auto'):
        # se guarda el modelo y la vista en los atributos de las clases
        self.Prosilicatab = Prosilicatab
        self.DevicesModel = DevicesModel
        # Evento cuando se elije un dispositivo diferente
        self.Prosilicatab.menuWidget.Carril.addItems(CARRILES)
        self.Prosilicatab.menuWidget.puertos.currentIndexChanged.connect(self.update_device)
        # evento cuando las configuraciones cambian
        
        self.Prosilicatab.menuWidget.signal_objet_changed.connect(self.update_param)
        self.Prosilicatab.menuWidget.resetButton.clicked.connect(self.Reset)
        # se  crea un reloj que permitira actualizar la adquisicion de datos de os sensores emulando una interupcion y se guarda el intervalo predefinido
        self.Prosilicatab.menuWidget.helpButton.clicked.connect(self.help)
        self.helpWindow = HelpWindow(PROSILICA_BASEDIR+u'/../VisualizationWidgets/help_prosilicatab.png',parent=self)
    
        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        # se actualizan los puertos existentes
        
        temp =self.Prosilicatab.getcurrentDevice()
        self.update_device_list()
        self.update_device()

    def update_param(self):
        dev = self.Prosilicatab.getcurrentDevice()
        if  not  self.UpdatingParameters:
            if dev !='':
                menuWidget =self.Prosilicatab.menuWidget
                try:
                    cam =self.DevicesModel.Cameras.GetCamera(dev)
                    cam.OffsetX = menuWidget.roi_x.value()
                    cam.OffsetY = menuWidget.roi_y.value()
                    cam.Width = menuWidget.roi_w.value()
                    cam.Height = menuWidget.roi_h.value()
                    cam.Carril = str(menuWidget.Carril.currentText())

                    cam.GainAutoTarget= menuWidget.Gain_Rate.value()
                    cam.BalanceWhiteAutoRate= menuWidget.WhiteBalance_Rate.value() 
                    cam.ExposureAutoTarget= menuWidget.Exposure_Rate.value()
                    self.DevicesModel.Cameras.SetCameraGain(dev,menuWidget.Gain_BG.checkedId())
                    self.DevicesModel.Cameras.SetCameraBalanceWhite(dev,menuWidget.WhiteBalance_BG.checkedId())
                    self.DevicesModel.Cameras.SetCameraExposure(dev,menuWidget.Exposure_BG.checkedId())

            
                except:
                    print "Configuracion Fallida"

    def update_menuwidget(self):
        self.UpdatingParameters =True
        dev = self.Prosilicatab.getcurrentDevice()
        if dev !='':
            cam =self.DevicesModel.Cameras.GetCamera(dev)
            #se define el maximo y minimo del offset en x 
            menuWidget = self.Prosilicatab.menuWidget
            menuWidget.roi_x.setMinimum(0)
            menuWidget.roi_x.setValue(cam.OffsetX)
            menuWidget.roi_x.setMaximum(cam.WidthMax)

            menuWidget.roi_w.setMinimum(0)
            menuWidget.roi_w.setValue(cam.Width)
            menuWidget.roi_w.setMaximum(cam.WidthMax)

            #se define el maximo y minimo del offset en y 
            menuWidget.roi_y.setMinimum(0)
            menuWidget.roi_y.setValue(cam.OffsetY)
            menuWidget.roi_y.setMaximum(cam.HeightMax)
            
            menuWidget.roi_h.setMinimum(0)
            menuWidget.roi_h.setValue(cam.Height)
            menuWidget.roi_h.setMaximum(cam.HeightMax)

            menuWidget.Gain_BG.buttonList[self.DevicesModel.Cameras.GetCameraGain(dev)].setChecked(True)
            menuWidget.Exposure_BG.buttonList[self.DevicesModel.Cameras.GetCameraExposure(dev)].setChecked(True)
            menuWidget.WhiteBalance_BG.buttonList[self.DevicesModel.Cameras.GetCameraBalanceWhite(dev)].setChecked(True)
            
            CamerasController.setSliderValues(menuWidget.Gain_Rate,0,100,cam.GainAutoTarget)
            CamerasController.setSliderValues(menuWidget.WhiteBalance_Rate,0,100,cam.BalanceWhiteAutoRate)
            CamerasController.setSliderValues(menuWidget.Exposure_Rate,0,100,cam.ExposureAutoTarget)
        self.UpdatingParameters =False
        
    def help(self):
        self.helpWindow.show()


    def Reset(self):
        self.UpdatingParameters =True
        dev = self.Prosilicatab.getcurrentDevice()
        source  = self.DevicesModel.DefaultDic
        self.DevicesModel.Cameras.ConfigureCamera(dev,source)
        self.update_menuwidget()
        self.UpdatingParameters =False
    
    @staticmethod
    def setSliderValues(slider,min,max,value):
            slider.setMinimum(min)
            slider.setMaximum(max)            
            slider.setValue(value)

    

    def update_device_list(self):
        camerasids = self.DevicesModel.Cameras.GetCamerasId()
        self.Prosilicatab.setCameraList(camerasids)          
        self.update_menuwidget()
    
    
    def update_device(self):
        self.update_menuwidget()

    def timerevent(self,event):
       dev = self.Prosilicatab.getcurrentDevice()
       if dev !='':
            success, img = self.DevicesModel.Cameras.getFrame(dev)
            if success:
                self.Prosilicatab.CameraCanvas.showImage(img) 

    def startAdquisition(self):
        self.update_device()
        self.timer.start()

    def stopAdquisition(self):
        self.timer.stop()



    def setDeviceModel(self,DevicesModel):
        self.DevicesModel = DevicesModel

    def setProsilicatab(self,Prosilicatab):
        self.Prosilicatab = Prosilicatab


    



class ProsilicaHarwtab(QtGui.QWidget):
    
    def __init__(self,Controller=None, parent=None,timerinterval=0.025):
        super(ProsilicaHarwtab, self).__init__(parent)
        
        F_layout = QtGui.QHBoxLayout() 
        self.CameraCanvas = CameraCanvas()
        self.CameraCanvas.create_native()
        self.menuWidget = CameraMenuWidget()
        self.CameraCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.CameraCanvas.native)
        self.Next = MyButton("Siguiente")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)


    def getcurrentDevice(self):
        port = str(self.menuWidget.puertos.currentText())
        return port
    
    def setCameraList(self,cameraList):
        self.menuWidget.puertos.addItems(cameraList)

    def setLaneList(self,Carriles):
        self.menuWidget.Carril.addItems(Carriles)


class CameraMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=300):
        super(CameraMenuWidget, self).__init__(label=u"Panel de Control",maxWidth=maxWidth)
      


        l_puertos = QtGui.QGroupBox(u"Seleccionar Cámara:")
        l_puertos_l = QtGui.QHBoxLayout()
        self.puertos = MyQComboBox()
        self.puertos.currentIndexChanged.connect(self.update_param)
        l_puertos_l.addWidget(self.puertos)
        l_puertos.setLayout(l_puertos_l)
        self.gbox.addRow(l_puertos)
        
        l_Carril = QtGui.QGroupBox(u"Carril Asociado:")
        l_Carril_l = QtGui.QHBoxLayout()
        self.Carril = MyQComboBox()
        self.Carril.currentIndexChanged.connect(self.update_param)
        l_Carril_l.addWidget(self.Carril)
        l_Carril.setLayout(l_Carril_l)
        self.gbox.addRow(l_Carril)
                

        # group Box de Carriles
        l_roi = QtGui.QGroupBox("ROI:")
        l_roi_l = QtGui.QFormLayout()
        l_roi_l.setMargin(0)
        l_roi_l.setSpacing(0)
        
        self.roi_x = MyQSpinBox()
        self.roi_x.valueChanged.connect(self.update_param)
        self.roi_y = MyQSpinBox()
        self.roi_y.valueChanged.connect(self.update_param)

        l_roi_l.addRow(TextAndSpinBox("X:",self.roi_x,0,0),TextAndSpinBox("Y:",self.roi_y,0,0))

        self.roi_w = MyQSpinBox()
        self.roi_w.valueChanged.connect(self.update_param)
        self.roi_h = MyQSpinBox()
        self.roi_h.valueChanged.connect(self.update_param)

        l_roi_l.addRow(TextAndSpinBox("W:",self.roi_w,0,0),TextAndSpinBox("H:",self.roi_h,0,0))
        l_roi.setLayout(l_roi_l)
        self.gbox.addRow(l_roi)
        
        
        
        self.Exposure_Rate = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.Exposure_Rate.valueChanged.connect(self.update_param)
        self.Exposure_BG = RadButtonandslider(u"Exposición:",
                                        ["Ninguno","Unico","Continuo"],
                                        self.Exposure_Rate,
                                        u"Velocidad de ajuste")
        
        self.Exposure_BG.buttonClicked.connect(self.update_param)
        self.gbox.addRow(self.Exposure_BG.widget)


        self.Gain_Rate = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.Gain_Rate.valueChanged.connect(self.update_param)
        self.Gain_BG = RadButtonandslider(u"Ganancia:",
                                        ["Ninguno","Unico","Continuo"],
                                        self.Gain_Rate,
                                        u"Velocidad de ajuste")
        
        self.Gain_BG.buttonClicked.connect(self.update_param)
        self.gbox.addRow(self.Gain_BG.widget)


        self.WhiteBalance_Rate = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.WhiteBalance_Rate.valueChanged.connect(self.update_param)
        self.WhiteBalance_BG = RadButtonandslider(u"Balance de blancos:",
                                        ["Ninguno","Unico","Continuo"],
                                        self.WhiteBalance_Rate,
                                        u"Velocidad de ajuste")
        
        self.WhiteBalance_BG.buttonClicked.connect(self.update_param)
        self.gbox.addRow(self.WhiteBalance_BG.widget)



if __name__ == '__main__':
    sys.path.insert(0, './Backend')
    from Backend.DeviceModel import DevicesModel
    import logging

    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    model = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    ex = ProsilicaHarwtab()
    Controller = CamerasController(ex,model)
    ex.show()
    Controller.startAdquisition()
    sys.exit(app.exec_())
