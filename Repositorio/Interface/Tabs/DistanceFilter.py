#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
import copy
from Utils import polar2rec
import time
from Constants import *
HOKUYO_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, HOKUYO_BASEDIR+u'/../VisualizationWidgets')
from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import PlotDataWidget,MyButton,TextAndSpinBox,ResetButton,SearchButton,SearchWidget,PointCloudCanvas,MyQComboBox,MyQCheckBox,ConfigurationWidget
#from VisualizationWidgets import *



class DistanceFilterController:
    
    DevicesModel = None
    DistanceFilterTab = None
    lastdevice = None
    timer = None
    UpdatingParameters =False
    Device = None
    RadarIz = None
    RadarDe = None
    pos = None
    def __init__(self, DistanceFilterTab, DevicesModel,timerinterval ='auto'):
        # se guarda el modelo y la vista en los atributos de las clases
        self.DistanceFilterTab = DistanceFilterTab

        self.DevicesModel = DevicesModel
        # Se definen los rangos de los spinbox
        self.SetSpinboxsRanges()
        # se actualizan los puertos existentes
        self.update_line_list()
        self.update_line()
        # Evento cuando se elije un dispositivo diferente
        self.DistanceFilterTab.menuWidget.Carril.currentIndexChanged.connect(self.update_line)
        # evento cuando las configuraciones cambian
        self.DistanceFilterTab.menuWidget.signal_objet_changed.connect(self.update_param)
        # se  crea un reloj que permitira actualizar la adquisicion de datos de os sensores emulando una interupcion y se guarda el intervalo predefinido
        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        self.DistanceFilterTab.menuWidget.resetButton.clicked.connect(lambda: self.update_menu(RESET))
        self.DistanceFilterTab.menuWidget.select_Ruta.clicked.connect(self.SelectForlder)

        self.pos = np.zeros(shape=(BEAMS_BUFFER*BEAMS,3))
        
        self.pos_count = BEAMS_BUFFER

        self.polar2rec =polar2rec(-45)
        
    def update_menu(self,mode):
        
        self.UpdatingParameters =True
        line_key = self.getcurrentLine()
        if mode == UPDATE:
            Source_Dic = self.DevicesModel.globalDic["Creation"][line_key]
        else:
            Source_Dic = self.DevicesModel.DefaultDic["Creation"][line_key]

        menu_dic = self.DistanceFilterTab.menuWidget
        menu_dic.rang_x_min.setValue(Source_Dic["x_limit"][0])
        menu_dic.rang_x_max.setValue(Source_Dic["x_limit"][1])
        menu_dic.rang_y_min.setValue(Source_Dic["y_limit"][0])
        menu_dic.rang_y_max.setValue(Source_Dic["y_limit"][1])
        # se modifica el estado de los checkbox
        menu_dic.save_original.setChecked(Source_Dic["save_original"])
        menu_dic.save_filtered.setChecked(Source_Dic["save_filtered"])
        menu_dic.save_No_ground.setChecked(Source_Dic["save_No_ground"])
        menu_dic.Ruta.setText(Source_Dic["Original_Folder"] )
        menu_dic.default_vel.setValue(Source_Dic["default_vel"] )
       
        
        self.UpdatingParameters =False
        if mode == RESET:
            self.update_param()
        

    def update_line(self):
        self.update_menu(UPDATE)


    def update_line_list(self):
        if self.DevicesModel.globalDic != None:
            line_list =self.DevicesModel.globalDic["Creation"].keys()
            self.DistanceFilterTab.menuWidget.Carril.addItems(line_list)          
            self.update_menu(UPDATE)
        else:
            print "No se ha encontrado archivo de configuracion"

    def update_param(self):
        
        if not self.UpdatingParameters: 
            line_key = self.getcurrentLine()
            Source_Dic = self.DevicesModel.globalDic["Creation"][line_key]
            menu_dic = self.DistanceFilterTab.menuWidget
            Source_Dic["x_limit"][0] = menu_dic.rang_x_min.value()
            Source_Dic["x_limit"][1] = menu_dic.rang_x_max.value()
            Source_Dic["y_limit"][0] = menu_dic.rang_y_min.value()
            Source_Dic["y_limit"][1] = menu_dic.rang_y_max.value()
            # se modifica el estado de los checkbox
            Source_Dic["save_original"] = menu_dic.save_original.isChecked()
            Source_Dic["save_filtered"] = menu_dic.save_filtered.isChecked()
            Source_Dic["save_No_ground"] = menu_dic.save_No_ground.isChecked()
            Source_Dic["Original_Folder"] = str(menu_dic.Ruta.text())
            Source_Dic["default_vel"] = menu_dic.default_vel.value()
    def timerevent(self,event):
        if not self.UpdatingParameters: 
            if self.Device !=None:
                if self.Device.Plugged:
                    if self.Device.is_open():
                        success =True
                        isHokuyo = False
                        ini = time.time()
                        try:
                            if self.Device in self.DevicesModel.Hokuyo_Dic.values():
                                data =self.Device.getRangeScanAndAppend(0,0)
                                isHokuyo = True
                            elif  self.Device in self.DevicesModel.Laser_Dic.values():
                                data = self.Device.read_scans()
                                if data == []:
                                    success = False
                                data = np.array(data)
                        except:
                            success = False 
                        

                        if success:
                            line = self.getcurrentLine()
                            x_limit =self.DevicesModel.globalDic["Creation"][line]["x_limit"]
                            y_limit =self.DevicesModel.globalDic["Creation"][line]["y_limit"]
                            default_vel = self.DevicesModel.globalDic["Creation"][line]["default_vel"]
                            
                            if isHokuyo:
                                X,Y = self.polar2rec.pol2rec(data,self.Device.angle_a0,scale=1000)
                                
                                if line!=IZQ_KEY:
                                    # print IZQ_KEY
                                    x_limit = [-x_limit[1],-x_limit[0]]

                                X = np.append(X[self.Device.i_range_min:self.Device.i_range_max],X[self.Device.d_range_min:self.Device.d_range_max])
                                Y = np.append(Y[self.Device.i_range_min:self.Device.i_range_max],Y[self.Device.d_range_min:self.Device.d_range_max] )
                                dis =0.1 #Aqui se implementaria la velocidad del radar si al final se decide por ello

                            else:
                                X,Y = polar2rec.Pol2Rec(data[:,1],data[:,2],xaxisAngle=self.Device.angle_a0,scale=1000)
                                idx =  np.logical_or(np.logical_and(data[:,1]>=self.Device.i_range_min,data[:,1]<=self.Device.i_range_max),
                                            np.logical_and(data[:,1]>=self.Device.d_range_min,data[:,1]<=self.Device.d_range_max)) 
      
                                if line==IZQ_KEY:
                                    x_limit = [-x_limit[1],-x_limit[0]]
      
                                X,Y = X[idx],Y[idx]
                                dis = (1.0/10.0)*(default_vel)

                            length = X.shape[0]
                            # dis =0.1
                            
                            self.pos[length:,:] = self.pos[:-length,:]
                            self.pos[length:,2] =self.pos[length:,2]+dis
                            self.pos[:length,0] = X
                            self.pos[:length,1] = Y
                            self.pos[:length,2] = 0
                            
                            samples = np.logical_and (
                                np.logical_and( self.pos[:,0]>=x_limit[0], self.pos[:,1]>=y_limit[0]) ,
                                np.logical_and( self.pos[:,0]<=x_limit[1], self.pos[:,1]<=y_limit[1]) )
                            self.posA = copy.copy(self.pos)
                            if isHokuyo:
                                self.posA[:,1] = self.posA[:,1]*(-1)
                            else:
                                pass
                            self.DistanceFilterTab.plotCanvas.set_data(self.posA[samples,:],colorV=(0,0,0,1))
                                                            

            else:
                self.update_device()


    def SelectForlder(self):
        dlg = QtGui.QFileDialog()
        folder_path = str(dlg.getExistingDirectory(caption= "Seleccionar Carpeta..."))
        if folder_path!= '':
            menuWidget = self.DistanceFilterTab.menuWidget
            menuWidget.Ruta.setText(folder_path)
            self.update_param()
    

    def SetSpinboxsRanges(self):
        
        menu_dic = self.DistanceFilterTab.menuWidget.__dict__
        for key in DISTANCE_FILTER_SPINBOX:
            params = DISTANCE_FILTER_SPINBOX[key]
            
            menu_dic[key].setMinimum(params[0])
            menu_dic[key].setMaximum(params[1])
            if len(params)==3:
                menu_dic[key].setSingleStep(params[2])

    def getcurrentLine(self):
        port = str(self.DistanceFilterTab.menuWidget.Carril.currentText())
        return port

    def startAdquisition(self):
        self.timer.start()
        self.update_device()

    def stopAdquisition(self):
        self.timer.stop()
        try:
            if self.Device != None:
                self.Device.LaserDispose()    
        except:
            pass
        try:
            if self.RadarDe!=None:
                self.RadarDe.disconnect()
        except:
            pass
        try:
            if self.RadarIz!=None:
                self.RadarIz.disconnect()
        except:
            pass
    
            
    def update_device(self):
        dev_port =''
        for key, value in self.DevicesModel.Laser_Dic.iteritems():
            if value.Plugged:
                dev_port = key
                self.Device =value
                self.isHokuyo =False
        for key, value in self.DevicesModel.Hokuyo_Dic.iteritems():
            if value.Plugged:
                dev_port = key
                self.Device =value
        if dev_port != '':
            self.DevicesModel.OpenLaser(dev_port)
    

        # for key, value in self.DevicesModel.Radar_Dic.iteritems():
        #     if value.Plugged:
        #         os.chmod(key,484)
        #         Radar =self.DevicesModel.Radar_Dic[key]
        #         Radar.connect()
        #         Radar.configSensor()
        #         if Radar.Carril == IZQ_KEY:
        #             self.RadarIz = Radar
        #             print "Radar Izquierdo Encontrado"
        #         else:
        #             self.RadarDe = Radar
        #             print "Radar Derecho Encontrado"


class DisFilertab(QtGui.QWidget):
    
    def __init__(self, parent=None,timerinterval=0.025):
        
        super(DisFilertab, self).__init__(parent)
        # se crea el layout a utilizar y se disponen los elementos

        F_layout = QtGui.QHBoxLayout() 
        self.plotCanvas = PointCloudCanvas()
        self.plotCanvas.create_native()

        self.menuWidget = DisFilMenuWidget()
        self.plotCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.plotCanvas.native)
        self.Next = MyButton("Siguiente")
        
        S_layout = QtGui.QHBoxLayout() 
        
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)


        #self.Next.clicked.connect(self.next_tab)
        # se crean los eventos a utlizar  y se inicializa el timer para capturar los datos de los sensores

    def setSesorsList(self, HokuyoList):
        self.menuWidget.puertos.addItems(HokuyoList)

    @staticmethod
    def pol2rec(data):
        X = POL2RECCOS*data[2:]
        Y = POL2RECSIN*data[2:]
        return X,Y

class DisFilMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=500):
        super(DisFilMenuWidget, self).__init__(label=u"Panel de Control",maxWidth=maxWidth)

        l_carril = QtGui.QGroupBox(u"Seleccionar Carril:")
        l_carril_l = QtGui.QHBoxLayout()
        self.Carril = MyQComboBox()
        l_carril_l.addWidget(self.Carril)
        l_carril.setLayout(l_carril_l)
        self.gbox.addRow(l_carril)

        l_dis_fil = QtGui.QGroupBox("Filtro de distancia:")
        l_dis_fil_l = QtGui.QFormLayout()
        # checkbox carril iquierdo
        
        #Rangos minimo Carril Izquierdo
        self.rang_x_min = QtGui.QDoubleSpinBox()
        self.rang_x_min.setSingleStep(0.1)
        self.rang_x_min.valueChanged.connect(self.update_param)
        l_dis_fil_l.addRow(TextAndSpinBox("X min:", self.rang_x_min, 0.1, 5))
     
        self.rang_x_max = QtGui.QDoubleSpinBox()
        self.rang_x_max.setSingleStep(0.1)
        self.rang_x_max.valueChanged.connect(self.update_param)
        l_dis_fil_l.addRow(TextAndSpinBox("X max:", self.rang_x_max, 0.1, 5))
        
        # checkbox carril Derecho
        
        #Rangos minimo Carril Derecho
        self.rang_y_min = QtGui.QDoubleSpinBox()
        self.rang_y_min.setSingleStep(0.1)
        self.rang_y_min.valueChanged.connect(self.update_param)
        l_dis_fil_l.addRow(TextAndSpinBox("Y min:", self.rang_y_min, -3, 5))
        
        self.rang_y_max = QtGui.QDoubleSpinBox()
        self.rang_y_max.setSingleStep(0.1)
        self.rang_y_max.valueChanged.connect(self.update_param)
        l_dis_fil_l.addRow(TextAndSpinBox("Y max:", self.rang_y_max, -3, 5))
        l_dis_fil.setLayout(l_dis_fil_l)
        self.gbox.addRow(l_dis_fil)


        l_almacenamiento = QtGui.QGroupBox(u"Almacenamiento de datos:")
        l_almacenamiento_l = QtGui.QFormLayout()

        #Rangos minimo Carril Izquierdo
        self.save_original = MyQCheckBox('Nube Original')        
        l_almacenamiento_l.addRow(self.save_original)
        self.save_original.stateChanged.connect(self.update_param)

        self.save_filtered = MyQCheckBox('Nube Filtrada')        
        self.save_filtered.stateChanged.connect(self.update_param)
        l_almacenamiento_l.addRow(self.save_filtered)
        
        self.save_No_ground = MyQCheckBox('Nube sin suelo')        
        self.save_No_ground.stateChanged.connect(self.update_param)
        
        l_almacenamiento_l.addRow(self.save_No_ground)
        self.Ruta = QtGui.QLineEdit("Nubes")
        self.select_Ruta = SearchButton()
        
        l_almacenamiento_l.addWidget(SearchWidget(self.Ruta,self.select_Ruta))
        
        l_almacenamiento.setLayout(l_almacenamiento_l)
        self.gbox.addRow(l_almacenamiento)
        

        l_vel = QtGui.QGroupBox("Velocidad por defecto:")
        l_vel_l = QtGui.QFormLayout()
        # checkbox carril iquierdo
        
        #Rangos minimo Carril Izquierdo
        self.default_vel = QtGui.QDoubleSpinBox()
        self.default_vel.setSingleStep(0.1)
        self.default_vel.valueChanged.connect(self.update_param)
        l_vel_l.addRow(TextAndSpinBox("Velocidad:", self.default_vel, 0.1, 5.))
        l_vel.setLayout(l_vel_l)
        self.gbox.addRow(l_vel)


if __name__ == '__main__':
    
 
    os.system('cls' if os.name == 'nt' else 'clear')
    #sys.path.insert(0, './Backend')
    import logging
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    app.setStyle("plastique")
    
    ex = DisFilertab()
    ex.show()
    sys.exit(app.exec_())
