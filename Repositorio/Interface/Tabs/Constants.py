#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Constastes del hokuyo
"""
BEAMS =1081

RESOLUTION = 0.25

APERTURE = 270

"""
Constantes para la creacion de la nube de puntos para el despliegue de la nube de puntos en el filtrado de distancias
"""

BEAMS_BUFFER = 120


""" 
constante para determinar si los updates se realizan a partir del archivo
 de configuracion o el archivo de parametros por defecto"""
RESET = 0
UPDATE  = 1

"""
LLaves de las graficas  de izquierda y derecha
"""
IZQ_KEY = "Izquierdo"
DER_KEY = "Derecho"
ROI_KEY = 2

"""
Colores de las graficas 
"""
IZQ_COLOR = (1,0,0,1)
DER_COLOR = (0,0,1,1)
LIMIT_COLOR = (0.82,0,0.98,0.5)



"""Nombres de los carriles a utilizar"""

CARRILES = ["Izquierdo","Derecho"]

""" Rangos de los spinbox a utilizar en cada tab"""

SAMPLES_SELECTION_SPINBOX = {   "Vector_Temporal_Size": [1,20],
                                "Laser_start" : [10,100],
                                "Laser_stop" : [10,100],
                                "Laser_step" : [1,5],
                                "Distance_min" : [0.1,3.0,0.1],
                                "Distance_max" : [0.1,5.0,0.1],
                                "Min_No_Vehicles" : [5,20],
                                "Min_Vehicles_Samples" : [10,30]
                                }
DISTANCE_FILTER_SPINBOX = { "rang_x_min": [0.1,3.0,0.1],
                            "rang_y_min" :[-5.0,-0.3,0.1],
                            "rang_x_max" : [0.5,5.0,0.1],
                            "rang_y_max" : [0.1,3.0,0.1],
                            "default_vel" : [1.0,5.0,0.1]
                            
                            }


""" texto de los tooltips en la interfaz grafica en la pestaña de configuracion Hardware >Laser"""

SELECT_LASER = u"Seleccionar Láser"

ANGLE_0_TOOLTIP =u"Angulo que se forma entre el haz a0 del láser y el plano de las abscisas"

LEFT_LINE_ENABLE = u"Habilitar o deshabilitar el carril izquierdo"

LEFT_LINE_MIN_RANGE = u"Primer haz  de interes en el carril izquierdo"

LEFT_LINE_MAX_RANGE = u"Ultimo haz  de interes en el carril izquierdo"


RIGHT_LINE_ENABLE = u"Habilitar o deshabilitar el carril derecho"

RIGHT_LINE_MIN_RANGE = u"Primer haz  de interes en el carril derecho"

RIGHT_LINE_MAX_RANGE = u"Ultimo haz  de interes en el carril derecho"

RESET_HOKUYO_TAB  = u"Buscar Dispositivos  o poner parametros por defecto"


CLOSE_CONFIGURATION = u"Terminar la configuración del sistema"

