#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
from Utils import ClasificationViewers
CLASIFICATION_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, CLASIFICATION_BASEDIR+'/../VisualizationWidgets')

from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import PlotDataWidget,MyButton,TextAndSpinBox,ResetButton,SearchButton,SearchWidget,PointCloudCanvas,MyQComboBox,MyQSpinBox,MyQCheckBox,ConfigurationWidget,HelpWindow, PointCloudCanvasGrid, CarButton,CameraButton
#from VisualizationWidgets import *

CLUSTERS = "Cluster"
FILTER  = "Filter"
FPFH = "FPFH"
CLASSIFIER = "SVM"
COM = "Com"
MEANS_TRAINING =['k-means++','PCA-Components','random'] 
KERNELS = ['lin','poly','rbf','lin-SVC']
UPDATE = 0
RESET = 1
class ClassificationController:
    DevicesModel = None
    ClassficationTab = None
    UpdatingParameters =False
    
    
    def __init__(self, ClassficationTab, DevicesModel):
        # se guarda el modelo y la vista en los atributos de las clases
        self.ClassficationTab = ClassficationTab

        self.DevicesModel = DevicesModel
        self.ClasificationViewers = ClasificationViewers(self.DevicesModel)
        self.PointCloudCanvasGrid = PointCloudCanvasGrid()

        self.Cluster_Change = False
        self.Filter_Outliers_Change = False
        self.FPFH_Change = False

        # Se definen los rangos de los spinbox
        self.SetSpinboxsRanges()
        # se actualizan los puertos existentes
        self.update_menu(CLUSTERS, UPDATE)
        self.update_menu(FILTER, UPDATE)
        self.update_menu(FPFH, UPDATE)
        self.update_menu(CLASSIFIER, UPDATE)
        self.update_menu(COM, UPDATE)
        
        # evento cuando las configuraciones cambian
        self.ClassficationTab.ClustersWidget.signal_objet_changed.connect(lambda:self.update_params(CLUSTERS))
        self.ClassficationTab.FiltradoMenuWidget.signal_objet_changed.connect(lambda:self.update_params(FILTER))
        self.ClassficationTab.FPFHWidget.signal_objet_changed.connect(lambda:self.update_params(FPFH))
        self.ClassficationTab.Classifier.signal_objet_changed.connect(lambda:self.update_params(CLASSIFIER))
        self.ClassficationTab.Communication.signal_objet_changed.connect(lambda:self.update_params(COM))
        
        # eventos cuando se resetea los parametros a por defecto
        self.ClassficationTab.ClustersWidget.resetButton.clicked.connect(lambda:self.update_menu(CLUSTERS ,RESET))
        self.ClassficationTab.FiltradoMenuWidget.resetButton.clicked.connect(lambda:self.update_menu(FILTER ,RESET))
        self.ClassficationTab.FPFHWidget.resetButton.clicked.connect(lambda:self.update_menu(FPFH ,RESET))
        self.ClassficationTab.Classifier.resetButton.clicked.connect(lambda:self.update_menu(CLASSIFIER ,RESET))
        self.ClassficationTab.Communication.resetButton.clicked.connect(lambda:self.update_menu(COM ,RESET))


        self.ClassficationTab.ClustersWidget.select_Ruta.clicked.connect(lambda:self.SelectForlder(CLUSTERS ))
        self.ClassficationTab.Classifier.select_Ruta.clicked.connect(lambda:self.SelectForlder(CLASSIFIER))

        
        self.helpWindow_clusters = HelpWindow(CLASIFICATION_BASEDIR+'/../VisualizationWidgets/help_cluster.png',parent=self)
        self.helpWindow_filter = HelpWindow(CLASIFICATION_BASEDIR+'/../VisualizationWidgets/help_filter.png',parent=self)
        self.helpWindow_FPFH = HelpWindow(CLASIFICATION_BASEDIR+'/../VisualizationWidgets/help_FPFH.png',parent=self)
        self.helpWindow_classifier = HelpWindow(CLASIFICATION_BASEDIR+'/../VisualizationWidgets/help_classifier.png',parent=self)
      
        self.ClassficationTab.ClustersWidget.helpButton.clicked.connect(lambda:self.helpWindow_clusters.show())
        self.ClassficationTab.FiltradoMenuWidget.helpButton.clicked.connect(lambda:self.helpWindow_filter.show())
        self.ClassficationTab.FPFHWidget.helpButton.clicked.connect(lambda:self.helpWindow_FPFH.show())
        self.ClassficationTab.Classifier.helpButton.clicked.connect(lambda:self.helpWindow_classifier.show())

        self.ClassficationTab.Nube.clicked.connect(self.ChangeCloud)
        self.ClassficationTab.Ejemplos.clicked.connect(self.ShowClouds) 

    def ChangeCloud(self):
        self.ClasificationViewers.setBaseCloud()
        self.startVisualization()

    def ShowClouds(self):
        if self.Cluster_Change:
            self.ClasificationViewers.setClusterCloud()
        else:
            pass

        if self.Filter_Outliers_Change:
            self.ClasificationViewers.setFilteredCloud()
        else:
            pass

        if self.FPFH_Change:
            self.ClasificationViewers.setFPFHCloud()
        else:
            pass

        self.startVisualization()

        self.Cluster_Change = False
        self.Filter_Outliers_Change = False
        self.FPFH_Change = False

    def startVisualization(self):
        self.PointCloudCanvasGrid.show()
        self.PointCloudCanvasGrid.grid1.set_data(self.ClasificationViewers.CloudBase_array)
        self.PointCloudCanvasGrid.grid2.set_data(self.ClasificationViewers.CloudCluster_array, self.ClasificationViewers.Color_cluster)
        self.PointCloudCanvasGrid.grid3.set_data(self.ClasificationViewers.CloudFiltered_array)
        self.PointCloudCanvasGrid.grid4.set_data(self.ClasificationViewers.CloudFPFH)

    def stopVisualization(self):
        self.PointCloudCanvasGrid.close()
        self.Visualization = False

    def update_menu(self,tag,mode):
        if mode == UPDATE:
            Source_Dic = self.DevicesModel.globalDic
        else:
            Source_Dic = self.DevicesModel.DefaultDic
        if tag ==CLUSTERS :
            dic = Source_Dic[CLUSTERS]
            ClustersWidget = self.ClassficationTab.ClustersWidget
            ClustersWidget.min_size.setValue(dic["Cluster_min"])
            ClustersWidget.max_size.setValue(dic["Cluster_max"])
            ClustersWidget.tolerance.setValue(dic["Cluster_tolerance"])
            ClustersWidget.wheels_min_size.setValue(dic["Wheel_size_min"])
            ClustersWidget.wheels_max_size.setValue(dic["Wheel_size_max"])
            ClustersWidget.wheels_tolerance.setValue(dic["Wheel_tolerance"])
            ClustersWidget.wheels_delta_ground.setValue(dic["Wheel_delta_ground"])
            ClustersWidget.ground_distance.setValue(dic["Wheel_distance_filter"])
            ClustersWidget.min_width.setValue(dic["Wheel_min_width"])
            ClustersWidget.Clusters.setChecked(dic["save_cluster"])
            ClustersWidget.Ruta.setText(dic["Cluster_Folder"])
        if tag == FILTER:
            dic = Source_Dic[CLUSTERS]
            FiltradoMenuWidget = self.ClassficationTab.FiltradoMenuWidget
            FiltradoMenuWidget.mean.setValue(dic["Filter_mean"])
            FiltradoMenuWidget.Treshold.setValue(dic["Filter_Thresh"])
        if tag == FPFH:
            dic = Source_Dic[FPFH]
            FPFHWidget = self.ClassficationTab.FPFHWidget
            FPFHWidget.delta_radio.setValue(dic["Density_delta"])
            FPFHWidget.normales.setValue(dic["Normales_radiusS"])
            FPFHWidget.uniform_sampling.setValue(dic["Uniform_Sampling_radius"])
            FPFHWidget.fpfh_radio.setValue(dic["FPFH_radiusS"])            
        if tag == CLASSIFIER:
            dic = Source_Dic[CLASSIFIER]
            Classifier = self.ClassficationTab.Classifier
            Classifier.Ruta.setText(dic["Model_path"])
            Classifier.Kmeans.setCurrentIndex(MEANS_TRAINING.index(dic["km_selected"]))
            Classifier.Kernel.setCurrentIndex(KERNELS.index(dic["svm_selected"]))
        if tag == COM:
            dic = Source_Dic[COM]
            Communication = self.ClassficationTab.Communication
            Communication.tcp_port.setValue(dic["tcp_port"])
            Communication.ip_0.setValue(dic["ip_0"])
            Communication.ip_1.setValue(dic["ip_1"])
            Communication.ip_2.setValue(dic["ip_2"])
            Communication.ip_3.setValue(dic["ip_3"])
        
        
    def update_params(self,tag):
        if tag ==CLUSTERS :
            dic = self.DevicesModel.globalDic[CLUSTERS]
            ClustersWidget = self.ClassficationTab.ClustersWidget
            dic["Cluster_min"] = ClustersWidget.min_size.value()
            dic["Cluster_max"] = ClustersWidget.max_size.value()
            dic["Cluster_tolerance"] = ClustersWidget.tolerance.value()
            dic["Wheel_size_min"] = ClustersWidget.wheels_min_size.value()
            dic["Wheel_size_max"] = ClustersWidget.wheels_max_size.value()
            dic["Wheel_tolerance"] = ClustersWidget.wheels_tolerance.value()
            dic["Wheel_delta_ground"] = ClustersWidget.wheels_delta_ground.value()
            dic["Wheel_distance_filter"] = ClustersWidget.ground_distance.value()
            dic["Wheel_min_width"] = ClustersWidget.min_width.value()
            dic["save_cluster"] = ClustersWidget.Clusters.isChecked()
            dic["Cluster_Folder"] = str(ClustersWidget.Ruta.text())
            self.Cluster_Change = True
            self.Filter_Outliers_Change = True
            self.FPFH_Change = True

        if tag == FILTER:
            dic = self.DevicesModel.globalDic[CLUSTERS]
            FiltradoMenuWidget = self.ClassficationTab.FiltradoMenuWidget
            dic["Filter_mean"] = FiltradoMenuWidget.mean.value()
            dic["Filter_Thresh"] = FiltradoMenuWidget.Treshold.value()
            self.Filter_Outliers_Change = True
            self.FPFH_Change = True
            
        if tag == FPFH:
            dic = self.DevicesModel.globalDic[FPFH]
            FPFHWidget = self.ClassficationTab.FPFHWidget
            dic["Density_delta"] = FPFHWidget.delta_radio.value()
            dic["Normales_radiusS"] = FPFHWidget.normales.value()
            dic["Uniform_Sampling_radius"] =FPFHWidget.uniform_sampling.value()
            dic["FPFH_radiusS"] =FPFHWidget.fpfh_radio.value()  
            self.FPFH_Change = True          
            
        if tag == CLASSIFIER:
            dic = self.DevicesModel.globalDic[CLASSIFIER]
            Classifier = self.ClassficationTab.Classifier
            dic["Model_path"] = str(Classifier.Ruta.text())
            dic["km_selected"] = str(Classifier.Kmeans.currentText())
            dic["SVM_selected"] =str(Classifier.Kernel.currentText())

        if tag == COM:
            dic = self.DevicesModel.globalDic[COM]
            Communication = self.ClassficationTab.Communication
            dic["tcp_port"] = Communication.tcp_port.value()
            dic["ip_0"] = Communication.ip_0.value()
            dic["ip_1"] = Communication.ip_1.value()
            dic["ip_2"] = Communication.ip_2.value()
            dic["ip_3"] = Communication.ip_3.value()
            
    def SelectForlder(self,tag):
        dlg = QtGui.QFileDialog()
        folder_path = str(dlg.getExistingDirectory(caption= "Seleccionar Carpeta..."))
        if folder_path!= '':
            if tag == CLUSTERS :
                ClustersWidget = self.ClassficationTab.ClustersWidget
                ClustersWidget.Ruta.setText(folder_path)
                self.update_params(tag)
            if tag == CLASSIFIER :
                Classifier = self.ClassficationTab.Classifier
                Classifier.Ruta.setText(folder_path)
                self.update_params(tag)
            

    def SetSpinboxsRanges(self):

        ClustersWidget = self.ClassficationTab.ClustersWidget
        # tama;o minimo clusters
        ClustersWidget.min_size.setMinimum(30)
        ClustersWidget.min_size.setMaximum(500000)
        # tama;o maximoclusters
        
        ClustersWidget.max_size.setMinimum(50)
        ClustersWidget.max_size.setMaximum(500000)
        #tolerancia entre vehiculo y vehiculo
        ClustersWidget.tolerance.setMinimum(0.20)
        ClustersWidget.tolerance.setMaximum(1.00)
        ClustersWidget.tolerance.setSingleStep(0.05)

        # tama;o minimo clusters
        ClustersWidget.wheels_min_size.setMinimum(50)
        ClustersWidget.wheels_min_size.setMaximum(500000)

        # tama;o maximo clusters
        ClustersWidget.wheels_max_size.setMinimum(50)
        ClustersWidget.wheels_max_size.setMaximum(500000)
        
        # tolerancia clusters llantas
        ClustersWidget.wheels_tolerance.setMinimum(0.20)
        ClustersWidget.wheels_tolerance.setMaximum(1.00)
        ClustersWidget.wheels_tolerance.setSingleStep(0.05)

        # delta entre llanta y piso
        ClustersWidget.wheels_delta_ground.setMinimum(0.005)
        ClustersWidget.wheels_delta_ground.setMaximum(0.03)
        ClustersWidget.wheels_delta_ground.setSingleStep(0.001)
        
        # DISTANCIA AL SUELO
        ClustersWidget.ground_distance.setMinimum(0.07)
        ClustersWidget.ground_distance.setMaximum(0.30)
        ClustersWidget.ground_distance.setSingleStep(0.01)
        # ANCHO MINIMO
        ClustersWidget.min_width.setMinimum(0.1)
        ClustersWidget.min_width.setMaximum(0.4)
        ClustersWidget.min_width.setSingleStep(0.01)


        FiltradoMenuWidget = self.ClassficationTab.FiltradoMenuWidget
        # promedio
        FiltradoMenuWidget.mean.setMinimum(1)
        FiltradoMenuWidget.mean.setMaximum(100)
        FiltradoMenuWidget.mean.setSingleStep(1)
        # umbral
        FiltradoMenuWidget.Treshold.setMinimum(0.5)
        FiltradoMenuWidget.Treshold.setMaximum(10)
        FiltradoMenuWidget.Treshold.setSingleStep(0.1)


        FPFHWidget = self.ClassficationTab.FPFHWidget
        # promedio
        FPFHWidget.delta_radio.setMinimum(2)
        FPFHWidget.delta_radio.setMaximum(10)
        FPFHWidget.delta_radio.setSingleStep(0.01)
        # radio normales
        FPFHWidget.normales.setMinimum(0.02)
        FPFHWidget.normales.setMaximum(0.25)
        FPFHWidget.normales.setSingleStep(0.01)
        # muestreo uniforme
        FPFHWidget.uniform_sampling.setMinimum(0.01)
        FPFHWidget.uniform_sampling.setMaximum(0.125)
        FPFHWidget.uniform_sampling.setSingleStep(0.01)
        # radio fpfh
        FPFHWidget.fpfh_radio.setMinimum(0.04)
        FPFHWidget.fpfh_radio.setMaximum(0.50)
        FPFHWidget.fpfh_radio.setSingleStep(0.01)


            
    


class ClasificationTab(QtGui.QWidget):
    
    def __init__(self, parent=None,timerinterval=0.025):
        
        super(ClasificationTab, self).__init__(parent)
        F_layout = QtGui.QHBoxLayout() 

        self.ClustersWidget= ClustersMenuWidget()
        self.FPFHWidget = FPFHMenuWidget()
        self.Classifier = ClassifierMenuWidget()
        self.FiltradoMenuWidget = FiltradoMenuWidget()
        self.Communication = CommunicationMenuWidget()
        

        fil_fpfh =QtGui.QWidget()
        fil_fpfh_l = QtGui.QVBoxLayout()
        fil_fpfh_l.addWidget(self.FiltradoMenuWidget)
        fil_fpfh_l.addWidget(self.FPFHWidget)
        fil_fpfh.setLayout(fil_fpfh_l)
        F_layout.addWidget(self.ClustersWidget)
        F_layout.addWidget(fil_fpfh)
        
        class_com =QtGui.QWidget()
        class_com_l = QtGui.QVBoxLayout()
        class_com_l.addWidget(self.Classifier)
        class_com_l.addWidget(self.Communication)
        class_com.setLayout(class_com_l)
        F_layout.addWidget(class_com)
        

        self.Next = MyButton("Siguiente")
        self.Ejemplos = CameraButton("")
        self.Nube = CarButton("")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addWidget(self.Ejemplos)
        S_layout.addWidget(self.Nube)
        
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)

class FiltradoMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=None):
        super(FiltradoMenuWidget, self).__init__(label=u"Filtrado Estadistico",maxWidth=maxWidth)
        

        self.mean = QtGui.QDoubleSpinBox()
        self.mean.setSingleStep(0.1)
        self.mean.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("Puntos a Promediar:", self.mean, 1, 100))
        self.Treshold = QtGui.QDoubleSpinBox()
        self.Treshold.setSingleStep(0.1)
        self.Treshold.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("Umbral:", self.Treshold, 0.1, 10))
        


class ClustersMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=None):
        super(ClustersMenuWidget, self).__init__(label=u"Clusterización",maxWidth=maxWidth)
        
        l_vehicles = QtGui.QGroupBox(u"Vehículos:")
        l_vehicles_l = QtGui.QFormLayout()
        

        #Rangos minimo Carril Izquierdo
        self.min_size = MyQSpinBox()
        self.min_size.valueChanged.connect(self.update_param)
        self.max_size = MyQSpinBox()
        self.max_size.valueChanged.connect(self.update_param)
        
        # l_vehicles_l.addRow(,)
        
        l_vehicles_l.addRow(TextAndSpinBox(u"Tamaño min:",self.min_size,50,50000),TextAndSpinBox(u"Tamaño max:",self.max_size,50,50000))

        self.tolerance = QtGui.QDoubleSpinBox()
        self.tolerance.setSingleStep(0.1)
        self.tolerance.valueChanged.connect(self.update_param)
        l_vehicles_l.addRow(TextAndSpinBox("Tolerancia:", self.tolerance, 0.1, 5))
        


        l_vehicles.setLayout(l_vehicles_l)
        self.gbox.addRow(l_vehicles)


        l_wheels = QtGui.QGroupBox("Llantas:")
        l_wheels_l = QtGui.QFormLayout()
        

        #Rangos minimo Carril Izquierdo
        self.wheels_min_size = MyQSpinBox()
        self.wheels_min_size.valueChanged.connect(self.update_param)
        self.wheels_max_size = MyQSpinBox()
        self.wheels_max_size.valueChanged.connect(self.update_param)
        

        l_wheels_l.addRow(TextAndSpinBox(u"Tamaño min:",self.wheels_min_size,50,50000),TextAndSpinBox(u"Tamaño max:",self.wheels_max_size,50,50000))

        self.wheels_tolerance = QtGui.QDoubleSpinBox()
        self.wheels_tolerance.setSingleStep(0.1)
        self.wheels_tolerance.valueChanged.connect(self.update_param)
        l_wheels_l.addRow(TextAndSpinBox(u"Tolerancia:", self.wheels_tolerance, 0.1, 5))
        
        self.wheels_delta_ground = QtGui.QDoubleSpinBox()
        self.wheels_delta_ground.setSingleStep(0.1)
        self.wheels_delta_ground.valueChanged.connect(self.update_param)
        l_wheels_l.addRow(TextAndSpinBox(u"Delta:", self.wheels_delta_ground, 0.1, 5))
        
        self.ground_distance = QtGui.QDoubleSpinBox()
        self.ground_distance.setSingleStep(0.1)
        self.ground_distance.valueChanged.connect(self.update_param)
        l_wheels_l.addRow(TextAndSpinBox(u"Distancia al suelo:", self.ground_distance, 0.1, 5))
        
        self.min_width = QtGui.QDoubleSpinBox()
        self.min_width.setSingleStep(0.1)
        self.min_width.valueChanged.connect(self.update_param)
        l_wheels_l.addRow(TextAndSpinBox(u"Ancho min:", self.min_width, 0.1, 5))
        

        l_wheels.setLayout(l_wheels_l)
        self.gbox.addRow(l_wheels)

        l_almacenamiento = QtGui.QGroupBox(u"Almacenamiento de datos:")
        l_almacenamiento_l = QtGui.QFormLayout()
        self.Clusters = MyQCheckBox('Clusters')        
        self.Clusters.stateChanged.connect(self.update_param)
        l_almacenamiento_l.addRow(self.Clusters)
        self.Ruta = QtGui.QLineEdit("Clusters")
        self.select_Ruta = SearchButton()
        l_almacenamiento_l.addWidget(SearchWidget(self.Ruta,self.select_Ruta))
        l_almacenamiento.setLayout(l_almacenamiento_l)
        self.gbox.addRow(l_almacenamiento)
        
     

class FPFHMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=None):
        super(FPFHMenuWidget, self).__init__(label=u"FPFH",maxWidth=maxWidth)
        self.delta_radio = QtGui.QDoubleSpinBox()
        self.delta_radio.setSingleStep(0.1)
        self.delta_radio.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("delta:", self.delta_radio, 0.1, 5))
        
        self.normales = QtGui.QDoubleSpinBox()
        self.normales.setSingleStep(0.1)
        self.normales.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("Normales:", self.normales, 0.1, 5))
        

        self.uniform_sampling = QtGui.QDoubleSpinBox()
        self.uniform_sampling.setSingleStep(0.1)
        self.uniform_sampling.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("Muestreo Uniforme:", self.uniform_sampling, -3, 5))
        
        self.fpfh_radio = QtGui.QDoubleSpinBox()
        self.fpfh_radio.setSingleStep(0.1)
        self.fpfh_radio.valueChanged.connect(self.update_param)
        self.gbox.addRow(TextAndSpinBox("Radio FPFH:", self.fpfh_radio, -3, 5))
        


class ClassifierMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=None):
        super(ClassifierMenuWidget, self).__init__(label=u"Clasificadores",maxWidth=maxWidth)

        l_almacenamiento = QtGui.QGroupBox(u"Ruta:")
        l_almacenamiento_l = QtGui.QHBoxLayout()
        self.Ruta = QtGui.QLineEdit("Prueba")
        self.select_Ruta = SearchButton()
        l_almacenamiento_l.addWidget(SearchWidget(self.Ruta,self.select_Ruta))
        l_almacenamiento.setLayout(l_almacenamiento_l)
        self.gbox.addRow(l_almacenamiento)
        
        l_Kmeans = QtGui.QGroupBox(u"Seleccionar Kmeans:")
        l_Kmeans_l = QtGui.QHBoxLayout()
        self.Kmeans = MyQComboBox()
        self.Kmeans.addItems(MEANS_TRAINING)
        
        self.Kmeans.currentIndexChanged.connect(self.update_param)
        l_Kmeans_l.addWidget(self.Kmeans)
        l_Kmeans.setLayout(l_Kmeans_l)
        self.gbox.addRow(l_Kmeans)


        l_Kernel = QtGui.QGroupBox(u"Seleccionar Kernel:")
        l_Kernel_l = QtGui.QHBoxLayout()
        self.Kernel = MyQComboBox()
        self.Kernel.addItems(KERNELS)
        
        self.Kernel.currentIndexChanged.connect(self.update_param)
        l_Kernel_l.addWidget(self.Kernel)
        l_Kernel.setLayout(l_Kernel_l)
        self.gbox.addRow(l_Kernel)

class CommunicationMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=None):
        super(CommunicationMenuWidget, self).__init__(label=u"Comunicación",maxWidth=maxWidth)

        l_config = QtGui.QGroupBox(u"Ruta IP:")
        v_layout = QtGui.QVBoxLayout()
        l_config_l = QtGui.QHBoxLayout()
        
        # l_config_l.addWidget(SearchWidget(self.Ruta,self.select_Ruta))
        self.ip_0 = MyQSpinBox()
        self.ip_1 = MyQSpinBox()
        self.ip_2 = MyQSpinBox()
        self.ip_3 = MyQSpinBox()
        self.ip_0.setMinimum(0),self.ip_0.setMaximum(255)
        self.ip_1.setMinimum(0),self.ip_1.setMaximum(255)
        self.ip_2.setMinimum(0),self.ip_2.setMaximum(255)
        self.ip_3.setMinimum(0),self.ip_3.setMaximum(255)
        l_config_l.addWidget(self.ip_0)
        l_config_l.addWidget(self.ip_1)
        l_config_l.addWidget(self.ip_2)
        l_config_l.addWidget(self.ip_3)
        self.ip_0.valueChanged.connect(self.update_param)
        self.ip_1.valueChanged.connect(self.update_param)
        self.ip_2.valueChanged.connect(self.update_param)
        self.ip_3.valueChanged.connect(self.update_param)
        
        self.tcp_port = MyQSpinBox()
        self.tcp_port.valueChanged.connect(self.update_param)
        v_layout.addLayout(l_config_l)
        v_layout.addWidget(TextAndSpinBox(u"Puerto:", self.tcp_port, 0, 65000))
        
        l_config.setLayout(v_layout)
        self.gbox.addRow(l_config)
        
       

if __name__ == '__main__':
    
 
    os.system('cls' if os.name == 'nt' else 'clear')
    #sys.path.insert(0, './Backend')
    sys.path.insert(0, CLASIFICATION_BASEDIR+ u'/../Backend')
    from Backend.DeviceModel import DevicesModel

    import logging
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    app.setStyle("plastique")
    
    ex = ClasificationTab()
    model = DevicesModel(CLASIFICATION_BASEDIR+ u"/../Backend/parametros")
    CC = ClassificationController(ex,model)
    ex.show()
    sys.exit(app.exec_())

