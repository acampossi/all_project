#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from  PyQt4 import QtGui,QtCore
import numpy as np
import os
RADAR_BASEDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, RADAR_BASEDIR+'../VisualizationWidgets')

from vispy.app import Timer
from VisualizationWidgets.VisualizationWidgets import PlotDataWidget,MyButton,TextAndSpinBox,ResetButton, MyQComboBox,MyQSpinBox,HelpButton,HelpWindow,ConfigurationWidget

CARRILES = ["Izquierdo","Derecho"]
class RadarController:
    """
    Clase que interconecta el modelo del sistema con la interfaz grafica para el control de las sensor StalkerRadar
    Atributos:
            DevicesModel        : Conexion con el modelo del sistema 
            Radartab           : Conexiones con las pesta;as de configuracion de las camaras de la interfaz grafica
            lastdevice          : El ultimo dispositivo escogido por el ususario
            timer               : temporizador que generara una interrupcion cada cierto intervalo cuando la  pesa;a este siendo visualizada
    """
    DevicesModel = None
    Radartab = None
    lastdevice = None
    timer = None
    UpdatingParameters =False
    
    def __init__(self, Radartab, DevicesModel,timerinterval =1/22):
        # se guarda el modelo y la vista en los atributos de las clases
        self.Radartab = Radartab
        
        self.DevicesModel = DevicesModel
        # Evento cuando se elije un dispositivo diferente
        self.Radartab.menuWidget.Carril.addItems(CARRILES)

        self.Radartab.menuWidget.puertos.currentIndexChanged.connect(self.update_device)
        # evento cuando las configuraciones cambian
        self.Radartab.menuWidget.signal_objet_changed.connect(self.update_param)
        # se  crea un reloj que permitira actualizar la adquisicion de datos de os sensores emulando una interupcion y se guarda el intervalo predefinido
        self.timerinterval = timerinterval
        self.timer = Timer(interval= timerinterval,connect= self.timerevent)
        # se actualizan los puertos existentes
        self.update_device_list()
        self.Radartab.menuWidget.resetButton.clicked.connect(self.reset)

        self.Radartab.menuWidget.helpButton.clicked.connect(self.help)
        self.helpWindow = HelpWindow(RADAR_BASEDIR+u'/../VisualizationWidgets/help_radartab.png',parent=self)
       
    def update_param(self):
        " Evento en el que se guarda los parametros de ingresados en la interfaz "
        
        dev = self.getcurrentDevice()
        if not self.UpdatingParameters:
            if dev !='':
                device = self.DevicesModel.Radar_Dic[dev]
                device.SetSensibility(self.Radartab.menuWidget.sensib_spinbox.value())
                device.SetGainRange(self.Radartab.menuWidget.gan_min_spinbox.value(),self.Radartab.menuWidget.gan_max_spinbox.value())
                device.Carril = str(self.Radartab.menuWidget.Carril.currentText())
                #print device.Carril
            else:
                self.DevicesModel.DetectRadarDevices() 
                self.update_device_list()
                
    def update_device(self):
        " Evento que se crea cuando se cambia el sensor seleccionado "
    
        dev_port = self.getcurrentDevice()
        if dev_port !='' and dev_port != self.lastdevice:
            os.chmod(dev_port,484)
            device =self.DevicesModel.Radar_Dic[dev_port]
            device.connect()
            device.configSensor()
            self.update_menuwidget()
            if self.lastdevice != None:
                self.DevicesModel.Radar_Dic[self.lastdevice].close()
            self.lastdevice = dev_port

    def help(self):
        self.helpWindow.show()
    
    

    def reset(self):
        dev = self.getcurrentDevice()

        if not self.UpdatingParameters:
            if dev !='':
                source = self.DevicesModel.DefaultDic["Radar"]
                device = self.DevicesModel.Radar_Dic[dev]
                device.SetSensibility(source["_sensibility"])
                device.SetGainRange(source["__minGain"],source["__maxGain"])
                device.Carril = source["Carril"]
                self.update_menuwidget()
            else:
                self.DevicesModel.DetectRadarDevices() 
                self.update_device_list()

    def timerevent(self,event):
        " Evento que se crea periodicamente para el despliegue de datos con intevalo de tiempo configurable"
        dev_port = self.getcurrentDevice()
        if dev_port !='':
            radar =self.DevicesModel.Radar_Dic[dev_port]
            self.vel_list = self.vel_list + radar.ReadData()
            list_len = len(self.vel_list)
            if list_len>100:
                self.vel_list = self.vel_list[-100:]
                list_len = 100
            IZQ_KEY = 0
            self.Radartab.plotCanvas.CreateLine(IZQ_KEY,np.arange(list_len),np.asarray(self.vel_list))
             
    def getcurrentDevice(self):
        " Retorna el sensor actualmente seleccionado en la interfaz grafica"
        port = str(self.Radartab.menuWidget.puertos.currentText())
        return port

    def startAdquisition(self):
        " Inicia la adquisicion del sensor"
        self.update_device()
        self.timer.start(interval=self.timerinterval)
        self.vel_list = []
            
    def stopAdquisition(self):
        " Detiene la adquisicion del sensor"
        self.timer.stop()
    

    
    def update_menuwidget(self):
        "metodo que lee la configuracion actual del sensor y la despliega en los widget de la interfaz grafica"
        self.UpdatingParameters =True
        dev = self.getcurrentDevice()
        if dev !='':
            device =self.DevicesModel.Radar_Dic[dev]
            # se define el maximo y minimo de la sensibilidad 
            menuWidget =self.Radartab.menuWidget
            menuWidget.sensib_spinbox.setMinimum(0)
            menuWidget.sensib_spinbox.setMaximum(15)
            menuWidget.sensib_spinbox.setValue(device.GetSensibility())
            
            # se define el maximo y minimo de la sensibilidad 
            minGain = device.GetMinGain()
            maxGain = device.GetMaxGain()
            # se define el maximo y minimo de ganancia minima 
            menuWidget.gan_min_spinbox.setMinimum(0)
            menuWidget.gan_min_spinbox.setMaximum(maxGain)
            menuWidget.gan_min_spinbox.setValue(minGain)
            
            menuWidget.gan_max_spinbox.setMinimum(minGain)
            menuWidget.gan_max_spinbox.setMaximum(7)
            menuWidget.gan_max_spinbox.setValue(maxGain)
            menuWidget.Carril.setCurrentIndex(CARRILES.index(device.Carril))
            
        self.UpdatingParameters = False
            

    def update_device_list(self):
        """Actualiza los posibles sensores a seleccionar"""
        List = []
        for key, value in self.DevicesModel.Radar_Dic.iteritems():
            if value.Plugged:
                List.append(key)
        self.Radartab.setSensorList(List)          
 
    def setDeviceModel(self,DevicesModel):
        self.DevicesModel = DevicesModel

    def setRadartab(self,Radartab):
        self.Radartab = Radartab



class RadarHarwtab(QtGui.QWidget):
    
    def __init__(self,devConfig=None, parent=None,timerinterval=0.025):
        
        super(RadarHarwtab, self).__init__(parent)
        F_layout = QtGui.QHBoxLayout() 
        self.plotCanvas = PlotDataWidget()
        self.plotCanvas.create_native()

        self.menuWidget = RadarMenuWidget()
        self.plotCanvas.native.setParent(self)
        F_layout.addWidget(self.menuWidget)
        F_layout.addWidget(self.plotCanvas.native)
        self.Next = MyButton("Siguiente")
        S_layout = QtGui.QHBoxLayout() 
        S_layout.addStretch(1)
        S_layout.addWidget(self.Next)
        V_layout = QtGui.QVBoxLayout()
        V_layout.addLayout(F_layout)
        V_layout.addLayout(S_layout)
        self.setLayout(V_layout)

    def setSensorList(self,device_list):
        self.menuWidget.puertos.addItems(device_list)

class RadarMenuWidget(ConfigurationWidget):
    
    def __init__(self,maxWidth=300):
        super(RadarMenuWidget, self).__init__(label=u"Panel de Control",maxWidth=maxWidth)
        
        l_puertos = QtGui.QGroupBox(u"Seleccionar Radar:")
        l_puertos_l = QtGui.QHBoxLayout()
        self.puertos = MyQComboBox()
        self.puertos.currentIndexChanged.connect(self.update_param)
        l_puertos_l.addWidget(self.puertos)
        l_puertos.setLayout(l_puertos_l)
        self.gbox.addRow(l_puertos)
        
        l_Carril = QtGui.QGroupBox(u"Carril Asociado:")
        l_Carril_l = QtGui.QHBoxLayout()
        self.Carril = MyQComboBox()
        self.Carril.currentIndexChanged.connect(self.update_param)
        l_Carril_l.addWidget(self.Carril)
        l_Carril.setLayout(l_Carril_l)
        self.gbox.addRow(l_Carril)
        
        l_ganancia = QtGui.QGroupBox("Ganancia:")
        l_ganancia_l = QtGui.QFormLayout()
         
        self.gan_min_spinbox = MyQSpinBox()
        self.gan_min_spinbox.valueChanged.connect(self.update_param)
        l_ganancia_l.addRow(TextAndSpinBox("min:",self.gan_min_spinbox,0,0))
        
        self.gan_max_spinbox = MyQSpinBox()
        self.gan_max_spinbox.valueChanged.connect(self.update_param)
        l_ganancia_l.addRow(TextAndSpinBox("max:",self.gan_max_spinbox,0,0))
        l_ganancia.setLayout(l_ganancia_l)
        self.gbox.addRow(l_ganancia)
        

        l_Sensibilidad = QtGui.QGroupBox("Sensibilidad:")
        l_Sensibilidad_l = QtGui.QFormLayout()
         
        self.sensib_spinbox = MyQSpinBox()
        self.sensib_spinbox.valueChanged.connect(self.update_param)
        l_Sensibilidad_l.addRow(TextAndSpinBox("",self.sensib_spinbox,0,0))
        
        l_Sensibilidad.setLayout(l_Sensibilidad_l)
        self.gbox.addRow(l_Sensibilidad)

        
        


if __name__ == '__main__':
    
    os.system('cls' if os.name == 'nt' else 'clear')

    sys.path.insert(0, './Backend')
    from Backend.DeviceModel import DevicesModel
    import logging
    #configuracion del logger
    logging.getLogger().setLevel(logging.DEBUG)
    app = QtGui.QApplication(sys.argv)
    model = DevicesModel("/home/ssi_ralvarez/Documentos/all_project/Repositorio/Interface/Backend/parametros")
    ex = RadarHarwtab()
    Controller = RadarController(ex,model)
    ex.show()
    Controller.startAdquisition()
    sys.exit(app.exec_())