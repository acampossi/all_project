#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os

INDEX_BASEDIR = os.path.dirname(os.path.abspath(__file__))
# os.environ['GENICAM_GENTL64_PATH'] = os.path.abspath(INDEX_BASEDIR+u"/Vimba_2_1/VimbaGigETL/CTI/x86_64bit")

# os.environ['GENICAM_GENTL64_PATH'] = "/home/jhon-kevin/Documentos/Vimba_2_1/VimbaGigETL/CTI/x86_64bit"
sys.path.insert(0,INDEX_BASEDIR +u'./VisualizationWidgets')
# set PYTHONPATH=INDEX_BASEDIR+'/VisualizationWidgets'
from VisualizationWidgets.VisualizationWidgets import Mytable, MyButton,QPlainTextEditLogger, ClosingMessage,SelectDevice


sys.path.insert(0,INDEX_BASEDIR +u'/Tabs')
from Tabs.Hokuyotab import HokuyoController
from Tabs.Radartab import RadarController
from Tabs.Prosilicatab import CamerasController
from Tabs.ThreadsTabs import ThreadsController
from Tabs.Clasification import ClassificationController
from Tabs.SamplesSelection import SamplesSelectionController
from Tabs.DistanceFilter import DistanceFilterController

sys.path.insert(0, INDEX_BASEDIR +u'./Backend')
from Backend.DeviceModel import DevicesModel


from PyQt4 import QtGui, QtCore
import logging


from tabs import Harware_Tabs, Software_Tabs


LASER_TAB_INDEX = 1
RADAR_TAB_INDEX = 2
PROSILICA_TAB_INDEX = 3


class MainController():
    """
    Clase que controla de forma global la aplicacion asignando los diferentes controladores 
    a cada una de las tabs    ademas de controlar la activacion y desactivacion de los timers
    """
    HardwareTimersEnabled =False 
    SoftwareTimersEnabled =False 


    def __init__(self, index, model, prototipe=False):
        
        #  se almacena las  diferetes vistas y tabs
        self.index = index
        self.model = model
        self.Hartab = index.Hartab
        self.Softab = index.Softab
        self.prototipe = prototipe
        #se crea el evento  para el control de los timers de despliegue
        self.Hartab.currentChanged.connect(self.tabChanged)
        self.Softab.currentChanged.connect(self.tabChanged)
        # se crea un evento que  permite conocer cuando se esta en los tabs 
        # o no para habilitar el encendido de los timers 
        self.index.stacked_layout.currentChanged.connect(self.EnableTimers)
        # Evento de cerrar
        self.index.closing.connect(self.Close)

        # controladores de sensores
        self.HokuyoController =  HokuyoController(self.Hartab.LaserHarwtab,model)
        self.RadarController = RadarController(self.Hartab.RadarHarwtab,model)
        self.CameraController = CamerasController(self.Hartab.ProsilicaHarwtab,model)
        # controladores de software
        self.ClassificationController = ClassificationController(self.Softab.Clasification,model)
        self.SamplesSelectionController = SamplesSelectionController(self.Softab.SamplesSelectionTab,model, self.prototipe) 
        self.DistanceFilterController = DistanceFilterController(self.Softab.DistanceFilter,model)
        # controladores de hilos
        self.ThreadsController = ThreadsController(self.model, self.index.table, self.index.winLogger)
        # se asigna los diferentes eventos a los botones       
        self.index.Iniciar.clicked.connect(self.play)
        self.index.parar.clicked.connect(self.Stop)
        self.index.Harw.clicked.connect(self.hardware)
        self.index.Soft.clicked.connect(self.software)
        logging.getLogger().addHandler(self.index.winLogger)
        # logging.getLogger().setLevel(logging.DEBUG)
        # se asigna el evento a los botones a los tabs de configuracion hardware para volver a la vista principal
        self.Hartab.LaserHarwtab.Next.clicked.connect(self.Stop)
        self.Hartab.ProsilicaHarwtab.Next.clicked.connect(self.Stop)
        self.Hartab.RadarHarwtab.Next.clicked.connect(self.Stop)
        # se asigna el evento a los botones a los tabs de configuracion software para volver a la vista principal
        self.Softab.DistanceFilter.Next.clicked.connect(self.Stop)
        self.Softab.Clasification.Next.clicked.connect(self.Stop)
        self.Softab.LPtab.Next.clicked.connect(self.Stop)
        self.Softab.SamplesSelectionTab.Next.clicked.connect(self.Stop)
        

    def play(self):
        
        if  self.index.stacked_layout.currentIndex()==0:
            laser_list,radar_list = self.EvaluateSensors()
            print laser_list, radar_list
            if len(laser_list)>1:
                self.index.DeviceSelection(laser_list)
                laser_list = self.index.selectDevice.selection
            
            if len(laser_list)!=0:
                self.index.stacked_layout.setCurrentIndex(1)
                self.index.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI")
                self.model.UpdateSoftwareObjects()
                # self.ThreadsController.stopThreads()
                Continue = self.ThreadsController.startThreads(laser_list,radar_list)
        
    def Stop(self):
        self.ThreadsController.stopThreads()
        self.index.stacked_layout.setCurrentIndex(0)
        self.index.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI")
        
    def hardware(self):
        if  self.index.stacked_layout.currentIndex()==0:

            self.index.stacked_layout.setCurrentIndex(2)
            self.index.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI - Configurar Hardware...")
        
    def software(self):
        if  self.index.stacked_layout.currentIndex()==0:

            self.index.stacked_layout.setCurrentIndex(3)
            self.index.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI - Configurar Software...")
        
    def EnableTimers(self):
        stacked_layout_index = self.index.stacked_layout.currentIndex()
        self.HardwareTimersEnabled = stacked_layout_index ==2
        self.SoftwareTimersEnabled = stacked_layout_index ==3
        self.tabChanged()

    def tabChanged(self):
        self.HokuyoController.stopAdquisition()
        self.CameraController.stopAdquisition()
        self.RadarController.stopAdquisition()
        # Software Timers
        self.SamplesSelectionController.stopAdquisition()
        self.DistanceFilterController.stopAdquisition()
        self.ClassificationController.stopVisualization()
        
        if  self.HardwareTimersEnabled:
            index = self.Hartab.currentIndex()
            if index == self.Hartab.ProsilicaHarwtab_index:
                self.CameraController.startAdquisition()
            elif index == self.Hartab.LaserHarwtab_index:
                self.HokuyoController.startAdquisition()
            elif index == self.Hartab.RadarHarwtab_index:
                self.RadarController.startAdquisition()

        self.SamplesSelectionController.stopAdquisition()
        if self.SoftwareTimersEnabled:
            index = self.Softab.currentIndex()
            if index == self.Softab.SamplesSelectionTab_index:
                self.SamplesSelectionController.startAdquisition()
            if index == self.Softab.DistanceFilter_index:
                self.DistanceFilterController.startAdquisition()

    def Close(self):
        if self.index.msg.result == self.index.msg.GUARDAR:
            self.model.SaveParameters()
        self.Stop()
        self.model.DisposeSensors()
        # try:
        #     # se cierran conexion con camaras y se  cierra vimba
        #     self.model.DisposeSensors() 
        # except:
        #     pass
            

    def EvaluateSensors(self):
        laser_list= []
        for dev_port in self.model.Hokuyo_Dic:
            opened = self.model.OpenLaser(dev_port)
            if opened:
                laser_list.append(["Hokuyo-utm30-lx",dev_port])
        for dev_port in self.model.Laser_Dic:
            opened = self.model.OpenLaser(dev_port)
            if opened:
                laser_list.append(["RPlidar-A2",dev_port])

        radar_list = []
        for dev_port in self.model.Radar_Dic:
            opened = self.model.OpenRadar(dev_port)
            if opened:
                radar_list.append(["Stalker Radar",dev_port,self.model.Radar_Dic[dev_port].Carril])
        return laser_list,radar_list        


class MainWindow(QtGui.QMainWindow):
    
    closing = QtCore.pyqtSignal(name='objectChanged')
    selectDevice = None
    def __init__(self,Prototipe=False):
        QtGui.QMainWindow.__init__(self)
        # Se redimensiona la ventana, se cambia en titulo, se pone el el fondo blanco
        self.CreateWidgets()
        self.stacked_layout = QtGui.QStackedLayout()
        self.prototipe = Prototipe
        self.stacked_layout.addWidget(self.CreateFirstLayout())
        self.stacked_layout.addWidget(self.CreateSecondLayout())
        self.Hartab = Harware_Tabs(Prototipe=Prototipe)
        self.stacked_layout.addWidget(self.Hartab)
        self.Softab = Software_Tabs(Prototipe=Prototipe)
        self.stacked_layout.addWidget(self.Softab)

        self.central_widget = QtGui.QWidget()
        self.central_widget.setLayout(self.stacked_layout)
        self.setCentralWidget(self.central_widget)
        self.stacked_layout.setCurrentIndex(0)

    def DeviceSelection(self,devices_list):
        self.selectDevice = SelectDevice(devices_list)
        
    def CreateWidgets(self):
        self.setWindowTitle(u"AFORO DE VEHÍCULOS - SSI")
        stylesheet = "background: white"
        self.setWindowIcon(QtGui.QIcon(INDEX_BASEDIR + '/VisualizationWidgets/icono2.png'))
        self.setStyleSheet(stylesheet)
        
        # se lee las imagenes desde archivo  y se asocian con los widgets
        self.pic1 = QtGui.QLabel()
        
        self.pic1.setGeometry(0,0, 600, 600)
        im = QtGui.QPixmap(INDEX_BASEDIR+ u'/VisualizationWidgets/icono4.png')
        self.pic1.setPixmap(im)

        # se crean los botones
        self.parar = MyButton("II")
        self.Iniciar = MyButton(u"►")
        self.Iniciar.setToolTip('This is a <b>QWidget</b> widget')
        self.Harw = MyButton("Hardware")
        self.Soft =MyButton("Software")

        # se crea el qgroup box para disponer las opciones de configuracion
        self.l_config = QtGui.QGroupBox(u"Configuración:")
        l_layout = QtGui.QHBoxLayout()
        l_layout.addWidget(self.Harw)
        l_layout.addWidget(self.Soft)
        self.l_config.setLayout(l_layout)
             
        # se instancia la tabla
        self.table =Mytable(14,3)
        # se instancia el logger
        self.winLogger = QPlainTextEditLogger(self)
        self.winLogger.widget.setStyleSheet("background: rgba(255,255,255,40%)")
        
        # self.winLogger.widget.hide()

    def CreateFirstLayout(self):
        # se define un layout tipo horizontal y se define la vista inicial
        First_layout = QtGui.QHBoxLayout()
        First_layout.addStretch(stretch = 1)
        First_layout.addWidget(self.pic1)
        First_layout.addStretch(stretch = 1)
        # se crea el segundo layout horizontal
        Second_layout = QtGui.QHBoxLayout()
        Second_layout.addWidget(self.l_config)
        Second_layout.addStretch(1)
        Second_layout.addWidget(self.Iniciar)
        # se crea el layout vertical y se disponen los layouts horizontales
        Ver_layout  = QtGui.QVBoxLayout()
        Ver_layout.addLayout(First_layout)
        Ver_layout.addLayout(Second_layout)
        widget = QtGui.QWidget()
        widget.setLayout(Ver_layout)
        return widget

    def CreateSecondLayout(self):
        spacing =30
        First_layout = QtGui.QHBoxLayout()
        First_layout.addSpacing(spacing)
        First_layout.addWidget(self.winLogger.widget)
        First_layout.addSpacing(spacing)
        First_layout.addWidget(self.table)
        First_layout.addSpacing(spacing)
        Second_layout = QtGui.QHBoxLayout()
        Second_layout.addStretch(1)
        Second_layout.addWidget(self.parar)
        Ver_layout  = QtGui.QVBoxLayout()
        Ver_layout.addSpacing(spacing)
        
        Ver_layout.addLayout(First_layout)
        Ver_layout.addLayout(Second_layout)
        widget = QtGui.QWidget()
        widget.setLayout(Ver_layout)
        return widget

    def closeEvent(self,event):
        self.msg = ClosingMessage()
        if self.msg.result != self.msg.CANCELAR:
            self.closing.emit()
            event.accept()
        else:
            event.ignore()
        
if __name__ == '__main__':
    
    logging.getLogger().setLevel(logging.INFO)
    

    #print os.environ['GENICAM_GENTL64_PATH'], "LEEER" 
    # bridge_OMP_modificado 
    # os.chmod('Backend.bridge_fpfh_vfh.so',356)
    os.chmod(INDEX_BASEDIR+ u"/Backend/bridge_fpfh_vfh.so",356)

    appQt = QtGui.QApplication(sys.argv)
    appQt.setStyle("plastique")
    Prototipe=False
    win = MainWindow(Prototipe=Prototipe)
    if Prototipe:
        model = DevicesModel(INDEX_BASEDIR+ u"/Backend/parametros_rplidar")
    else:
        model = DevicesModel(INDEX_BASEDIR+ u"/Backend/parametros")

    #modelSoft = Parameters_class(INDEX_BASEDIR+ u"/Backend/default_parameters")
    controller = MainController(win,model, prototipe=Prototipe)
    win.show()
    appQt.exec_()
    # model.DisposeSensors()
