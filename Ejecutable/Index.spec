# -*- mode: python -*-

block_cipher = None

options = [ ('v', None, 'OPTION'), ('W ignore', None, 'OPTION') ]
a = Analysis(['../../Repositorio/Interface/Index.py'],
             pathex=['/home/jhon-kevin/Documentos/all_project/Ejecutable/Repo'],
             binaries=[],
             datas=[('/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Backend/parametros','./Backend'),
                    ('/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Backend/default_parameters','./Backend'),
                    ('/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/Prueba_FPFH_camion','Prueba_FPFH_camion'),
                    ('/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/VisualizationWidgets/*.png','./VisualizationWidgets'),
                    ('/home/jhon-kevin/Documentos/all_project/Repositorio/Interface/VisualizationWidgets/*.jpg','./VisualizationWidgets'),
                    ('/home/jhon-kevin/Documentos/Vimba_2_1/VimbaGigETL/CTI/x86_64bit','./Vimba_2_1/VimbaGigETL/CTI/x86_64bit'),
                    ('/home/jhon-kevin/Documentos/Vimba_2_1/VimbaC/DynamicLib/x86_64bit','./Vimba_2_1/VimbaC/DynamicLib/x86_64bit')],
             hiddenimports=['six','sklearn.neighbors.typedefs','sklearn.tree._utils', 'matplotlib', '_pyqt4', 'vispy.app.backends._pyqt4', 'PyQt4', 'PyQt4.uic'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='Index',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='Index')
